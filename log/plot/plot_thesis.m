% plot_thesis.m Nice plots for my thesis

clear all;
close all;

dataDir = '../';

run thesis_plot_globals.m
global FONT_SIZE FONT_SIZE_TITLE FONT_UNITS FIGURE_UNITS ...
    MAX_FIGURE_WIDTH INTERPRETER LINE_WIDTH;

lineWidth = LINE_WIDTH;
fontSize = FONT_SIZE;
fontUnits = 'points';
figureUnits = FIGURE_UNITS;
interp = INTERPRETER;
figurePosition = [5 5 17 14];

yLabelPos = -1;


%% Scrape low friction

task = 'iiwa_nloc_scrape_low_friction';
data = load(fullfile(dataDir, [task '.mat']));

f1 = figure;
f1.Name = 'End effector motion';
f1.Units = figureUnits;
f1.Position = [5 5 MAX_FIGURE_WIDTH 20];

subplot(4, 1, 1);
plot(data.time, data.eePosX, 'r-');
hold on;
plot(data.time, data.eePosY, 'g-.');
plot(data.time, data.eePosZ, 'b--');
grid on;

subplot(4, 1, 2);
plot(data.time, data.eeVelX), 'r-';
hold on;
plot(data.time, data.eeVelY, 'g-.');
plot(data.time, data.eeVelZ, 'b--');
grid on;

subplot(4, 1, 3);
plot(data.time, data.eeForceX, 'r-');
hold on;
plot(data.time, data.eeForceY, 'g-.');
plot(data.time, data.eeForceZ, 'b--');
grid on;

subplot(4, 1, 4);
plot(data.time, data.workCost, 'k');
grid on;

f1.Units = figureUnits;
[f1.Children.XGrid] = deal('on');
[f1.Children.YGrid] = deal('on');
f1.Children(1).XLabel.String = 'Time [s]';
f1.Children(1).XLabel.FontSize = fontSize;
f1.Children(1).XLabel.Interpreter = interp;
[f1.Children(2:4).XTickLabel]=deal([]);
c = vertcat(f1.Children.Children);
[c.LineWidth] = deal(lineWidth);

[f1.Children.FontSize] = deal(fontSize);
[f1.Children.TickLabelInterpreter] = deal(interp);

ylabel(f1.Children(4), '$x$ [m]', ...
        'HorizontalAlignment', 'center', ...
        'fontSize', fontSize, ...
        'Interpreter', interp, ...
        'units', 'centimeters');
    
ylabel(f1.Children(3), '$v$ [$m s^{-1}$]', ...
        'HorizontalAlignment', 'center', ...
        'fontSize', fontSize, ...
        'Interpreter', interp, ...
        'units', 'centimeters');    
    
ylabel(f1.Children(2), '$\lambda$ [N]', ...
        'HorizontalAlignment', 'center', ...
        'fontSize', fontSize, ...
        'Interpreter', interp, ...
        'units', 'centimeters');      
    
ylabel(f1.Children(1), '$W$ [$J s^{-1}$]', ...
        'HorizontalAlignment', 'center', ...
        'fontSize', fontSize, ...
        'Interpreter', interp, ...
        'units', 'centimeters');      
    
f1.Children(1).YLabel.Position(1) = yLabelPos;
f1.Children(2).YLabel.Position(1) = yLabelPos;
f1.Children(3).YLabel.Position(1) = yLabelPos;
f1.Children(4).YLabel.Position(1) = yLabelPos;


% text(-1.5, figurePosition(4)*0, 'Position [m]', ...
%     'Units', figureUnits, ...
%     'Rotation', 90, ...
%     'HorizontalAlignment', 'left', ...    
%     'FontSize', fontSize, ...
%     'Parent', f1);

% f2 = figure;
% subplot(3, 1, 1);
% plot(data.time, data.eeVelX);
% subplot(3, 1, 2);
% plot(data.time, data.eeVelY);
% subplot(3, 1, 3);
% plot(data.time, data.eeVelZ);
% 
% 
% f2.Name = 'End effector velocity';
% f2.Units = figureUnits;
% f2.Position = figurePosition;
% [f2.Children.XGrid] = deal('on');
% [f2.Children.YGrid] = deal('on');
% f2.Children(1).XLabel.String = 'Time [s]';
% f2.Children(1).XLabel.FontSize = fontSize;
% f2.Children(1).XLabel.Interpreter = interp;
% [f2.Children(2:3).XTickLabel]=deal([]);
% c = [f2.Children.Children];
% [c.LineWidth] = deal(lineWidth);
% 
% [f2.Children.FontSize] = deal(fontSize);
% [f2.Children.TickLabelInterpreter] = deal(interp);
% 
% ylabel(f2.Children(2), 'Velocity [m]', ...
%         'HorizontalAlignment', 'center', ...
%         'fontSize', fontSize, ...
%         'interpreter', interp);
% 
% f3 = figure;
% subplot(3, 1, 1);
% plot(data.time, data.eeForceX);
% subplot(3, 1, 2);
% plot(data.time, data.eeForceY);
% subplot(3, 1, 3);
% plot(data.time, data.eeForceZ);
% grid on;
% 
% f3.Name = 'End effector force';
% f3.Units = figureUnits;
% f3.Position = figurePosition;
% [f3.Children.XGrid] = deal('on');
% [f3.Children.YGrid] = deal('on');
% f3.Children(1).XLabel.String = 'Time [s]';
% f3.Children(1).XLabel.FontSize = fontSize;
% f3.Children(1).XLabel.Interpreter = interp;
% [f3.Children(2:3).XTickLabel]=deal([]);
% c = [f3.Children.Children];
% [c.LineWidth] = deal(lineWidth);
% 
% [f3.Children.FontSize] = deal(fontSize);
% [f3.Children.TickLabelInterpreter] = deal(interp);
% 
% ylabel(f3.Children(2), 'Force [N]', ...
%         'HorizontalAlignment', 'center', ...
%         'fontSize', fontSize, ...
%         'interpreter', interp);
    
f4 = figure;
plot(data.time, data.workCost);
grid on;

f4.Name = 'Mechanical power';
f4.Units = figureUnits;
f4.Position = [5 5 17 5];
[f4.Children.XGrid] = deal('on');
[f4.Children.YGrid] = deal('on');
f4.Children(1).XLabel.String = 'Time [s]';
f4.Children(1).XLabel.FontSize = fontSize;
f4.Children(1).XLabel.Interpreter = interp;
c = [f4.Children.Children];
[c.LineWidth] = deal(lineWidth);

[f4.Children.FontSize] = deal(fontSize);
[f4.Children.TickLabelInterpreter] = deal(interp);

ylabel(f4.Children(1), 'Power [J/sec]', ...
        'HorizontalAlignment', 'center', ...
        'fontSize', fontSize, ...
        'interpreter', interp);
    

%% Save as pdf
filename = [task '.pdf'];
saveAsPDF(f1, filename);
system(sprintf('exiftool -overwrite_original -Title=''%s'' %s', ...
    f1.Name, filename), '-echo');

    
% %% Save as png
% filename = [task '_pos' '.png'];
% print('-r300', f1, filename, '-dpng');
% system(sprintf('exiftool -overwrite_original -Title=''%s'' %s', f1.Name, filename), '-echo');
% 
% filename = [task '_vel' '.png'];
% print('-r300', f2, filename, '-dpng');
% system(sprintf('exiftool -overwrite_original -Title=''%s'' %s', f2.Name, filename), '-echo');
% 
% filename = [task '_force' '.png'];
% print('-r300', f3, filename, '-dpng');
% system(sprintf('exiftool -overwrite_original -Title=''%s'' %s', f3.Name, filename), '-echo');
% 
% filename = [task '_power' '.png'];
% print('-r300', f4, filename, '-dpng');
% system(sprintf('exiftool -overwrite_original -Title=''%s'' %s', f4.Name, filename), '-echo');
%     
%     
% %% Save as eps
% filename = [task '_pos' '.eps'];
% print('-r300', f1, filename, '-depsc');
% system(sprintf('exiftool -overwrite_original -Title=''%s'' %s', f1.Name, filename), '-echo');
% 
% filename = [task '_vel' '.eps'];
% print('-r300', f2, filename, '-depsc');
% system(sprintf('exiftool -overwrite_original-Title=''%s'' %s', f2.Name, filename), '-echo');
% 
% filename = [task '_force' '.eps'];
% print('-r300', f3, filename, '-depsc');
% system(sprintf('exiftool -overwrite_original -Title=''%s'' %s', f3.Name, filename), '-echo');
% 
% filename = [task '_power' '.eps'];
% print('-r300', f4, filename, '-depsc');
% system(sprintf('exiftool -overwrite_original -Title=''%s'' %s', f4.Name, filename), '-echo');

    