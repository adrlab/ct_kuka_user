function plotTask(task)

dataDir = '../';

lineWidth = 1.5;
fontSize = 12;
fontUnits = 'points';
figureUnits = 'centimeters';
figurePosition = [5 5 17 14];
yLabelPos = [-0.34 0.05 -1];


%% Scrape low friction

data = load(fullfile(dataDir, [task '.mat']));

f1 = figure;

subplot(3, 1, 1);
plot(data.time, data.eePosX);
subplot(3, 1, 2);
plot(data.time, data.eePosY);
subplot(3, 1, 3);
plot(data.time, data.eePosZ);

f1.Name = 'End effector position';
f1.Units = figureUnits;
f1.Position = figurePosition;
[f1.Children.XGrid] = deal('on');
[f1.Children.YGrid] = deal('on');
f1.Children(1).XLabel.String = 'Time [sec]';
f1.Children(1).XLabel.FontSize = fontSize;
[f1.Children(2:3).XTickLabel]=deal([]);
c = [f1.Children.Children];
[c.LineWidth] = deal(lineWidth);

ylabel(f1.Children(2), 'Position [m]', ...
        'HorizontalAlignment', 'center', ...
        'fontSize', fontSize);
%     'Position', yLabelPos, ...


% text(-1.5, figurePosition(4)*0, 'Position [m]', ...
%     'Units', figureUnits, ...
%     'Rotation', 90, ...
%     'HorizontalAlignment', 'left', ...    
%     'FontSize', fontSize, ...
%     'Parent', f1);

f2 = figure;
subplot(3, 1, 1);
plot(data.time, data.eeVelX);
subplot(3, 1, 2);
plot(data.time, data.eeVelY);
subplot(3, 1, 3);
plot(data.time, data.eeVelZ);
grid on;

f2.Name = 'End effector velocity';
f2.Units = figureUnits;
f2.Position = figurePosition;
[f2.Children.XGrid] = deal('on');
[f2.Children.YGrid] = deal('on');
f2.Children(1).XLabel.String = 'Time [sec]';
f2.Children(1).XLabel.FontSize = fontSize;
[f2.Children(2:3).XTickLabel]=deal([]);
c = [f2.Children.Children];
[c.LineWidth] = deal(lineWidth);

ylabel(f2.Children(2), 'Velocity [m]', ...
        'HorizontalAlignment', 'center', ...
        'fontSize', fontSize);

f3 = figure;
subplot(3, 1, 1);
plot(data.time, data.eeForceX);
subplot(3, 1, 2);
plot(data.time, data.eeForceY);
subplot(3, 1, 3);
plot(data.time, data.eeForceZ);
grid on;

f3.Name = 'End effector force';
f3.Units = figureUnits;
f3.Position = figurePosition;
[f3.Children.XGrid] = deal('on');
[f3.Children.YGrid] = deal('on');
f3.Children(1).XLabel.String = 'Time [sec]';
f3.Children(1).XLabel.FontSize = fontSize;
[f3.Children(2:3).XTickLabel]=deal([]);
c = [f3.Children.Children];
[c.LineWidth] = deal(lineWidth);

ylabel(f3.Children(2), 'Force [N]', ...
        'HorizontalAlignment', 'center', ...
        'fontSize', fontSize);
    
f4 = figure;
plot(data.time, data.workCost);
grid on;

f4.Name = 'Mechanical power';
f4.Units = figureUnits;
f4.Position = [5 5 17 5];
[f4.Children.XGrid] = deal('on');
[f4.Children.YGrid] = deal('on');
f4.Children(1).XLabel.String = 'Time [sec]';
f4.Children(1).XLabel.FontSize = fontSize;
c = [f4.Children.Children];
[c.LineWidth] = deal(lineWidth);

ylabel(f4.Children(1), 'Power [J/sec]', ...
        'HorizontalAlignment', 'center', ...
        'fontSize', fontSize);

    
%% Save as png
filename = [task '_pos' '.png'];
print('-r300', f1, filename, '-dpng');
system(sprintf('exiftool -Title=''%s'' %s', f1.Name, filename), '-echo');

filename = [task '_vel' '.png'];
print('-r300', f2, filename, '-dpng');
system(sprintf('exiftool -Title=''%s'' %s', f2.Name, filename), '-echo');

filename = [task '_force' '.png'];
print('-r300', f3, filename, '-dpng');
system(sprintf('exiftool -Title=''%s'' %s', f3.Name, filename), '-echo');

filename = [task '_power' '.png'];
print('-r300', f4, filename, '-dpng');
system(sprintf('exiftool -Title=''%s'' %s', f4.Name, filename), '-echo');
    
    
%% Save as eps
filename = [task '_pos' '.eps'];
print('-r300', f1, filename, '-depsc');
system(sprintf('exiftool -Title=''%s'' %s', f1.Name, filename), '-echo');

filename = [task '_vel' '.eps'];
print('-r300', f2, filename, '-depsc');
system(sprintf('exiftool -Title=''%s'' %s', f2.Name, filename), '-echo');

filename = [task '_force' '.eps'];
print('-r300', f3, filename, '-depsc');
system(sprintf('exiftool -Title=''%s'' %s', f3.Name, filename), '-echo');

filename = [task '_power' '.eps'];
print('-r300', f4, filename, '-depsc');
system(sprintf('exiftool -Title=''%s'' %s', f4.Name, filename), '-echo');

end
    