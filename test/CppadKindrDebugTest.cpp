/*
 * CppadKindrDebugTest.cpp
 *
 *  Created on: Sep 4, 2020
 *      Author: johannes
 *
 * Problem: This test fails when compiling in debug mode; results in error related to cppadcg and kindr, like:
 * "function ceil(...) not defined".
 *
 * Reason: also appears in ct_models when CPPADCG is on. Seems to be a bug with cppad and kindr.
 * Kindr triggers an assert for the cppadcg type when in debug mode. See related issue on ct's github page (#43).
 *
 * Workaround: Make sure kindr is not compiling in debug mode.
 *
 */

#include <gtest/gtest.h>

#include <ct/core/core.h>
#include <ct/rbd/rbd.h>


TEST(CppadEigenCeilDebugTest, DoubleEigenCeil)
{

	using SCALAR = double;
	using Vector3s = ct::rbd::tpl::RigidBodyPose<SCALAR>::Vector3Tpl;

	ct::rbd::tpl::RigidBodyPose<SCALAR> rbdPose;
	rbdPose.setRandom();

	Vector3s vec = Vector3s::Random();

	rbdPose.rotateBaseToInertia(vec);

	ASSERT_TRUE(true);
}

TEST(CppadEigenCeilDebugTest, CppadEigenCeil)
{

//	using SCALAR = CppAD::AD<CppAD::cg::CG<double>>;

	using SCALAR = ct::core::ADCGScalar;
	using Vector3s = ct::rbd::tpl::RigidBodyPose<SCALAR>::Vector3Tpl;

	double tmp = 3.142e+0;
	SCALAR tmp_s = (SCALAR) tmp;

	ct::rbd::tpl::RigidBodyPose<SCALAR> rbdPose;
//	rbdPose.setRandom();

	Vector3s vec; // = Vector3s::Random();

	rbdPose.rotateBaseToInertia(vec);

	ASSERT_TRUE(true);
}


int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
