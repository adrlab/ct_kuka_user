/*
 * TermFrictionWorkTest.cpp
 *
 *  Created on: March 10, 2021
 *      Author: johannes
 *
 * Test for debugging of nan/inf error during evaluation of jit compiled function replicating the work term.
 *
 */

#include <gtest/gtest.h>

#include <ct/core/core.h>
#include <ct/rbd/rbd.h>

#include <kindr/Core>

#include <ct/iiwa/IiwaSystem.h>
#include <ct/iiwa/pathNames.h>

using ADCGScalar = ct::core::ADCGScalar;

typedef typename ct::core::tpl::TraitSelector<ADCGScalar>::Trait TRAIT;

using ContactModelCG =  ct::kuka::EEContactModel<ct::rbd::iiwa::tpl::Dynamics<ADCGScalar>::Kinematics_t>;

const size_t state_dim = ct::iiwa::System::STATE_DIM;
const size_t control_dim = ct::iiwa::System::CONTROL_DIM;
const size_t num_joints = ct::iiwa::System::NJOINTS;
const size_t num_contact_para = ContactModelCG::NUM_PARA;
const size_t num_weights = 1;

const size_t ad_para_dim = state_dim + control_dim + num_contact_para + num_weights;

typedef ct::core::StateVector<state_dim, double> q_type;
typedef ct::core::ControlVector<control_dim, double> r_type;

typedef ct::core::StateMatrix<state_dim, double> Q_type;
typedef ct::core::ControlMatrix<control_dim, double> R_type;

using state = ct::core::StateVector<state_dim, ADCGScalar>;
using control = ct::core::ControlVector<control_dim, ADCGScalar>;
using RobotState = ct::rbd::FixBaseRobotState<num_joints, 0, ADCGScalar>;

using DerivativesCppadJIT = ct::core::DerivativesCppadJIT<ad_para_dim, 1>;
using ContactModelCG =  ct::kuka::EEContactModel<ct::rbd::iiwa::tpl::Dynamics<ADCGScalar>::Kinematics_t>;

std::shared_ptr<ContactModelCG> contactModelCG = std::shared_ptr<ContactModelCG>(new ContactModelCG());


Eigen::Matrix<ADCGScalar, 1, 1> testCmRegular(const Eigen::Matrix<ADCGScalar, ad_para_dim, 1>& input)
{

    // friction force times velocity
    Eigen::Matrix<ADCGScalar, state_dim, 1> x = input.template segment<state_dim>(0);
    Eigen::Matrix<ADCGScalar, control_dim, 1> u = input.template segment<control_dim>(state_dim);
    Eigen::Matrix<ADCGScalar, num_contact_para, 1> cp =
    		input.template segment<num_contact_para>(state_dim + control_dim);

    ADCGScalar weight = input(state_dim + control_dim + num_contact_para);

	contactModelCG->k() 		= cp(0);
	contactModelCG->d() 		= cp(1);
	contactModelCG->alpha_k() 	= cp(2);
	contactModelCG->alpha_s() 	= cp(3);
	contactModelCG->mu() 		= cp(4);
	contactModelCG->eps() 		= cp(5);	// <-- causing the jit compilation error (-nan in generated code)
	contactModelCG->zOffset() 	= cp(6);

//	x.setOnes();
	RobotState robotState = RobotState(state(x));

    ct::rbd::iiwa::tpl::Dynamics<ADCGScalar>::Kinematics_t kinematics = ct::rbd::iiwa::tpl::Dynamics<ADCGScalar>::Kinematics_t();

	ContactModelCG::EEForceLinear contactForce = contactModelCG->computeContactForces(robotState.toRBDState())[0];
    kindr::Velocity<ADCGScalar, 3> velocity = kinematics.getEEVelocityInWorld(0, robotState.toRBDState());

	ADCGScalar power = contactForce(0)*velocity(0) + contactForce(1)*velocity(1);

	return Eigen::Matrix<ADCGScalar, 1, 1>(power);
}

/*Eigen::Matrix<ADCGScalar, 1, 1> testCmCustom(const Eigen::Matrix<ADCGScalar, ad_para_dim, 1>& input)
{

    // friction force times velocity
    Eigen::Matrix<ADCGScalar, state_dim, 1> x = input.template segment<state_dim>(0);
    Eigen::Matrix<ADCGScalar, control_dim, 1> u = input.template segment<control_dim>(state_dim);
    Eigen::Matrix<ADCGScalar, num_contact_para, 1> cp =
    		input.template segment<num_contact_para>(state_dim + control_dim);

    ADCGScalar weight = input(state_dim + control_dim + num_contact_para);

	ADCGScalar k 		= cp(0);
	ADCGScalar d 		= cp(1);
	ADCGScalar alpha_k 	= cp(2);
	ADCGScalar alpha_s 	= cp(3);
	ADCGScalar mu 		= cp(4);
	ADCGScalar eps 		= cp(5);
	ADCGScalar zOffset 	= cp(6);

	RobotState robotState = RobotState(state(x));

    ct::rbd::iiwa::tpl::Dynamics<ADCGScalar>::Kinematics_t kinematics = ct::rbd::iiwa::tpl::Dynamics<ADCGScalar>::Kinematics_t();

    kindr::Position<ADCGScalar, 3> position = kinematics.getEEPositionInWorld(0, robotState.toRBDState().basePose(), robotState.toRBDState().jointPositions());
    kindr::Velocity<ADCGScalar, 3> velocity = kinematics.getEEVelocityInWorld(0, robotState.toRBDState());

    // Compute normal force
    ct::rbd::iiwa::tpl::Dynamics<ADCGScalar>::Kinematics_t::EEForceLinear forceNormal;
    ct::rbd::iiwa::tpl::Dynamics<ADCGScalar>::Kinematics_t::EEForceLinear forceSpring;
    ct::rbd::iiwa::tpl::Dynamics<ADCGScalar>::Kinematics_t::EEForceLinear forceDamper;

    forceSpring.setZero();
    forceDamper.setZero();

    ADCGScalar sigmoid = 1. / (1. + TRAIT::exp(- alpha_s * (- position(2) + zOffset ) ));

    forceDamper(2) = -d * velocity(2) * sigmoid;
    forceSpring(2) = k * TRAIT::exp( -alpha_k * ( position(2) - zOffset ));

    forceNormal = forceSpring + forceDamper;

    // Compute tangential force
    ct::rbd::iiwa::tpl::Dynamics<ADCGScalar>::Kinematics_t::EEForceLinear forceTangential;
    kindr::Velocity<ADCGScalar, 3> velXYUnit, velXY;

	// Project velocity onto flat ground
	velXY << velocity(0), velocity(1), (ADCGScalar)0.0;

	ADCGScalar eps2(eps * eps);

	ADCGScalar velNormSquared = velXY(0)*velXY(0) + velXY(1)*velXY(1) + eps2;

	ADCGScalar velNorm = TRAIT::sqrt(velNormSquared);

//	velXYUnit = velXY / velNorm; // <-- cannot divide zero ( velXY(2) / velNorm )

	// SOLUTION: set zero *after* normalization
	velXYUnit = velocity / velNorm;
	velXYUnit(2) = (ADCGScalar)0.0;

	forceTangential = - velXYUnit.toImplementation() * mu * forceNormal(2);

	// Compute total contact force
    ct::rbd::iiwa::tpl::Dynamics<ADCGScalar>::Kinematics_t::EEForceLinear forceTotal;

    forceTotal = forceTangential + forceSpring + forceDamper;

	return Eigen::Matrix<ADCGScalar, 1, 1>(forceTotal(2));
}*/

TEST(CppadcgCmDebugTest, ReproduceBug)
{

	using DerivativesCppadJIT = ct::core::DerivativesCppadJIT<ad_para_dim, 1>;

	DerivativesCppadJIT::FUN_TYPE_CG funHandle =
			[&](const Eigen::Matrix<ADCGScalar, ad_para_dim, 1>& input) { return testCmRegular(input); };

//	using DerivativesCppadCG = ct::core::DerivativesCppadCG<ad_para_dim, 1>;

//	std::shared_ptr<DerivativesCppadCG> derivativesCG =
//			std::shared_ptr<DerivativesCppadCG>(new DerivativesCppadCG(funHandle));

    std::shared_ptr<DerivativesCppadJIT> derivativeJIT =
    		std::shared_ptr<DerivativesCppadJIT>(new DerivativesCppadJIT(funHandle, ad_para_dim, 1));

    std::string codeName = "ReproduceBug";

    // Generate source code
//    derivativesCG->generateForwardZeroSource(codeName + "Forward", ct::iiwa::CODEGEN_OUTPUT_DIR);
//    derivativesCG->generateJacobianSource(codeName + "Jacobbian", ct::iiwa::CODEGEN_OUTPUT_DIR);
//    derivativesCG->generateHessianSource(codeName + "Hessian", ct::iiwa::CODEGEN_OUTPUT_DIR);

    // Generate and compile code
    ct::core::DerivativesCppadSettings settings;
    settings.createForwardZero_ = true;
	settings.createReverseOne_ = true;
    settings.createJacobian_ = true;
    settings.createHessian_ = true;

    settings.compiler_ = ct::core::DerivativesCppadSettings::CompilerType::CLANG;
    settings.useDynamicLibrary_ = false;
    settings.generateSourceCode_ = true;

    std::cout << "CppadcgCmDebugTest: compiling JIT for " << codeName << std::endl;
    settings.print();

	try
	{

    derivativeJIT->compileJIT(settings, codeName, true);

	std::shared_ptr<ct::iiwa::ContactModel> contactModel(new ct::iiwa::ContactModel());

	// Set default contact model parameters
	contactModel->k() 			= 50.0;
	contactModel->d() 			= 60.0;
	contactModel->alpha_k() 	= 10.0;
	contactModel->alpha_s() 	= 50.0;
	contactModel->mu() 			= 0.1;
	contactModel->eps() 		= 0.01;
	contactModel->zOffset() 	= 0.0;

	q_type x;
	r_type u;

	double t = 0.0;
	double out;

	x.setZero();
	u.setZero();

    Eigen::Matrix<double, ad_para_dim, 1> adParameterVector;

    Eigen::Matrix<double, 1, 1> w(1.0);

    adParameterVector.template segment<state_dim>(0) = x;
    adParameterVector.template segment<control_dim>(state_dim) = u;
    adParameterVector(state_dim + control_dim + num_contact_para) = 1.0;
    adParameterVector(ad_para_dim - 1) = t;

    adParameterVector(state_dim + control_dim + 0) = contactModel->k();
    adParameterVector(state_dim + control_dim + 1) = contactModel->d();
    adParameterVector(state_dim + control_dim + 2) = contactModel->alpha_k();
    adParameterVector(state_dim + control_dim + 3) = contactModel->alpha_s();
    adParameterVector(state_dim + control_dim + 4) = contactModel->mu();
    adParameterVector(state_dim + control_dim + 5) = contactModel->eps();
    adParameterVector(state_dim + control_dim + 6) = contactModel->zOffset();

	std::cout << "evaluation: " << derivativeJIT->forwardZero(adParameterVector) << std::endl;

/*	q_type q = termFW.stateDerivative(x, u, t);
	r_type r = termFW.controlDerivative(x, u, t);
	Q_type Q = termFW.stateSecondDerivative(x, u, t);
	R_type R = termFW.controlSecondDerivative(x, u, t);*/

	std::cout << "state derivative: " << std::endl << derivativeJIT->jacobian(adParameterVector) << std::endl;
	std::cout << "control derivative: " << std::endl << derivativeJIT->hessian(adParameterVector, w) << std::endl;

	ASSERT_TRUE(true);

	} catch (std::exception& e)
	{
		FAIL();
	}
}

/*TEST(CppadcgCmDebugTest, TestCmCustom)
{

	using DerivativesCppadJIT = ct::core::DerivativesCppadJIT<ad_para_dim, 1>;

	DerivativesCppadJIT::FUN_TYPE_CG funHandle =
			[&](const Eigen::Matrix<ADCGScalar, ad_para_dim, 1>& input) { return testCmCustom(input); };

//	using DerivativesCppadCG = ct::core::DerivativesCppadCG<ad_para_dim, 1>;

//	std::shared_ptr<DerivativesCppadCG> derivativesCG =
//			std::shared_ptr<DerivativesCppadCG>(new DerivativesCppadCG(funHandle));

    std::shared_ptr<DerivativesCppadJIT> derivativeJIT =
    		std::shared_ptr<DerivativesCppadJIT>(new DerivativesCppadJIT(funHandle, ad_para_dim, 1));

    std::string codeName = "TestCmCustom";

    // Generate source code
//    derivativesCG->generateForwardZeroSource(codeName + "Forward", ct::iiwa::CODEGEN_OUTPUT_DIR);
//    derivativesCG->generateJacobianSource(codeName + "Jacobbian", ct::iiwa::CODEGEN_OUTPUT_DIR);
//    derivativesCG->generateHessianSource(codeName + "Hessian", ct::iiwa::CODEGEN_OUTPUT_DIR);

    // Generate and compile code
    ct::core::DerivativesCppadSettings settings;
    settings.createForwardZero_ = true;
	settings.createReverseOne_ = true;
    settings.createJacobian_ = true;
    settings.createHessian_ = true;

    settings.compiler_ = ct::core::DerivativesCppadSettings::CompilerType::CLANG;
    settings.useDynamicLibrary_ = false;
    settings.generateSourceCode_ = true;

    std::cout << "CppadcgCmDebugTest: compiling JIT for " << codeName << std::endl;
    settings.print();

    derivativeJIT->compileJIT(settings, codeName, true);

	ASSERT_TRUE(true);
}*/


int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
	//testing::GTEST_FLAG(filter) = "CppadCmJitEvaluationTest";
    return RUN_ALL_TESTS();
}
