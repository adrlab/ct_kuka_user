/*
 * ADCodegenLinearizerTest.h
 *
 *  Created on: Jul 28, 2020
 *      Author: johannes
 */


#include <ct/iiwa/IiwaSystem.h>

#include <ct/iiwa/codegen/IiwaCMLinearizedForward.h>
//#include <ct/iiwa/codegen/IiwaLinearizedForward.h>
//#include <ct/iiwa/codegen/IiwaLinearizedReverse.h>

#include <gtest/gtest.h>

/*!
 * Code generation test for writing code to file

TEST(ADCodegenLinearizerTest, CodegenTest)
{
    // define the dimensions of the system
    const size_t state_dim = TestNonlinearSystem::STATE_DIM;
    const size_t control_dim = TestNonlinearSystem::CONTROL_DIM;

    // typedefs for the auto-differentiable codegen system
    typedef ADCodegenLinearizer<state_dim, control_dim>::ADCGScalar Scalar;
    typedef typename Scalar::value_type AD_ValueType;
    typedef tpl::TestNonlinearSystem<Scalar> TestNonlinearSystemAD;

    // create an auto-differentiable codegen system
    const double w_n = 100.0;
    shared_ptr<TestNonlinearSystemAD> oscillatorAD(new tpl::TestNonlinearSystem<Scalar>(AD_ValueType(w_n)));

    // create a linearizer that uses codegeneration
    ADCodegenLinearizer<state_dim, control_dim> adLinearizer(oscillatorAD);

    try
    {
        std::cout << "generating code..." << std::endl;
        // generate code for the Jacobians
        adLinearizer.generateCode("TestNonlinearSystemLinearized");
        std::cout << "... done!" << std::endl;
    } catch (const std::runtime_error& e)
    {
        std::cout << "code generation failed: " << e.what() << std::endl;
        ASSERT_TRUE(false);
    }
}*/

TEST(ADCodegenLinearizerTest, IiwaCMLinearizedForwardTest)
{

	const size_t state_dim = ct::iiwa::System::STATE_DIM;
	const size_t control_dim = ct::iiwa::System::CONTROL_DIM;

    typedef ct::core::StateMatrix<state_dim, double> A_type;
    typedef ct::core::StateControlMatrix<state_dim, control_dim, double> B_type;

	std::shared_ptr<ct::iiwa::ContactModel> contactModel(new ct::iiwa::ContactModel());
    std::shared_ptr<ct::iiwa::System> iiwa(new ct::iiwa::System(contactModel));

	// Numeric linearizer
	ct::rbd::RbdLinearizer<ct::iiwa::System> iiwaLinNum = ct::rbd::RbdLinearizer<ct::iiwa::System>(iiwa);

	// Generated linear system with contact model
	ct::iiwa::codegen::IiwaCMLinearizedForward iiwaCMLin = ct::iiwa::codegen::IiwaCMLinearizedForward(contactModel);
//	ct::iiwa::codegen::IiwaLinearizedReverse iiwaCMLin = ct::iiwa::codegen::IiwaLinearizedReverse();

	// Set default contact model parameters
	contactModel->k() 			= 50.0;
	contactModel->d() 			= 60.0;
	contactModel->alpha_k() 	= 10.0;
	contactModel->alpha_s() 	= 50.0;
	contactModel->mu() 			= 0.1;
	contactModel->eps() 		= 0.01;
	contactModel->zOffset() 	= 0.0;

	ct::iiwa::System::Force Fc;

    // create state, control and time variables
    ct::core::StateVector<state_dim> x;
    ct::core::ControlVector<control_dim> u;
    double t = 0;

    for (size_t i = 0; i < 1000; i++)
    {
        // set a random state
        x.setRandom();
        u.setRandom();

        // use the numerical differentiation linearizer
        A_type A_num = iiwaLinNum.getDerivativeState(x, u, t);
        B_type B_num = iiwaLinNum.getDerivativeControl(x, u, t);

        // use the auto diff codegen linearzier
        A_type A_cm = iiwaCMLin.getDerivativeState(x, u, t);
        B_type B_cm = iiwaCMLin.getDerivativeControl(x, u, t);

//        std::cout << "A max coef: " << (A_num - A_cm).array().abs().maxCoeff() << std::endl;
//        std::cout << "B max coef: " << (B_num - B_cm).array().abs().maxCoeff() << std::endl;

        Eigen::Index idxR;
        Eigen::Index idxC;

//        std::cout << "A_num adday: " << A_num.array() << std::endl;
//
//        A_num.array().maxCoeff(&idxR, &idxC);
//
//        std::cout << "max coef: " << idxR << "\t" << idxC << std::endl;

        double Acoef = (A_num - A_cm).array().abs().maxCoeff(&idxR, &idxC);
        double AcoefRel = Acoef / std::abs(A_num(idxR, idxC));

        if ( Acoef > 1e-1)
		{
			std::cout << "A coef diff: " << Acoef << " A coef diff rel: " << AcoefRel;

			iiwa->computeContactForceInWorld(x, Fc);
			std::cout << "Contact force: " << Fc.transpose() << std::endl;
		}

        double Bcoef = (B_num - B_cm).array().abs().maxCoeff(&idxR, &idxC);
        double BcoefRel = Bcoef / std::abs(B_num(idxR, idxC));

        if (Bcoef > 1e-1)
		{
			std::cout << "B coef crit: " << Bcoef << std::endl;
		}

        // verify the result
//        ASSERT_LT((Acoef, 1e2);
//        ASSERT_LT((Bcoef, 1e2);

        ASSERT_LT(AcoefRel, 1e-1);
        ASSERT_LT(BcoefRel, 1e-1);

    }
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
