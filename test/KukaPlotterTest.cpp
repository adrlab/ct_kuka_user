/*
 * KUKAPlotterTest.cpp
 *
 *  Created on: Dec 4, 2019
 *      Author: johannes
 */

#include <iostream>
#include <cstdlib>

#include <chrono>
#include <thread>

#include <ct/core/core.h>
#include <ct/iiwa/iiwa.h>
#include <ct/kuka/KukaPlotter.h>
#include <ct/kuka/FixBaseFDSystem.h>

#include <gtest/gtest.h>

using namespace ct::core;
using namespace ct::kuka;


//TEST(KUKAPlotter, PlotTimeSeriesVector)
//{
//
//	KUKAPlotter plotter;
//
//    const size_t nSeries = 10;
//    const size_t vecDim = 2;
//
//	// Create a series of vectors and fill random elements
//	StateVectorArray<vecDim, double> seriesVec(nSeries);
//
//    for (size_t i = 0; i < nSeries; i++)
//    {
//        seriesVec.at(i).setRandom();
//    }
//
//	TimeArray time(1.0, nSeries+1);
//
//    plotter.plotTimeSeriesVector(time, seriesVec);
//
//    plot::show();
//
//}



TEST(KUKAPlotter, PlotAll)
{

    using RobotDynamics = ct::rbd::iiwa::tpl::Dynamics<double>;
    using RobotSystem = ct::kuka::FixBaseFDSystem<RobotDynamics>;
    using ContactModel = ct::kuka::EEContactModel<typename RobotDynamics::Kinematics_t>;

    std::shared_ptr<ContactModel> contactModel(new ContactModel);
    std::shared_ptr<RobotSystem> system(new RobotSystem(contactModel));

    const size_t state_dim = RobotSystem::STATE_DIM;
    const size_t control_dim = RobotSystem::CONTROL_DIM;

//    using ScalarCG = CppAD::AD< CppAD::cg::CG<double>>; // cannot use because of bug below
    using ScalarCG = double;
    using TermQuadraticAD = ct::optcon::TermQuadratic<state_dim, control_dim, double, ScalarCG>;

    const size_t nSeries = 10;

	ct::rbd::tpl::RigidBodyPose<double> basePose = ct::rbd::tpl::RigidBodyPose<double>();
	basePose.position().setRandom();
	basePose.getEulerAnglesXyz().setRandom();

	// Create a series of vectors and fill random elements
	StateVectorArray<state_dim, double> states(nSeries);
	ControlVectorArray<control_dim, double> controls(nSeries);

	Eigen::Matrix<double, state_dim, state_dim> Q;
	Eigen::Matrix<double, control_dim, control_dim> R;

	std::shared_ptr<ct::optcon::TermBase<state_dim, control_dim, double, ScalarCG>>
		termQuad1(new TermQuadraticAD(Q.setRandom(), R.setRandom()));
	std::shared_ptr<ct::optcon::TermBase<state_dim, control_dim, double, ScalarCG>>
		termQuad2(new TermQuadraticAD(Q.setRandom(), R.setRandom()));

	termQuad1->setName("Term 1");
	termQuad2->setName("Term 2");

	// Add time activation
	std::shared_ptr<ct::core::SingleActivation>
		timeActivation(new ct::core::SingleActivation(0.0, 5.0));

	termQuad2->setTimeActivation(timeActivation, true);

	std::vector<std::shared_ptr<ct::optcon::TermBase<state_dim, control_dim, double, ScalarCG>>> terms;
//	std::vector<std::shared_ptr<TermQuadraticAD>> terms;

	terms.push_back(termQuad1);
	terms.push_back(termQuad2);

    for (size_t i = 0; i < nSeries; i++)
    {
        states.at(i).setRandom();
        controls.at(i).setRandom();
    }

	TimeArray time(1.0, nSeries);

	{	// Use this to close windows after end of scope
		KukaPlotter plotter;

		plotter.plotJointPositions(time, states);

		plotter.plotJointVelocities(time, states);
		plotter.plotJointTorques(time, controls);

		plotter.plotEEStates(time, states, system, basePose);

		// TODO debug evaluation of AD cost terms.
		// Problem: (SCLAR_EVAL)eval depends on (SCALAR)evaluate.
		plotter.plotCostTerms(time, states, controls, terms);

		//plot::ion();
		plot::show(true);

		std::this_thread::sleep_for(std::chrono::milliseconds(3000));
	}
}



int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
