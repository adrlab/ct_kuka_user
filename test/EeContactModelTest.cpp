
#include <ct/rbd/rbd.h>

#include <memory>
#include <gtest/gtest.h>

#include <ct/rbd/physics/EEContactModel.h>

#include "ct/iiwa/IiwaSystem.h"
#include "ct/kuka/EEContactModel.h"


TEST(EEContactModelTest, basicTest)
{
    typedef ct::iiwa::Dynamics::Kinematics_t IiwaKinematics;
    typedef typename ct::iiwa::ContactModel::EEForcesLinear EEForcesLinear;

    ct::iiwa::ContactModel eeContactModel;

    ct::rbd::RBDState<IiwaKinematics::NJOINTS> state;
    state.setRandom();

    EEForcesLinear forces;

    try
    {
        forces = eeContactModel.computeContactForces(state);
    } catch (const std::runtime_error& e)
    {
        std::cout << "error thrown: " << e.what() << std::endl;
        ASSERT_TRUE(false);
    }

    for (size_t i = 0; i < forces.size(); i++)
    {
        std::cout << "Force at EE-ID " << i << ": " << forces[i].transpose();
    }

    std::cout << std::endl;
}


int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
