/*
 * TermNormalForceTest.cpp
 *
 *  Created on: Aug 19, 2020
 *      Author: johannes
 */

#include <ct/iiwa/IiwaSystem.h>

#include <ct/kuka/TermNormalForceJitCm.h>

#include <gtest/gtest.h>


TEST(TermNormalForceTest, ScalarEvalTest)
{

	const size_t state_dim = ct::iiwa::System::STATE_DIM;
	const size_t control_dim = ct::iiwa::System::CONTROL_DIM;

	typedef ct::core::StateVector<state_dim, double> q_type;
	typedef ct::core::ControlVector<control_dim, double> r_type;

    typedef ct::core::StateMatrix<state_dim, double> Q_type;
    typedef ct::core::ControlMatrix<control_dim, double> R_type;

    typedef ct::core::StateControlMatrix<state_dim, control_dim, double> P_type;

	std::shared_ptr<ct::iiwa::ContactModel> contactModel(new ct::iiwa::ContactModel());

	// Set default contact model parameters
	contactModel->k() 			= 50.0;
	contactModel->d() 			= 60.0;
	contactModel->alpha_k() 	= 10.0;
	contactModel->alpha_s() 	= 50.0;
	contactModel->mu() 			= 0.1;
	contactModel->eps() 		= 0.01;
	contactModel->zOffset() 	= 0.0;

	q_type x;
	r_type u;

	double t = 0.0;
	double out;

	x.setZero();
	u.setZero();

	try
	{

	ct::iiwa::TermNormalForceJitCm termFN = ct::iiwa::TermNormalForceJitCm(
			contactModel, "ScalarEvalTest", 1.0, 1.0, 0.0);

	termFN.setup(true);

	std::cout << "evaluation: " << termFN.evaluate(x, u, t) << std::endl;

	q_type q = termFN.stateDerivative(x, u, t);
	r_type r = termFN.controlDerivative(x, u, t);
	Q_type Q = termFN.stateSecondDerivative(x, u, t);
	R_type R = termFN.controlSecondDerivative(x, u, t);

	std::cout << "state derivative: " << std::endl << q.transpose() << std::endl;
	std::cout << "control derivative: " << std::endl << r.transpose() << std::endl;
	std::cout << "state second derivative: " << std::endl << Q << std::endl;
	std::cout << "control second derivative: " << std::endl << R << std::endl;

	for (size_t i=0; i<1; i++)
	{
		x.setRandom();
		u.setRandom();

		out = termFN.evaluate(x, u, t);

		std::cout << "evaluation: " << out << std::endl;

		q_type q = termFN.stateDerivative(x, u, t);
		r_type r = termFN.controlDerivative(x, u, t);
		Q_type Q = termFN.stateSecondDerivative(x, u, t);
		R_type R = termFN.controlSecondDerivative(x, u, t);

		std::cout << "state derivative: " << std::endl << q.transpose() << std::endl;
		std::cout << "control derivative: " << std::endl << r.transpose() << std::endl;
		std::cout << "state second derivative: " << std::endl << Q << std::endl;
		std::cout << "control second derivative: " << std::endl << R << std::endl;

	}

	} catch (std::exception& e)
	{
		FAIL();
	}
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

