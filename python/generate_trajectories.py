import EEStateGenerator as gen

genCart = gen.EEStateGeneratorCartesian()

fn = genCart.exampleForceSaw([0.15, 0.8, -0.20] + [.0, .0, .0, 1.] + [.0]*12)
genCart.plotFromFile(fn)