# tajectory_gen.py Generate trajectory of the iiwa EE state vector.

import rospkg, time, csv, pandas
import matplotlib.pyplot as plt
import numpy as np
import numpy.matlib
from math import sin, cos, pi, radians, ceil, floor

# from abc import  ABC
from os.path import join
from operator import mul, div, add

import pdb
#from pyqtgraph.examples.parallelize import start


class EEStateGenerator: # Base Class

    genDir = join(rospkg.RosPack().get_path("ct_kuka_user"), "python/gen")

    @property
    def defaultState(self):
        raise NotImplementedError
    
    @property
    def defaultFile(self):
        raise NotImplementedError
                

    def writeToFile(self, stateTraj=[defaultState], filename=defaultFile):
        
        file = open(filename, mode='w')
        
        writer = csv.writer(file, dialect=csv.excel_tab)
        writer.writerows(stateTraj)
        
        file.close()
    
    
    def genSinusoid(self,
            initial=[.0]*3, scale=[1.]*3, omega=[1.]*3, phase=[.0]*3, N=10):
                           
        dim = len(initial)
        
        # Generate unit increment vectors
        time = map(lambda x, d: map(mul, [1.0]*d, [x]*d), range(N), [dim]*N)

        # Sample sinusoid
        sinus = map(lambda t: map(sin, map(add, map(mul, t, omega), phase)), time)
        
        # Scale with amplitude
        sinus = map(lambda s, a: map(mul, s, a), sinus[:], [scale]*N)
        
        # Add offset
        sinus = map(lambda s, i: map(add, s, i), sinus[:], [initial]*N)
        
        
        return(sinus)
    
    
#     def genRamp(self, initialVector=[0.0], scaleVector=[1.0], N=10):
#         
#         dim = len(initialVector)
#         
#         # Generate unit increment vectors
#         ramp = map(lambda x, d: map(mul, [1.0]*d, [x]*d), range(N), [dim]*N)
#         
#         # Scale
#         ramp = map(lambda r, s: map(mul, r, s), ramp[:], [scaleVector]*N)
#         
#         # Add offset
#         ramp = map(lambda r, i: map(add, r, i), ramp[:], [initialVector]*N)
#                     
#         return ramp
    
    def genRamp(self, start, end, deltaT):
        # Generate ramp with linear slope between two numpy vectors

#         start = np.array(start)
#         end = np.array(end)
        
        # TODO make consistent rounding scheme
        numSamples = ceil(deltaT / self.samplingTime)
            
        ramp = np.linspace(start, end, numSamples)
    
#         return list(map(list, ramp))
        return ramp
    
    
    def addTime(self, traj):
        
        time = np.array([np.arange(traj.shape[0]) * self.samplingTime]).transpose()
        
        return np.concatenate((time, traj),1)
        
        
    def genConst(self, vec, deltaT):
        # Keep value constant for duration deltaT
        
        # TODO make consistent rounding scheme
        numSamples = ceil(deltaT / self.samplingTime)
        
        constVec = np.matlib.repmat(vec, int(numSamples), 1)
        
        return constVec
             
#     def genConst(self, initial, N):
#         
#         return [initial]*N
        
        
        
    def genSinusPosVel(self, initialPos=[.0]*3, amp=[.5]*3, rounds=[1.]*3, N=10):
        
        dim = len(initialPos)
        
        omega = map(lambda r: 2*pi*r/N, rounds)
        
        pos = self.genSinusoid(initialPos, amp, omega, [.0]*dim, N)
#         pdb.set_trace()
        vel = self.genSinusoid([.0]*dim, map(mul, map(div, amp, [self.samplingTime]*dim), omega), omega, [pi/2]*dim, N)
        
        return [pos, vel]
        

        
class EEStateGeneratorCartesian(EEStateGenerator): 
    
    defaultFile = (join(EEStateGenerator.genDir, "kuka_cart_tmp.dat"))
    
    # The default end effector state is somewhere close to the initial state 
    # of the robot when the dgm is started.  
    defaultState = [0.24, 0.95, -0.6] + [.5, .0, 2.3, .0] + [.0]*12
    samplingTime = 1e-3
    
    
    def plotFromFile(self, filename=defaultFile):
    
        data = pandas.read_csv(filename, sep='\t')
        data.columns = ['time', 
                        'x', 'y', 'z', 'qw', 'qx', 'qy', 'qz',
                        'v_x', 'v_y', 'v_z', 'w_x', 'w_y', 'w_z',
                        'F_x', 'F_y', 'F_z', 'T_x', 'T_y', 'T_z']
        
        time = data.iloc[:, 0]
        time.columns = ['time']
    
    #         pose = data.iloc[:, 1:8]
    #         pose.columns = ['x', 'y', 'z', 'qw', 'qx', 'qy', 'qz']
        
        plt.figure(1)
        ax1 = plt.gca()
        ax1.set_title('pose')
        ax1.grid()
        data.plot('time', ['x', 'y', 'z', 'qw', 'qx', 'qy', 'qz'], 
                  style=['r-', 'g-', 'b-', 'k--', 'r--', 'g--', 'b--'], ax=ax1)
    
    #         twist = data.iloc[:, 8:14]
    #         twist.columns = ['lin_x', 'lin_y', 'lin_z', 'ang_x', 'ang_y', 'ang_z']
        
        plt.figure(2)
        ax2 = plt.gca()
        ax2.set_title('twist')
        ax2.grid()
        data.plot('time', ['v_x', 'v_y', 'v_z', 'w_x', 'w_y', 'w_z'],
                  style=['r-', 'g-', 'b-', 'r--', 'g--', 'b--'], ax=ax2)
        
    #         wrench = data.iloc[:, 14:20]
    #         wrench.columns = ['lin_x', 'lin_y', 'lin_z', 'ang_x', 'ang_y', 'ang_z']
        
        plt.figure(3)
        ax3 = plt.gca()
        ax3.set_title('wrench')
        ax3.grid()
        data.plot('time', ['F_x', 'F_y', 'F_z', 'T_x', 'T_y', 'T_z'],
                  style=['r-', 'g-', 'b-', 'r--', 'g--', 'b--'], ax=ax3)
    
        plt.show()
#     pdb.set_trace()
    
    
    def exampleForceSaw(self, start=defaultState):

        filename = "cart_force_saw.dat"
    
    #        end = (
    #            [radians(0), radians(30), radians(0), radians(-105), radians(0), radians(30), radians(0)] +
    #            [.0, .0, -20.0, .0, .0, .0])
    
        flankTime = 4.0; # sec
        deltaX = 0.2; # m
        
        # Generate pose
        startP = np.array(start[0:7])
        endP = np.copy(startP)
        endP[0] = endP[0] - deltaX
        
        sigsawPos = np.concatenate(
            (self.genRamp(startP, endP, flankTime),
             self.genRamp(endP, startP, flankTime),
             self.genRamp(startP, endP, flankTime),
             self.genRamp(endP, startP, flankTime)))
        
        numRows = sigsawPos.shape[0]
        
        # Generate twist
        startT = np.array(start[7:13])
        startT[0] += deltaX / flankTime
        velP = np.copy(startT)
        
        constTwist = np.concatenate(
            (self.genConst(-velP, flankTime),
             self.genConst(+velP, flankTime),
             self.genConst(-velP, flankTime),
             self.genConst(+velP, flankTime)))
        
#         constTwist = np.matlib.repmat(startT, numRows, 1)
        
        # Generate wrench
        startW = np.array(start[13:19])
        endW = np.copy(startW)
        endW[2] = endW[2] + 20
        
        sigsawWrench = np.concatenate(
            (self.genRamp(startW, endW, flankTime),
             self.genRamp(endW, startW, flankTime),
             self.genRamp(startW, endW, flankTime),
             self.genRamp(endW, startW, flankTime)))
        
        
        traj = self.addTime(np.concatenate(
            (sigsawPos, constTwist, sigsawWrench), 1))
        
#         sigsawWrench =  (
#             self.genRamp(start, end, flankTime) +
#             self.genRamp(end, start, flankTime) + 
#             self.genRamp(start, end, flankTime) + 
#             self.genRamp(end, start, flankTime))
    
        filepath = join(self.genDir, filename)
        
        self.writeToFile(traj, filepath)

        return filepath


        
class EEStateGeneratorWrenchMode(EEStateGenerator):
    
    genDir = join(rospkg.RosPack().get_path("ct_kuka_user"), "python/gen");

    defaultFile = (join(genDir, "kuka_wrench_tmp.dat"))

    # The default control commands the joint positions defined as 'home'
    # in the dgm startup file of the java application plus zero EE wrench.
    defaultState = (
        [radians(0), radians(30), radians(0), radians(-105), radians(0), radians(30), radians(0)] +
        [.0, .0, .0, .0, .0, .0])

    samplingTime = 1e-3
        
        
    def plotFromFile(self, filename=defaultFile):
        
        data = pandas.read_csv(filename, sep='\t')
    
        pos = data.iloc[:, 0:7]
        pos.columns = ['q1', 'q2', 'q3', 'q4', 'q5', 'q6', 'q7']
        
        plt.figure(1)
        ax1 = plt.gca()
        ax1.set_title('Jopint Positions')
        ax1.grid()
        pos.plot(style=['r-', 'g-', 'b-', 'c-', 'y-', 'm-', 'k-'], ax=ax1)
        
        wrench = data.iloc[:, 7:13]
        wrench.columns = ['lin_x', 'lin_y', 'lin_z', 'ang_x', 'ang_y', 'ang_z']
        
        plt.figure(2)
        ax2 = plt.gca()
        ax2.set_title('EE Wrench')
        ax2.grid()
        wrench.plot(style=['r-', 'g-', 'b-', 'r--', 'g--', 'b--'], ax=ax2)
    
        plt.show()
    #     pdb.set_trace()
    
    
    
    def genRamp(self, start, end, deltaT):
    # Generate ramp with linear slope between two numpy vectors

        start = np.array(start)
        end = np.array(end)
        
        # TODO make consistent rounding scheme
        numSamples = ceil(deltaT / self.samplingTime)
            
        ramp = np.linspace(start, end, numSamples)
    
        return list(map(list, ramp))
    
    
    def exampleSawPosWrench(self, start=defaultState):

        filename = "iiwa_wrench_sigsaw.dat"
            
        end = (
            [radians(0), radians(40), radians(0), radians(-85), radians(0), radians(10), radians(0)] +
            [.0, .0, -10.0, .0, .0, .0])
        
        flankTime = 4; # sec

	#pdb.set_trace()
        
        # Generate position trajectory
        sigsawPos =  (
            self.genRamp(start[:7], end[:7], flankTime) +
            self.genRamp(end[:7], start[:7], flankTime) + 
            self.genRamp(start[:7], end[:7], flankTime) + 
            self.genRamp(end[:7], start[:7], flankTime))
            
	#pdb.set_trace()

	# Generate wrench trajecotry
	sigsawWrench = (
            [[.0]*6]*int(ceil(flankTime*2/self.samplingTime)) +
            self.genRamp(start[7:], end[7:], flankTime) +
            self.genRamp(end[7:], start[7:], flankTime))



	# Combine both trajectories
        sigsaw = np.concatenate((np.array(sigsawPos), np.array(sigsawWrench)), axis=1)

        filepath = join(self.enDir, filename)
        
        self.writeToFile(sigsaw, filepath)

        return filepath
        

    def exampleSawForce(self, start=defaultState):

        filename = "kuka_wrench_sigsaw.dat"

        end = start[:]
        end[-4] = -20

#        end = (
#            [radians(0), radians(30), radians(0), radians(-105), radians(0), radians(30), radians(0)] +
#            [.0, .0, -20.0, .0, .0, .0])

        flankTime = 4; # sec

        sigsawWrench =  (
            self.genRamp(start, end, flankTime) +
            self.genRamp(end, start, flankTime) + 
            self.genRamp(start, end, flankTime) + 
            self.genRamp(end, start, flankTime))

        filepath = join(self.genDir, filename)
        
        self.writeToFile(sigsawWrench, filepath)

        return filepath
     
