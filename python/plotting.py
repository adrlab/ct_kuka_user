# plotting.py Plot the trajectories of the data recorder.

#import matplotlib
#matplotlib.use('Agg') # Make sure display works

import matplotlib.pyplot as plt
import numpy as np
import pandas
import pdb


def plotFile(filename):

	data = pandas.read_csv(filename, delim_whitespace=True)

	keysValid = data.keys().drop('%')

	dValid = data.drop(data.keys()[-1], axis=1)
	dValid.columns=keysValid 

	# Build time axis in sec
	time = dValid.ZeitInSec - dValid.ZeitInSec[0]
	time += (dValid.ZeitInNanoSec - dValid.ZeitInNanoSec[0]) * 1e-9

	plt.figure(1, [20,13])
	ax1 = plt.gca()
	ax1.set_title('Position')
	
	ax1.plot(time, dValid.CartPosMsr1_X, 'r')
	ax1.plot(time, dValid.CartPosMsr1_Y, 'g')
	ax1.plot(time, dValid.CartPosMsr1_Z, 'b')

	ax1.legend()
	ax1.grid()
	plt.xlabel('time [sec]')
	plt.ylabel('[m]')

	plt.figure(2, [20,12])
	ax2 = plt.gca()
	ax2.set_title('Force')
	plt.xlabel('time [sec]')
	plt.ylabel('[N]')
		
	ax2.plot(time, dValid.cartForce1_X, 'r')
	ax2.plot(time, dValid.cartForce1_Y, 'g')
	ax2.plot(time, dValid.cartForce1_Z, 'b')
	
	ax2.legend()
	ax2.grid()

	plt.figure(3)
	ax3 = plt.gca()
	ax3.set_title('Force Variance')
				
	ax3.plot(time, dValid.cartForceVar1_X, 'r')
	ax3.plot(time, dValid.cartForceVar1_Y, 'g')
	ax3.plot(time, dValid.cartForceVar1_Z, 'b')
	
	ax3.legend()
	ax3.grid()

        plt.figure(1)
        ax4 = plt.gca()
       
        ax4.plot(time, dValid.CartPosCmd1_X, 'r--')
        ax4.plot(time, dValid.CartPosCmd1_Y, 'g--')
        ax4.plot(time, dValid.CartPosCmd1_Z, 'b--')
 
        ax4.legend()

	plt.show()


#plotFile('foo.log')


