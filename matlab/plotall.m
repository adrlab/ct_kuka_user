% Plot trajectories stored as mat file

M = load('../log/solver/iiwa_nloc_goto.mat');

f1 = figure;
joints = {M.joint1, M.joint2, M.joint3, M.joint4, M.joint5, M.joint6, M.joint7};
plotSeries(joints, M.t);
f1.Children(end).Title.String = 'Joint Position';
f1.Position = [10 20 550 900];

f2 = figure;
jointVels = {M.jointVel1, M.jointVel2, M.jointVel3, M.jointVel4, M.jointVel5, M.jointVel6, M.jointVel7};
plotSeries(jointVels, M.t);
f2.Children(end).Title.String = 'Joint Velocity';
f2.Position = [570 20 550 900];

f3 = figure;
controls = {M.control1, M.control2, M.control3, M.control4, M.control5, M.control6, M.control7};
plotSeries(controls, M.t);
f3.Children(end).Title.String = 'Control input (joint torque)';
f3.Position = [1130 20 550 900];

f4 = figure;
forces = {M.eeforceX, M.eeforceY, M.eeforceZ};
plotSeries(forces, M.t);
f4.Children(end).Title.String = 'EE force';
f4.Position = [1130 20 550 500];
set(f4.Children, 'XTick', linspace(M.t(1), M.t(end), 20));

f5 = figure;
poses = {M.eeposx, M.eeposy, M.eeposz};
plotSeries(poses, M.t);
f5.Children(end).Title.String = 'EE pose';
f5.Position = [1130 530 550 500];
set(f5.Children, 'XTick', linspace(M.t(1), M.t(end), 20));

f6 = figure;
poses = {M.eevelx, M.eevely, M.eevelz};
plotSeries(poses, M.t);
f6.Children(end).Title.String = 'EE velocity';
f6.Position = [1130 530 550 500];
set(f6.Children, 'XTick', linspace(M.t(1), M.t(end), 20));

% % f7 = figure;
% % plot(M.t, M.workCostIntegral);
% % grid on;
% % f7.Children(end).Title.String = 'Work cost integral';
% % f7.Position = [1130 40 550 300];

% if nargin > 1
% 	figs = get(groot, 'Children');
% 	figs = [f1 f2 f3 f4 f5 f6];
% 	figsNo = 1:length(figs);
% 	
% 	close(figs(setdiff(figsNo, plotNo)));
% end


function plotSeries(timeSeries, time)
    n = length(timeSeries);

    for i=1:n
        subplot(n, 1, i);
        fig = plot(time, timeSeries{i});
        ylabel(sprintf('%u', i));
        grid on;
    end
end

