% Plot log file

clear all;
close all;

% Determine most recent log file
% logfile = getlatestfile('./', '.mat');

[~, res] = system('ls ../log/solver/*.mat -t1 | sed -n 1p');
logfile = strtrim(res);
load(logfile);

% load Log12.mat;

t = squeeze(t);

% Joint positions
f1 = figure;
f1.Name = 'joint positions';
subplot(6, 1, 1);
plot(t, x(1,:));
subplot(6, 1, 2);
plot(t, x(2,:));
subplot(6, 1, 3);
plot(t, x(3,:));
subplot(6, 1, 4);
plot(t, x(4,:));
subplot(6, 1, 5);
plot(t, x(5,:));
subplot(6, 1, 6);
plot(t, x(6,:));


f2 = figure;
f2.Name = 'joint velocities';
subplot(6, 1, 1);
plot(t, x(7,:));
subplot(6, 1, 2);
plot(t, x(8,:));
subplot(6, 1, 3);
plot(t, x(9,:));
subplot(6, 1, 4);
plot(t, x(10,:));
subplot(6, 1, 5);
plot(t, x(11,:));
subplot(6, 1, 6);
plot(t, x(12,:));

t = t(1:end-1);

f2 = figure;
f2.Name = 'joint torques';
subplot(6, 1, 1);
plot(t, u_ff(1,:));
subplot(6, 1, 2);
plot(t, u_ff(2,:));
subplot(6, 1, 3);
plot(t, u_ff(3,:));
subplot(6, 1, 4);
plot(t, u_ff(4,:));
subplot(6, 1, 5);
plot(t, u_ff(5,:));
subplot(6, 1, 6);
plot(t, u_ff(6,:));


