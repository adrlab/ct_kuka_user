/*
 * KukaOptcon.cpp
 *
 *  Created on: Jul 24, 2020
 *      Author: johannes
 */

#include <ct/kuka/KukaOptcon.h>

template class ct::core::LinearSystem<ct::kuka::state_dim, ct::kuka::control_dim, double>;

template class ct::optcon::CostFunctionAD<ct::kuka::state_dim, ct::kuka::control_dim, double>;
template class ct::optcon::TermQuadratic<ct::kuka::state_dim, ct::kuka::control_dim, double, double>;
template class ct::optcon::TermQuadratic<ct::kuka::state_dim, ct::kuka::control_dim, double, ct::kuka::ScalarCG>;




