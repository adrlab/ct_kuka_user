
#include <kindr/Core>

#include <ct/iiwa/iiwa.h>

#include <kuka/FixBaseFDSystem.h>
#include <kuka/TermFrictionWork.h>
#include <kuka/EEContactModel.h>


using namespace ct::rbd;
using namespace ct::core;
using namespace std;


int main(int argc, char* argv[])
{
	typedef double Scalar;
	typedef CppAD::AD<CppAD::cg::CG<double>> ScalarAD;

	// AD types:
	typedef ct::rbd::iiwa::tpl::Dynamics<ScalarAD> IiwaDynamicsAD;
    typedef ct::kuka::FixBaseFDSystem<IiwaDynamicsAD> IiwaSystemAD;
    typedef ct::kuka::EEContactModel<typename IiwaDynamicsAD::Kinematics_t> ContactModelAD;

    // Standard types
	typedef ct::rbd::iiwa::tpl::Dynamics<Scalar> IiwaDynamics;
    typedef ct::kuka::FixBaseFDSystem<IiwaDynamics, 0, false> IiwaSystem;
    typedef ct::kuka::EEContactModel<typename IiwaDynamics::Kinematics_t> ContactModel;

    static const size_t n_joints = IiwaSystem::NJOINTS;
    static const size_t state_dim = IiwaSystem::STATE_DIM;
    static const size_t control_dim = IiwaSystem::CONTROL_DIM;

    typedef ct::core::LinearSystem<state_dim, control_dim, Scalar> LinearSystem_t;

    using CostFunctionAD_t = ct::optcon::CostFunctionAD<state_dim, control_dim, Scalar>;
    using TermQuadraticAD_t = ct::optcon::TermQuadratic<state_dim, control_dim, Scalar, ScalarAD>;

    typedef ct::core::StateMatrix<state_dim, double> A_type;
    typedef ct::core::StateControlMatrix<state_dim, control_dim, double> B_type;

    const std::string workingDirectory = "/home/johannes/repos/control/src/ct_kuka_user/config/iiwa";
    const std::string contactFile = workingDirectory + "/sim/contact.info";
    const std::string costFunctionFile = workingDirectory + "/nloc/scrape/cost.info";
    const std::string settingsFile = workingDirectory + "/nloc/scrape/solver.info";


    // Create contact models
    double k, d, alpha_k, alpha_s, zOffset, mu;

//    ct::core::loadScalar(contactFile, "spring_constant", k);
//    ct::core::loadScalar(contactFile, "damping_constant", d);
//    ct::core::loadScalar(contactFile, "alpha_k", alpha_k);
//    ct::core::loadScalar(contactFile, "alpha_s", alpha_s);
//    ct::core::loadScalar(contactFile, "ground_offset", zOffset);
//    ct::core::loadScalar(contactFile, "friction_coefficient", mu);

    std::shared_ptr<ContactModelAD> contactModelAD(new ContactModelAD);
    std::shared_ptr<ContactModel> contactModel(new ContactModel);

    contactModelAD->loadParameters(contactFile);
    contactModel->loadParameters(contactFile);

//    contactModelAD->k() = k;
//    contactModelAD->d() = d;
//    contactModelAD->alpha_k() = alpha_k;
//    contactModelAD->alpha_s() = alpha_s;
//    contactModelAD->zOffset() = zOffset;
//    contactModelAD->mu() = mu;
//
//    contactModel->k() = k;
//    contactModel->d() = d;
//    contactModel->alpha_k() = alpha_k;
//    contactModel->alpha_s() = alpha_s;
//    contactModel->zOffset() = zOffset;
//    contactModel->mu() = mu;

//    // Create robot systems
//    std::shared_ptr<IiwaSystemAD> iiwaAD_(new IiwaSystemAD(contactModelAD));
//    std::shared_ptr<IiwaSystem> iiwa_(new IiwaSystem(contactModel));
//
//    std::shared_ptr<ct::core::ADCodegenLinearizer<state_dim, control_dim, Scalar>> iiwalinearizer_(
//    		new ct::core::ADCodegenLinearizer<state_dim, control_dim, Scalar>(iiwaAD_));;

    ct::core::StateVector<state_dim> x;
    ct::core::ControlVector<control_dim> u;

    x.setZero();
    u.setZero();
    double t = 0.0;

    x[0] = 0.0;
    x[1] = 1.0;
    x[2] = 0.0;
    x[3] = -2.0;
    x[4] = 0.0;
    x[5] = 0.0;
    x[6] = 0.0;

    x[7] = 0.03;

////    iiwalinearizer_->generateCode("Test");
//    iiwalinearizer_->compileJIT();
//
//    A_type A = iiwalinearizer_->getDerivativeState(x, u, t);
//    B_type B = iiwalinearizer_->getDerivativeControl(x, u, t);
//
//    cout << "A:\n" << A << endl;
//    cout << "B:\n" << B << endl;

    IiwaDynamics::Kinematics_t kinematics = IiwaDynamics::Kinematics_t();
    IiwaSystem::FixBaseRobotState_t fbrState = IiwaSystem::FixBaseRobotState_t(x);

    ct::rbd::RBDState<n_joints, double> state = fbrState.toRBDState();

    ContactModel::Position3S eePos = kinematics.getEEPositionInWorld(0, state.basePose(), state.jointPositions());
    ContactModel::Velocity3S eeVelocity = kinematics.getEEVelocityInWorld(0, state);

    cout << "EE pos: " << eePos.toImplementation() << endl;
    cout << "EE vel: " << eeVelocity.toImplementation() << endl;

    ContactModel::EEForcesLinear eeForces = contactModel->computeContactForces(state);

    cout << "EE force: " << eeForces[0] << endl;

    // Set up cost function
    shared_ptr<CostFunctionAD_t> cost(new CostFunctionAD_t());

    shared_ptr<TermQuadraticAD_t> termQuadAD(new TermQuadraticAD_t());
    termQuadAD->loadConfigFile(costFunctionFile, "term0");

    shared_ptr<ct::kuka::TermFrictionWork> termWorkAD(
    		new ct::kuka::TermFrictionWork(contactFile, "TermFrictionWork", 100));

//				1.0, k, d, alpha_k, alpha_s, zOffset, mu, "test_work_term"));

//    cost->addIntermediateADTerm(termQuadAD, true);
    cost->addIntermediateADTerm(termWorkAD, true);
    cost->setCurrentStateAndControl(x, u);
    cost->initialize();

    cout << "Cost evaluation " << cost->evaluateIntermediate() << endl;
    cout << "State derivative cost \n" << cost->stateDerivativeIntermediate() << endl;
    cout << "State second derivative cost \n" << cost->stateSecondDerivativeIntermediate() << endl;
    cout << "Control derivative cost \n" << cost->controlDerivativeIntermediate() << endl;
    cout << "Control second derivative cost \n" << cost->controlSecondDerivativeIntermediate() << endl;
    cout << "State control derivative cost \n" << cost->stateControlDerivativeIntermediate() << endl;

/*
    // Set-up NLOC solver
//    ct::core::ADCodegenLinearizer<state_dim, control_dim, Scalar> iiwaLinearizer

    std::shared_ptr<LinearSystem_t> iiwaLin_ = iiwalinearizer_;

//    std::shared_ptr<LinearSystem_t> iiwaLin_(
//    		new ct::core::ADCodegenLinearizer<state_dim, control_dim, Scalar>(iiwaAD_));

    iiwalinearizer_->compileJIT();

//    std::shared_ptr<CostFunctionAD_t> costPtr(&cost);

    ct::optcon::NLOptConSettings nloc_settings = ct::optcon::NLOptConSettings::fromConfigFile(settingsFile);

    FixBaseNLOC<IiwaSystem> nloc_solver = FixBaseNLOC<IiwaSystem>(cost, nloc_settings, iiwa_, true, iiwaLin_);

    ct::core::Time timeHorizon = 1.0;
    FixBaseNLOC<IiwaSystem>::RobotState_t q;
    FeedbackMatrix<state_dim, control_dim> Kfb;

    FixBaseNLOC<IiwaSystem>::StateVectorArray x_ref = FixBaseNLOC<IiwaSystem>::StateVectorArray(timeHorizon, x.setZero());
    FixBaseNLOC<IiwaSystem>::FeedbackArray u_fb = FixBaseNLOC<IiwaSystem>::FeedbackArray(timeHorizon, Kfb.setZero());
    FixBaseNLOC<IiwaSystem>::ControlVectorArray u_ff = FixBaseNLOC<IiwaSystem>::ControlVectorArray(timeHorizon, u.setZero());

//    nloc_solver.initialize(q, timeHorizon); //, x_ref, u_fb, u_ff);

    q.setZero();
    u.setZero();

    nloc_solver.initializeSteadyPose(q, timeHorizon, nloc_solver.getSettings().computeK(timeHorizon), u);
    nloc_solver.solve();

    cout << "Solution State: \n";

    for (int i=0; i<nloc_solver.getSolution().x_ref().size(); i++)
    {
    	x = nloc_solver.getSolution().x_ref()[i];
    	IiwaSystem::Force contactForce;
    	iiwa_->computeContactForceInWorld(x, contactForce);

    	cout << x.transpose() << "\t";
    	cout << contactForce.transpose() << endl;
    }
*/
}

