
/* iiwaSim.cpp Simulation of the iiwa arm falling down onto the ground.
 *
 * TODO Refactor
 */


#include <ct/ros/ros.h>

#include <ct/visualizeTrajectory.h>

#include <ct/iiwa/IiwaSystem.h> 	// get robot model double types
#include <ct/kuka/KukaPlotter.h>


using namespace ct::core;
using namespace ct::rbd;
using namespace std;

int main(int argc, char* argv[])
{
    try
    {

    typedef typename ct::iiwa::System::FixBaseRobotState_t RobotState;

    const size_t state_dim = ct::iiwa::System::STATE_DIM;
    const size_t control_dim = ct::iiwa::System::CONTROL_DIM;
    const size_t n_joints = ct::iiwa::System::NJOINTS;

    // Set base pose
	ct::rbd::tpl::RigidBodyPose<double> basePose = ct::rbd::tpl::RigidBodyPose<double>();
	basePose.position() = ct::rbd::tpl::RigidBodyPose<double>::Position3Tpl(0.2, 0.0, 0.0);
	basePose.setFromEulerAnglesXyz(ct::rbd::tpl::RigidBodyPose<double>::Vector3Tpl(M_PI, 0.0, 0.0));

    // Instantiate iiwa dynamics and contact model
    shared_ptr<ct::iiwa::ContactModel> contactModel(new ct::iiwa::ContactModel);
    shared_ptr<ct::iiwa::System> iiwaRobot(new ct::iiwa::System(contactModel));

    ct::iiwa::Dynamics::Kinematics_t iiwaKin = ct::iiwa::Dynamics::Kinematics_t();

    // Initialize ROS
    ros::init(argc, argv, "iiwa_user");
    ros::NodeHandle nh("~");

    std::string configDirectory;
    if (!nh.getParam("config_directory", configDirectory))
    	throw(std::runtime_error("Parameter 'config_directory' not set"));

    const std::string contactFile = configDirectory + "/contact.info";

    contactModel->loadParameters(contactFile);

//    Eigen::Matrix<double, n_joints, 1> damping;
//    ct::core::loadMatrix(contactFile, "joint_damping", damping);
//    iiwaRobot->setJointDamping(damping);

    // create an integrator
    Integrator<state_dim> integrator(iiwaRobot);

	StateVectorArray<state_dim, double> x_k;
	ct::core::tpl::TimeArray<double> t_k;

    ct::iiwa::System::state_vector_t x;
    ct::iiwa::Dynamics::JointState_t q;

    // Set all joints to zero except for joint 1
    q.setZero();
    q.getPosition(1) = M_PI/2;
    q.getPosition(5) = 0.0;
    q.getPosition(0) = 0.0;

    x = ct::iiwa::System::FixBaseRobotState_t(q).toStateVector();

    // simulate n steps
    Time t_0 = 0.0;
    double dt = 1e-5;
    double N = 10/dt;
    integrator.integrate_n_steps(x, t_0, N, dt, x_k, t_k);

    // print the new state
    cout << "State after the simulation:\n";
    cout << "pos " << x.head(n_joints).transpose() << endl;
    cout << "vel " << x.tail(n_joints).transpose() << endl;

    // Compute cartesian quantities
    typename ct::iiwa::Dynamics::Kinematics_t::EEForceLinear w_wrench, Lee_wrench;

    DiscreteArray<typename ct::iiwa::Dynamics::Kinematics_t::EEForceLinear> contactForce(N);
    DiscreteArray<Eigen::Matrix<double, 3, 1>> positions(N);
    DiscreteArray<Eigen::Matrix<double, 3, 1>> velocities(N);

    typename RobotState::RBDState_t rbdstate;
    typename ct::iiwa::ContactModel::EEForcesLinear w_contactForces;

    for (size_t i = 0; i < N; i++)
    {
    	rbdstate = RobotState::rbdStateFromVector(x_k[i], basePose);
//    	rbdstate.basePose() = basePose;

//    	contactForce = contactModel->computeContactForces(rbdstate)[0];
    	iiwaRobot->computeContactForceInWorld(x_k[i], contactForce[i]);

//		rbdstate = iiwaState::rbdStateFromVector(x, basePose);
//		rbdstate.basePose() = basePose;

    	positions[i] = iiwaKin.getEEPositionInWorld(0, basePose, rbdstate.jointPositions()).toImplementation();
    	velocities[i] = iiwaKin.getEEVelocityInWorld(0, rbdstate).toImplementation();
    }

    ControlVectorArray<control_dim> u_k = ControlVectorArray<control_dim>(N);
    DiscreteArray<double> workCost = DiscreteArray<double>(N);
    u_k.setConstant(ct::iiwa::System::control_vector_t::Zero());
    workCost.setConstant(0.0);

    ct::core::DiscreteArray<ct::rbd::SpatialForceVector<double>> contactForces(N);
    contactForces.setConstant(ct::rbd::SpatialForceVector<double>().setZero());

    ct::kuka::KukaPlotter plotter;

    plotter.plotJointPositions(t_k, x_k);
    plotter.plotJointVelocities(t_k, x_k);
    plotter.plotEEStates(t_k, x_k, iiwaRobot, basePose);

    ct::core::plot::show(false);

    // Visualize trajectory using ROS
    ct::ros::RBDStatePublisher statePublisher(ct::models::iiwa::urdfJointNames(), "iiwa/iiwaBase", "/world");
    statePublisher.advertise(nh, "/current_joint_states", 10);

    thread visThread;

    while (ros::ok())
    {
		ros::spinOnce();

		if (visThread.joinable())
			visThread.join();

		ROS_INFO("Visualizing");
		visThread = thread(
				visualizeTrajectory<ct::iiwa::Dynamics::Kinematics_t>,
				std::ref(statePublisher),
				std::ref(x_k),
				std::ref(contactForces),
				std::ref(basePose),
				dt);
    }


    } catch (runtime_error& e)
    {
        cout << "Exception caught: " << e.what() << endl;
    }
}




