/*
 * IiwaSystem.cpp
 *
 *  Created on: Jul 23, 2020
 *      Author: johannes
 */

#include <ct/iiwa/IiwaSystem.h>

//template class IiwaDynamics;
//template class IiwaSystem;
//template class IiwaContactModel;

//template class ct::rbd::EndEffector<ct::iiwa::Dynamics::NJOINTS, double>;
//template class ct::rbd::SelectionMatrix<ct::iiwa::System::STATE_DIM, ct::iiwa::System::CONTROL_DIM, double>;

template class ct::kuka::FixBaseFDSystem<ct::iiwa::Dynamics>;
template class ct::kuka::EEContactModel<typename ct::iiwa::Dynamics::Kinematics_t>;

//template class ct::rbd::FixBaseNLOC<ct::iiwa::System>; // TODO debug
