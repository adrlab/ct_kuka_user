/**********************************************************************************************************************
This file is part of the Control Toolbox (https://adrlab.bitbucket.io/ct), copyright by ETH Zurich, Google Inc.
Licensed under Apache2 license (see LICENSE file in main directory)
**********************************************************************************************************************/


#include <ct/core/core.h>
#include <ct/optcon/optcon.h>
#include <ct/rbd/rbd.h>

//#include <cmath>
//#include <memory>

#include <ct/iiwa/iiwa.h>
#include <ct/iiwa/pathNames.h>

#include <ct/kuka/FixBaseFDSystem.h>
//#include <ct/kuka/EEContactModel.h>
#include <ct/kuka/KukaCodegenLinearizer.h>
#include <ct/kuka/pathNames.h>


typedef CppAD::AD< CppAD::cg::CG<double>> ADCGScalar;

const size_t state_dim = ct::kuka::FixBaseFDSystem<ct::rbd::iiwa::tpl::Dynamics<ADCGScalar>>::STATE_DIM;
const size_t control_dim = ct::kuka::FixBaseFDSystem<ct::rbd::iiwa::tpl::Dynamics<ADCGScalar>>::CONTROL_DIM;
const size_t njoints = ct::rbd::iiwa::tpl::Dynamics<ADCGScalar>::NJOINTS;

template <typename SCALAR>
using control_vector_t = typename ct::rbd::iiwa::tpl::Dynamics<SCALAR>::control_vector_t;

template <typename SCALAR>
using ExtLinkForces_t = typename ct::rbd::iiwa::tpl::Dynamics<SCALAR>::ExtLinkForces_t;



int main(int argc, char** argv)
{
	typedef ct::kuka::FixBaseFDSystem<ct::rbd::iiwa::tpl::Dynamics<ADCGScalar>> IiwaNonLinearSystemAD;

	// a contact model (auto-diff'able)
    typedef ct::kuka::EEContactModel<typename ct::rbd::iiwa::tpl::Dynamics<ADCGScalar>::Kinematics_t> ContactModelAD;

//	std::shared_ptr<IiwaNonLinearSystem> adSystem = std::shared_ptr<IiwaNonLinearSystem>(new IiwaNonLinearSystem);

	// explicitely pass kinematics from the system such that both, contact model and system use
	// the same instance to give the AD codegen the opportunity to fully optimize the code
	std::shared_ptr<ContactModelAD> contactModel = std::shared_ptr<ContactModelAD>(new ContactModelAD);

	// Generate linear system with these ordinary parameters. These values need to be reused if linear system is used
	// incombination with other contact model dependents.
	contactModel->k() 			= 50.0;
	contactModel->d() 			= 60.0;
	contactModel->alpha_k() 	= 10.0;
	contactModel->alpha_s() 	= 50.0;
	contactModel->mu() 			= 0.1;
	contactModel->eps() 		= 0.01;
	contactModel->zOffset() 	= 0.0;

    std::shared_ptr<IiwaNonLinearSystemAD> iiwa(new IiwaNonLinearSystemAD(contactModel));

    ct::core::ADCodegenLinearizer<state_dim, control_dim> iiwaLinearizer(iiwa);

    ct::kuka::KukaCodegenLinearizer<IiwaNonLinearSystemAD> iiwaCMLinearizer(iiwa);

    try
    {

        std::cout << "Generating linearized dynamics with fixed contact model ... " << std::endl;

        std::cout << "using forward mode" << std::endl;
        iiwaLinearizer.generateCode("IiwaLinearizedForward", ct::iiwa::CODEGEN_OUTPUT_DIR,
            ct::core::CODEGEN_TEMPLATE_DIR, "iiwa", "codegen", false);

        std::cout << "using reverse mode" << std::endl;
        iiwaLinearizer.generateCode("IiwaLinearizedReverse", ct::iiwa::CODEGEN_OUTPUT_DIR,
            ct::core::CODEGEN_TEMPLATE_DIR, "iiwa", "codegen", true);

        std::cout << "Generating linearized dynamics with variable contact model ... " << std::endl;

        std::cout << "using forward mode" << std::endl;
        iiwaCMLinearizer.generateCode("IiwaCMLinearizedForward", ct::iiwa::CODEGEN_OUTPUT_DIR,
        	ct::kuka::CODEGEN_TEMPLATE_DIR, "iiwa", "codegen", false);


    } catch (const std::runtime_error& e)
    {
        std::cout << "Code generation failed: " << e.what() << std::endl;
    }

    return 0;
}
