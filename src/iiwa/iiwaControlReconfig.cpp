
/* iiwaControl.cpp
 *
 * Trajectory optimization of the iiwa arm with soft contact model and Coulomb friction. Using dynamic reconfigure for
 * contact model parameters adjustment.
 *
 *
 */


//#define MATLAB
//#define MATLAB_FULL_LOG

#include <ct/ros/ros.h>

#include <ct/iiwa/IiwaSystem.h>
#include <ct/iiwa/codegen/IiwaCMLinearizedForward.h>

#include <ct/kuka/KukaNloc.h>
#include <ct/kuka/KukaPlotter.h>
#include <ct/visualizeTrajectory.h>

#include <dynamic_reconfigure/server.h>
#include <ct_kuka_user/nlocConfig.h>

using namespace std;
using namespace ct;

using iiwaFullState = typename iiwa::System::FixBaseRobotState_t;
using iiwaRBDState = typename iiwaFullState::RBDState_t;

// get the num of states of the robot
const size_t state_dim = iiwa::System::STATE_DIM;
const size_t control_dim = iiwa::System::CONTROL_DIM;

using LinearSystem_t = core::LinearSystem<state_dim, control_dim, double>;
using InverseKinematicsBase_t = rbd::InverseKinematicsBase<iiwa::System::NJOINTS, double>;
using StateFeedbackController_t = core::StateFeedbackController<state_dim, control_dim, double>;

shared_ptr<ct::kuka::KukaNloc<iiwa::Dynamics>> g_iiwanloc;

bool g_finish = false;
bool g_reset = false;
bool g_solved = false;
bool g_plot = false;
int g_skip = 1;

void callback(ct_kuka_user::nlocConfig& config, uint32_t level);

int main(int argc, char* argv[])
{

    typedef typename iiwa::System::FixBaseRobotState_t iiwaState;

    // Set base orientation
	rbd::tpl::RigidBodyPose<double> basePose = rbd::tpl::RigidBodyPose<double>();
	basePose.position() = rbd::tpl::RigidBodyPose<double>::Position3Tpl(0.0, 0.0, 0.0);
	basePose.setFromEulerAnglesXyz(rbd::tpl::RigidBodyPose<double>::Vector3Tpl(0.0, 0.0, 0.0));

    // Initialize ROS
    ::ros::init(argc, argv, "iiwa_control");
    ::ros::NodeHandle nh("~");

    // Setup NLOC problem
    ROS_INFO("Loading task files");

    string nlocDirectory;
    if (!nh.getParam("nloc_directory", nlocDirectory))
    	throw(runtime_error("Parameter 'nloc_directory' not set"));

    // TODO move these to KukaNloc
    const string configFile = nlocDirectory + "/config.info";
    const string costFunctionFile = nlocDirectory + "/cost.info";
    const string contactFile = nlocDirectory + "/contact.info";

    shared_ptr<iiwa::ContactModel> contactModel(new iiwa::ContactModel());
    contactModel->loadParameters(contactFile);

    shared_ptr<iiwa::System> iiwa(new iiwa::System(contactModel, basePose));

    std::shared_ptr<LinearSystem_t> iiwaLin(new ct::iiwa::codegen::IiwaCMLinearizedForward(contactModel));
//    shared_ptr<LinearSystem_t> iiwaLin(new rbd::RbdLinearizer<iiwa::System>(iiwa));
    shared_ptr<InverseKinematicsBase_t> iiwaIK(new rbd::IiwaInverseKinematics<double>());

    g_iiwanloc = std::shared_ptr<kuka::KukaNloc<iiwa::Dynamics>>(new kuka::KukaNloc<iiwa::Dynamics>(
    		contactFile, configFile, costFunctionFile, contactModel, iiwa, iiwaLin, iiwaIK, true));

    ROS_INFO("Setting up Dynamic Reconfigure");
    dynamic_reconfigure::Server<ct_kuka_user::nlocConfig> cfgServer;
    dynamic_reconfigure::Server<ct_kuka_user::nlocConfig>::CallbackType f;
    f = boost::bind(&callback, _1, _2);
    cfgServer.setCallback(f);

    ROS_INFO("Waiting 1 second for begin");
    this_thread::sleep_for(std::chrono::seconds(1));

    // Visualize trajectory using ROS
    core::DiscreteArray<rbd::SpatialForceVector<double>> contactForces(g_iiwanloc->getNumTimeSteps());
    contactForces.setConstant(rbd::SpatialForceVector<double>().setZero());

    ct::ros::RBDStatePublisher statePublisher(models::iiwa::urdfJointNames(), "iiwa/iiwaBase", "/world");
    statePublisher.advertise(nh, "/current_joint_states", 10);

    kuka::KukaNloc<iiwa::Dynamics>::StateVectorArray_t state;
    StateFeedbackController_t solution;

    std::thread visThread, plotThread;

    bool foundBetter = true;
	size_t iteration = 0;

    while (::ros::ok()) //(iteration < 1)
    {

        if (g_reset)
        {
            ROS_INFO("Resetting NLOC");

            g_iiwanloc->recomputeIkPoses(false);
            g_iiwanloc->reloadCostTerms(false);
            g_iiwanloc->reloadSettings(false);
            g_iiwanloc->initialize();

            g_reset = false;
            g_solved = false;
        }

        if (!g_solved)
        {
            ROS_INFO("Running Iteration");

            try
            {
                foundBetter = g_iiwanloc->getSolver().runIteration();

            } catch (runtime_error& e)
            {
                ROS_ERROR("Could not find solution, error: %s", e.what());
                g_solved = true;
            }

            if (!foundBetter)
            {
                ROS_INFO("NLOC solved");
                g_solved = true;
            }
        }

        if (foundBetter || g_solved)
        	solution = g_iiwanloc->getSolver().getSolution();

        ::ros::spinOnce();

        if (iteration % g_skip == 0)
        {

            if (visThread.joinable())
                visThread.join();

    		ROS_INFO("Visualizing iteration %lu", iteration);
    		visThread = std::thread(
    				visualizeTrajectory<iiwa::Dynamics::Kinematics_t>,
    				std::ref(statePublisher),
    				std::ref(solution.x_ref()),
    				std::ref(contactForces),
    				std::ref(basePose),
    				g_iiwanloc->getNlocSettings().dt);

    		if (g_plot)
    		{
    			kuka::KukaPlotter plotter;

    			plotter.plotEEStates(solution.time(), solution.x_ref(), g_iiwanloc->getSystem(), basePose);
    			ct::core::plot::show(true);
    		}
        }

//        if (iteration > g_iiwanloc->getNlocSettings().max_iterations)
        if (g_finish)
        {
        	visThread.join();
        	break;
        }

        iteration++;
    }

    ROS_INFO("Exit NLOC loop");

    kuka::KukaPlotter plotter;

//	plotter.plotJointPositions(solution.time(), solution.x_ref());
//	plotter.plotJointVelocities(solution.time(), solution.x_ref());
//
//	plotter.plotJointTorques(solution.time(), solution.uff());
//	plotter.plotJointTorques(solution.time(), iiwanloc.getControlRef());

	plotter.plotEEStates(solution.time(), solution.x_ref(), g_iiwanloc->getSystem(), basePose);
	plotter.plotCostTerms(solution.time(), solution.x_ref(), solution.uff(), g_iiwanloc->getIntermediateTerms());

	ct::core::plot::show(false);

    // Kepp visualizing last solution until termination
    while (::ros::ok())
    {
    	ROS_INFO("Visualizing solution");
		visThread = std::thread(
				visualizeTrajectory<iiwa::Dynamics::Kinematics_t>,
				std::ref(statePublisher),
				std::ref(solution.x_ref()),
				std::ref(contactForces),
				std::ref(basePose),
				g_iiwanloc->getNlocSettings().dt);

        if (visThread.joinable())
            visThread.join();
    }

	visThread.join();
	::ros::shutdown();
    return 0;

} // main


void callback(ct_kuka_user::nlocConfig& config, uint32_t level)
{

    if (config.reset)
        g_reset = true;

    if (config.finish)
    	g_finish = true;

    config.reset = false;
    config.finish = false;

    g_skip = config.skip;
    g_plot = config.plot;

    g_iiwanloc->getContactModel()->k() = config.k;
    g_iiwanloc->getContactModel()->d() = config.d;
    g_iiwanloc->getContactModel()->alpha_k() = config.alpha_k;
    g_iiwanloc->getContactModel()->alpha_s() = config.alpha_s;
    g_iiwanloc->getContactModel()->zOffset() = config.zOffset;
    g_iiwanloc->getContactModel()->eps() = config.c;
    g_iiwanloc->getContactModel()->mu() = config.mu;

}

