
/* iiwaControl.cpp
 *
 * Trajectory optimization of the iiwa arm with soft contact model and Coulomb friction.
 *
 *
 */


//#define MATLAB
//#define MATLAB_FULL_LOG

#include <ct/ros/ros.h>

#include <ct/iiwa/IiwaSystem.h>
#include <ct/iiwa/codegen/IiwaCMLinearizedForward.h>

#include <ct/kuka/KukaNloc.h>
#include <ct/kuka/KukaPlotter.h>
#include <ct/visualizeTrajectory.h>

using namespace std;
using namespace ct;

using iiwaFullState = typename iiwa::System::FixBaseRobotState_t;
using iiwaRBDState = typename iiwaFullState::RBDState_t;

// get the num of states of the robot
const size_t state_dim = iiwa::System::STATE_DIM;
const size_t control_dim = iiwa::System::CONTROL_DIM;

using LinearSystem_t = core::LinearSystem<state_dim, control_dim, double>;
using InverseKinematicsBase_t = rbd::InverseKinematicsBase<iiwa::System::NJOINTS, double>;
using StateFeedbackController_t = core::StateFeedbackController<state_dim, control_dim, double>;


int main(int argc, char* argv[])
{
	try
	{

    // Set base orientation
	rbd::tpl::RigidBodyPose<double> basePose = rbd::tpl::RigidBodyPose<double>();
	basePose.position() = rbd::tpl::RigidBodyPose<double>::Position3Tpl(0.0, 0.0, 0.0);
	basePose.setFromEulerAnglesXyz(rbd::tpl::RigidBodyPose<double>::Vector3Tpl(0.0, 0.0, 0.0));

    // Initialize ROS
    ::ros::init(argc, argv, "iiwa_control");
    ::ros::NodeHandle nh("~");

    // Setup NLOC problem
    ROS_INFO("Loading task files");

    string nlocDirectory;
    if (!nh.getParam("nloc_directory", nlocDirectory))
    	throw(runtime_error("Parameter 'nloc_directory' not set"));

    // TODO move these to KukaNloc
    const string configFile = nlocDirectory + "/config.info";
    const string costFunctionFile = nlocDirectory + "/cost.info";
    const string contactFile = nlocDirectory + "/contact.info";

    shared_ptr<iiwa::ContactModel> contactModel(new iiwa::ContactModel());
    contactModel->loadParameters(contactFile);

    shared_ptr<iiwa::System> iiwa(new iiwa::System(contactModel, basePose));

    std::shared_ptr<LinearSystem_t> iiwaLin(new ct::iiwa::codegen::IiwaCMLinearizedForward(contactModel));
//    shared_ptr<LinearSystem_t> iiwaLin(new rbd::RbdLinearizer<iiwa::System>(iiwa));
    shared_ptr<InverseKinematicsBase_t> iiwaIK(new rbd::IiwaInverseKinematics<double>());

    kuka::KukaNloc<iiwa::Dynamics> iiwanloc = kuka::KukaNloc<iiwa::Dynamics>(
    		contactFile, configFile, costFunctionFile, contactModel, iiwa, iiwaLin, iiwaIK, true);

    // Visualize trajectory using ROS
    core::DiscreteArray<rbd::SpatialForceVector<double>> contactForces(iiwanloc.getNumTimeSteps());
    contactForces.setConstant(rbd::SpatialForceVector<double>().setZero());

    ct::ros::RBDStatePublisher statePublisher(models::iiwa::urdfJointNames(), "iiwa/iiwaBase", "/world");
    statePublisher.advertise(nh, "/current_joint_states", 10);

    kuka::KukaNloc<iiwa::Dynamics>::StateVectorArray_t state;
    StateFeedbackController_t solution;

    ROS_INFO("Waiting 1 sec for begin");
    this_thread::sleep_for(std::chrono::seconds(1));

    iiwanloc.getSolver().solve();
    solution = iiwanloc.getSolver().getSolution();

	kuka::KukaPlotter plotter;

//	plotter.plotJointPositions(solution.time(), solution.x_ref());
//	plotter.plotJointVelocities(solution.time(), solution.x_ref());
//
//	plotter.plotJointTorques(solution.time(), solution.uff());
//	plotter.plotJointTorques(solution.time(), iiwanloc.getControlRef());

	plotter.plotEEStates(solution.time(), solution.x_ref(), iiwanloc.getSystem(), basePose);
//	plotter.plotCostTerms(solution.time(), solution.x_ref(), solution.uff(), iiwanloc.getIntermediateTerms());

	core::plot::show(false);

    std::thread visThread;

    while (::ros::ok())
    {
		::ros::spinOnce();

		if (visThread.joinable())
			visThread.join();

		ROS_INFO("Visualizing");
		visThread = std::thread(
				visualizeTrajectory<ct::iiwa::Dynamics::Kinematics_t>,
				std::ref(statePublisher),
				std::ref(solution.x_ref()),
				std::ref(contactForces),
				std::ref(basePose),
				iiwanloc.getNlocSettings().dt);
    }

	} catch (runtime_error& e)
	{
		cout << "Exception caught: " << e.what() << endl;
	}

} // main



