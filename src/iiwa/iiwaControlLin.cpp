
/* iiwaControl.cpp
 * Trajectory optimization of the iiwa arm with soft contact model and Coulomb friction.
 *
 *
 */


//#define MATLAB
//#define MATLAB_FULL_LOG

#include <ct/ros/ros.h>

#include <ct/iiwa/IiwaNLOC.h>
#include <ct/kuka/KukaPlotter.h>

//#include <kuka/FixBaseFDSystem.h>
#include <ct/visualizeTrajectory.h>


using namespace ct::rbd;
using namespace ct::core;
using namespace std;

//using iiwaFullState = typename iiwaSystem::FixBaseRobotState_t;
//using iiwaRBDState = typename iiwaFullState::RBDState_t;

// get the num of states of the robot
const size_t state_dim = IiwaNLOC::state_dim;
const size_t control_dim = IiwaNLOC::control_dim;

//using IiwaDynamics = IiwaNLOC::IiwaDynamics;

using StateFeedbackController_t = StateFeedbackController<state_dim, control_dim, double>;

int main(int argc, char* argv[])
{
	try
	{

//    typedef typename iiwaSystem::FixBaseRobotState_t iiwaState;

    // Set base orientation to match Apollo's base
	ct::rbd::tpl::RigidBodyPose<double> basePose = ct::rbd::tpl::RigidBodyPose<double>();
//	basePose.position() = ct::rbd::tpl::RigidBodyPose<double>::Position3Tpl(0.0, 0.0, 0.0);
//	basePose.setFromEulerAnglesXyz(ct::rbd::tpl::RigidBodyPose<double>::Vector3Tpl(0.0, 0.0, 0.0));


    // Initialize ROS
    ros::init(argc, argv, "iiwa_control");
    ros::NodeHandle nh("~");


    // Setup NLOC problem
    ROS_INFO("Loading config files");

    string nlocDirectory;
    if (!nh.getParam("nloc_directory", nlocDirectory))
    	throw(runtime_error("Parameter 'nloc_directory' not set"));

    const string configFile = nlocDirectory + "/config.info";
    const string costFunctionFile = nlocDirectory + "/cost.info";
    const string contactFile = nlocDirectory + "/contact.info";

    IiwaNLOC iiwanloc = IiwaNLOC(contactFile, configFile, costFunctionFile, basePose);

    ROS_INFO("Waiting 1 sesc for begin");
    this_thread::sleep_for(std::chrono::seconds(1));

    iiwanloc.getSolver().solve();

    StateFeedbackController_t solution = iiwanloc.getSolver().getSolution();

    ct::kuka::KukaPlotter plotter;

//    plotter.plotJointPositions(solution.time(), solution.x_ref());
//    plotter.plotJointVelocities(solution.time(), solution.x_ref());

//    plotter.plotJointTorques(solution.time(), solution.uff());
//    plotter.plotJointTorques(solution.time(), iiwanloc.getControlRef());

    plotter.plotEEStates(solution.time(), solution.x_ref(), iiwanloc.getSystem(), basePose);
//    plotter.plotCostTerms(solution.time(), solution.x_ref(), solution.uff(), iiwanloc.getIntermediateTerms());

    ct::core::plot::show(false);

    // Visualize trajectory using ROS
    ct::core::DiscreteArray<ct::rbd::SpatialForceVector<double>> contactForces(solution.x_ref().size());
    contactForces.setConstant(ct::rbd::SpatialForceVector<double>().setZero());
    ct::ros::RBDStatePublisher statePublisher(ct::models::iiwa::urdfJointNames(), "iiwa/iiwaBase", "/world");
    statePublisher.advertise(nh, "/current_joint_states", 10);

    std::thread visThread;

    while (ros::ok())
    {
		ros::spinOnce();

		if (visThread.joinable())
			visThread.join();

		ROS_INFO("Visualizing");
		visThread = std::thread(
				visualizeTrajectory<ct::iiwa::Dynamics::Kinematics_t>,
				std::ref(statePublisher),
				std::ref(solution.x_ref()),
				std::ref(contactForces),
				std::ref(basePose),
				iiwanloc.getNLOCSettings().dt);
    }

	} catch (runtime_error& e)
	{
		cout << "Exception caught: " << e.what() << endl;
	}
}
