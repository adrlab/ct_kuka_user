
/* lwr4Control.cpp
 * Trajectory optimization of the lwr4 arm with soft contact model and Coulomb friction.
 *
 *
 */

//#define MATLAB
//#define MATLAB_FULL_LOG

#include <ct/ros/ros.h>

//#include <ct/kuka/KukaNloc.h>
#include <ct/lwr4/Lwr4NLOC.h>

#include <ct/kuka/FixBaseFDSystem.h>
#include <ct/kuka/KukaPlotter.h>

#include <ct/visualizeTrajectory.h>

#include <ct/lwr4/basePose.h>
#include <ct/lwr4/lwr4.h>

using namespace ct::rbd;
using namespace ct::core;
using namespace std;

using Lwr4Dynamics = ct::rbd::lwr4::tpl::Dynamics<double>;
using Lwr4System = ct::kuka::FixBaseFDSystem<Lwr4Dynamics>;
using ContactModel = ct::kuka::EEContactModel<typename Lwr4Dynamics::Kinematics_t>;

using Lwr4FullState = typename Lwr4System::FixBaseRobotState_t;
using Lwr4RBDState = typename Lwr4FullState::RBDState_t;

// get the num of states of the robot
const size_t state_dim = Lwr4System::STATE_DIM;
const size_t control_dim = Lwr4System::CONTROL_DIM;

using StateFeedbackController_t = StateFeedbackController<state_dim, control_dim, double>;


int main(int argc, char* argv[])
{
	try
	{

	ct::rbd::tpl::RigidBodyPose<double> basePose = ct::rbd::tpl::RigidBodyPose<double>();
	basePose.position() = ct::rbd::tpl::RigidBodyPose<double>::Position3Tpl(ct::lwr4::APOLLO_BASE_POSITION_XYZ);
	basePose.setFromEulerAnglesXyz(ct::lwr4::APOLLO_BASE_ORIENTATION_XYZ);

	// Initialize ROS
	ros::init(argc, argv, "lwr4_control");
	ros::NodeHandle nh("~");

	// Setup NLOC problem
	ROS_INFO("Loading config files");

	string nlocDirectory;
	if (!nh.getParam("nloc_directory", nlocDirectory))
		throw(runtime_error("Parameter 'nloc_directory' not set"));

	const string configFile = nlocDirectory + "/config.info";
	const string costFunctionFile = nlocDirectory + "/cost.info";
	const string contactFile = nlocDirectory + "/contact.info";

    Lwr4NLOC lwr4NLOC = Lwr4NLOC(contactFile, configFile, costFunctionFile, basePose);

    ROS_INFO("Waiting 1 second for begin");
	this_thread::sleep_for(std::chrono::seconds(1));

	lwr4NLOC.getSolver().solve();

	StateFeedbackController_t solution = lwr4NLOC.getSolver().getSolution();

    ct::kuka::KukaPlotter plotter;

    plotter.plotJointPositions(solution.time(), solution.x_ref());
    plotter.plotJointVelocities(solution.time(), solution.x_ref());
    plotter.plotJointTorques(solution.time(), solution.uff());
    plotter.plotEEStates(solution.time(), solution.x_ref(), lwr4NLOC.getSystem(), basePose);

    ct::core::plot::show(false);

	// Visualize trajectory using ROS
	ct::core::DiscreteArray<ct::rbd::SpatialForceVector<double>> contactForces(solution.x_ref().size());
	contactForces.setConstant(ct::rbd::SpatialForceVector<double>().setZero());

	ct::ros::RBDStatePublisher statePublisher(ct::models::lwr4::urdfJointNames(), "KUKA/BASE", "/world");
	statePublisher.advertise(nh, "/current_joint_states", 10);

	std::thread visThread;

	while (ros::ok())
	{
		ros::spinOnce();

		if (visThread.joinable())
			visThread.join();

		ROS_INFO("Visualizing");
		visThread = std::thread(
				visualizeTrajectory<Lwr4Dynamics::Kinematics_t>,
				std::ref(statePublisher),
				std::ref(solution.x_ref()),
				std::ref(contactForces),
				std::ref(basePose),
				std::ref(lwr4NLOC.getNLOCSettings().dt));
	}

	} catch (runtime_error& e)
	{
		cout << "Exception caught: " << e.what() << endl;
	}
}
