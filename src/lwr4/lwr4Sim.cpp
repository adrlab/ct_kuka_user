
/* First simulation of the lwr4 robot arm.
 * This executable is adapted from the ct tutorial 'Creating our first executable'.
 *
 *
 */


//#define MATLAB
//#define MATLAB_FULL_LOG

#include <ct/ros/ros.h>
#include <ct/lwr4/lwr4.h>

#include <ct/visualizeTrajectory.h>
#include <ct/kuka/FixBaseFDSystem.h>
#include <ct/kuka/EEContactModel.h>
#include <ct/kuka/KukaPlotter.h>
#include <ct/lwr4/basePose.h>


using namespace ct::rbd;
using namespace std;

int main(int argc, char* argv[])
{
    try
    {
	// Define robot model types
	typedef lwr4::tpl::Dynamics<double> Lwr4Dynamics;
	typedef ct::kuka::FixBaseFDSystem<Lwr4Dynamics> Lwr4System;
	typedef ct::kuka::EEContactModel<typename Lwr4Dynamics::Kinematics_t> ContactModel;

	typedef typename Lwr4System::FixBaseRobotState_t Lwr4FullState;
	typedef Lwr4FullState::RBDState_t Lwr4RBDState;

    const size_t state_dim = Lwr4System::STATE_DIM;
    const size_t control_dim = Lwr4System::CONTROL_DIM;
    const size_t n_joints = Lwr4System::NJOINTS;

    typename Lwr4Dynamics::Kinematics_t lwr4Kin = typename Lwr4Dynamics::Kinematics_t();
    typedef typename Lwr4Dynamics::Kinematics_t::Position3Tpl Position;

    shared_ptr<ContactModel> contactModel(new ContactModel);

	ct::rbd::tpl::RigidBodyPose<double> basePose = ct::rbd::tpl::RigidBodyPose<double>();
	basePose.position() = ct::rbd::tpl::RigidBodyPose<double>::Position3Tpl(ct::lwr4::APOLLO_BASE_POSITION_XYZ);
	basePose.setFromEulerAnglesXyz(ct::lwr4::APOLLO_BASE_ORIENTATION_XYZ);

    shared_ptr<Lwr4System> lwr4Robot(new Lwr4System(contactModel, basePose));

    // Initialize ROS
    ros::init(argc, argv, "lwr4_sim");
    ros::NodeHandle nh("~");

    std::string configDirectory;
    if (!nh.getParam("config_directory", configDirectory))
    	throw(std::runtime_error("Parameter 'config_directory' not set"));

    const std::string contactFile = configDirectory + "/contact.info";

    contactModel->loadParameters(contactFile);

    // create an integrator
    ct::core::Integrator<state_dim> integrator(lwr4Robot);

	ct::core::StateVectorArray<state_dim, double> x_traj;
	ct::core::tpl::TimeArray<double> t_traj;

    Lwr4System::state_vector_t x;
    Lwr4Dynamics::JointState_t state_q;

    // Set initial joint configuration
    state_q.setZero();
    state_q.getPosition(1) = M_PI/2.;
    state_q.getPosition(5) = 0.0;
    state_q.getPosition(0) = -1.0;

    x = Lwr4FullState(state_q).toStateVector();

    cout << "Initial state is " << x.transpose() << endl;

    // simulate n steps
    ct::core::Time t_start = 0.0;
    double n_steps = 5000;
    double dt = 1e-3;
    integrator.integrate_n_steps(x, t_start, n_steps, dt, x_traj, t_traj);

    // print the new state
    cout << "state after integration: " << x.transpose() << endl;

    Lwr4FullState fullState = Lwr4FullState(x);

    // Compute ee pose
    Position eePos = lwr4Kin.getEEPositionInWorld(0, basePose, fullState.joints().getPositions());

    cout << "EE pos: " << eePos.toImplementation().transpose() << endl;

    cout << "EE link id: " << lwr4Kin.getEndEffector(0).getLinkId() << endl;

    typename Lwr4Dynamics::Kinematics_t::EEForce w_force;

    w_force.setZero();
    w_force(0) = 0.03;
    w_force(1) = 0.2;
    w_force(2) = 0.0;
    w_force(3) = 0.01;
    w_force(4) = 0.1;
    w_force(5) = 0.01;

//    ct::core::ControlVectorArray<control_dim> u_zero = ct::core::ControlVectorArray<control_dim>(x_traj.size());
//    u_zero.setConstant(Lwr4System::control_vector_t::Zero());

    // Compute contact forces
    ct::core::DiscreteArray<ct::rbd::SpatialForceVector<double>> contactForce(x_traj.size());

    for (size_t i = 0; i < x_traj.size(); i++)
    {
    	Lwr4RBDState rbdstate = Lwr4FullState::rbdStateFromVector(x_traj[i]);
    	typename ContactModel::EEForcesLinear w_contactForces = contactModel->computeContactForces(rbdstate);
    	typename Lwr4Dynamics::Kinematics_t::EEForce w_wrench, Lee_wrench;

    	w_wrench.setZero();
    	w_wrench.force() = w_contactForces[0];

    	lwr4Robot->computeEEForceInLink(x_traj[i], w_wrench, Lee_wrench);

    	contactForce[i] = Lee_wrench.setZero();
    }

    ct::kuka::KukaPlotter plotter;

    plotter.plotJointPositions(t_traj, x_traj);
    plotter.plotJointVelocities(t_traj, x_traj);
    plotter.plotEEStates(t_traj, x_traj, lwr4Robot, basePose);

    ct::core::plot::show(false);


    // Visualize trajectory using ROS
    ct::ros::RBDStatePublisher statePublisher(ct::models::lwr4::urdfJointNames(), "KUKA/BASE", "/world");
    statePublisher.advertise(nh, "/current_joint_states", 10);

    std::thread visThread;

    while (ros::ok())
    {
        if (visThread.joinable())
            visThread.join();

        ROS_INFO("Visualizing");
        visThread = std::thread(
            visualizeTrajectory<Lwr4Dynamics::Kinematics_t>,
            std::ref(statePublisher),
			std::ref(x_traj),
			std::ref(contactForce),
			std::ref(basePose),
			dt);
    }


    } catch (std::runtime_error& e)
    {
        std::cout << "Exception caught: " << e.what() << std::endl;
    }
}




