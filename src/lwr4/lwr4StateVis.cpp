

#include <ct/lwr4/lwr4.h>
#include <ct/ros/ros.h>

#include <ct/kuka/FixBaseFDSystem.h>
#include <ct/kuka/KukaPlotter.h>
#include <ct/kuka/visualizeTrajectory.h>

#include <ct/lwr4/basePose.h>

using namespace ct::rbd;
using namespace ct::core;
using namespace std;


int main(int argc, char* argv[])
{
  try
    {

	typedef lwr4::tpl::Dynamics<double> Lwr4Dynamics;
	typedef ct::kuka::FixBaseFDSystem<Lwr4Dynamics> Lwr4System;
	typedef ct::kuka::EEContactModel<typename Lwr4Dynamics::Kinematics_t> ContactModel;
	typedef typename Lwr4System::FixBaseRobotState_t iiwaRBDState;

	shared_ptr<ContactModel> contactModel(new ContactModel);

	shared_ptr<Lwr4System> lwr(new Lwr4System(contactModel));

	// Initialize ROS
	ros::init(argc, argv, "lwr4_state_vis");
	ros::NodeHandle nh("~");

	std::string filename;
	if (!nh.getParam("stateFile", filename))
	 std::cout << "Working directory parameter 'workingDirectory' not set" << std::endl;

	typename Lwr4System::state_vector_t state;
	loadMatrix(filename, "x", state);

	// x.setConstant(0.0);
	// x(0) = 0.2;
	// x(1) = M_PI/2.0;
	// x(2) = 0.0;
	// x(3) = 0.0;
	// x(5) = 0.0;
	// x(6) = 0.0;

	ControlVector<Lwr4System::CONTROL_DIM> u = ControlVector<Lwr4System::CONTROL_DIM>();
	u.setConstant(0.0);

	StateVectorArray<Lwr4System::STATE_DIM> x_traj(1, state);
	ControlVectorArray<Lwr4System::CONTROL_DIM> uff(1, u);
	TimeArray time(1, 0.0);

	cout << "x traj [0] " << x_traj[0] << endl;

	ct::rbd::tpl::RigidBodyPose<double> basePose = ct::rbd::tpl::RigidBodyPose<double>();
	basePose.position() = ct::rbd::tpl::RigidBodyPose<double>::Position3Tpl(ct::lwr4::APOLLO_BASE_POSITION_XYZ);
	basePose.setFromEulerAnglesXyz(ct::lwr4::APOLLO_BASE_ORIENTATION_XYZ);

	ct::core::DiscreteArray<typename Lwr4Dynamics::Kinematics_t::EEForce> forceTraj(1);
	forceTraj[0].setConstant(0.0);

	// Visualize trajectory using ROS
	ct::ros::RBDStatePublisher statePublisher(ct::models::lwr4::urdfJointNames(), "KUKA/Base", "/world");
	statePublisher.advertise(nh, "/current_joint_states", 10);

	std::thread visThread;


	while (ros::ok())
	{
	if (visThread.joinable())
		   visThread.join();
	   ROS_INFO("Visualizing");
	   visThread = std::thread(
				   visualizeTrajectory<Lwr4Dynamics::Kinematics_t>,
				   std::ref(statePublisher),
				   std::ref(x_traj),
				   std::ref(forceTraj),
				   std::ref(basePose),
				   0.01);
	}


	} catch (std::runtime_error& e)
	{
		std::cout << "Exception caught: " << e.what() << std::endl;
	}
}
