/*
 * derivativesCppadJitDemo.cpp
 *
 *  Created on: Jun 20, 2019
 *      Author: johannes
 */

#include <ct/kuka/pathNames.h>
#include <ct/kuka/EEContactModel.h>
#include <ct/iiwa/iiwa.h>

#include <ct/core/core.h>
#include <ct/optcon/optcon.h>

using namespace ct::rbd;
using namespace ct::core;
using namespace std;

static const int num_para = 7;
static const int state_dim = 14;

static const int in_dim = state_dim + num_para;
static const int out_dim = 1;

typedef DerivativesCppadJIT<in_dim, out_dim>::CG_SCALAR Scalar;

using iiwaDynamics = ct::rbd::iiwa::tpl::Dynamics<Scalar>;
using ContactModelCG = ct::kuka::EEContactModel<typename iiwaDynamics::Kinematics_t>;

using RobotState = ct::rbd::FixBaseRobotState<7, 0, Scalar>;
using Force = ContactModelCG::EEForceLinear;

typedef typename ct::core::tpl::TraitSelector<Scalar>::Trait AdcgTrait;

int main(int argc, char** argv)
{

	const std::string workingDirectory = ct::kuka::BASE_DIR + "/config/iiwa/nloc/scrape";
	const std::string contactFile = workingDirectory + "/contact.info";
//	contactModel->loadParameters(contactFile);

	Eigen::Matrix<double, in_dim, 1> x = Eigen::Matrix<double, in_dim, 1>::Constant(0.0);
	Eigen::Matrix<double, 7, 1> u = Eigen::Matrix<double, 7, 1>::Constant(0.0);

	//   x.head(7).setConstant(0.0);
	x(0) = 0.3;
	x(1) = 1.8;
	x(2) = -0.1;
	x(7) = 0.2;
	x(8) = 0.1;

	x(14) = 10;
	x(15) = 10;
	x(16) = 1.0;
	x(17) = 1.0;
	x(18) = 0.1;
	x(19) = 0.01;
	x(20) = 0.0;

	ContactModelCG contactModelCG = ContactModelCG();

	// Test auto diff of special functions
	DerivativesCppadJIT<in_dim, out_dim>::FUN_TYPE_CG myfunction;

	myfunction = [&](const DerivativesCppadJIT<in_dim, out_dim>::IN_TYPE_CG& input)
	{
		Eigen::Matrix<Scalar, state_dim, 1> state = input.template segment<state_dim>(0);
		Eigen::Matrix<Scalar, num_para, 1> cp = input.template segment<num_para>(state_dim);

		   DerivativesCppadJIT<in_dim, out_dim>::OUT_TYPE_CG output;

//		   const ct::core::ControlVector<7, Scalar> u = ct::core::ControlVector<7, Scalar>::Base::Zero();
//		   const Scalar t = (Scalar)0.0;

	    	contactModelCG.k() 			= cp(0);
	    	contactModelCG.d() 			= cp(1);
	    	contactModelCG.alpha_k() 	= cp(2);
	    	contactModelCG.alpha_s() 	= cp(3);
	    	contactModelCG.mu() 		= cp(4);
	    	contactModelCG.eps() 		= cp(5);
	    	contactModelCG.zOffset() 	= cp(6);

		   RobotState robotState = RobotState(ct::core::StateVector<state_dim, Scalar>(state));

		   Force contactForce = contactModelCG.computeContactForces(robotState.toRBDState())[0];


		   Scalar penalty = (Scalar)3.3 * log(contactForce(2));
//		   Scalar penalty = (Scalar)3.3 * log(1.0);

		   output[0] = penalty;
//		   output[0] = AdcgTrait::log(state(0));
//		   output[0] = contactForce(2);
//		   output[0] = x.dot(x);
//		   output = DerivativesCppadJIT<in_dim, out_dim>::OUT_TYPE_CG::Zero();

		   return output;
	};


	DerivativesCppadJIT<in_dim, out_dim> derivatives = DerivativesCppadJIT<in_dim, out_dim>(myfunction);
	Eigen::Matrix<double, out_dim, 1> lambda = Eigen::Matrix<double, out_dim, 1>::Constant(1.0);

	ct::core::DerivativesCppadSettings settings;
	settings.createForwardZero_ = true;
	settings.createReverseOne_ = true;
	settings.createJacobian_ = true;
	settings.createHessian_ = true;
	settings.print();

	std::cout << "compiling JIT" << std::endl;
	derivatives.compileJIT(settings, "derivativesCppadJitDemo");

	cout << "value is \n" << x << endl;
	cout << "fun eval \n" << derivatives.forwardZero(x) << endl;
	cout << "fun jac \n" << derivatives.jacobian(x) << endl;
	cout << "fun hess \n" << derivatives.hessian(x, lambda) << endl;


}



