
/* iiwaSim.cpp Simulation of the iiwa arm falling down onto the ground.
 *
 *
 */


#include <ct/ros/ros.h>
#include <ct/iiwa/iiwa.h>
#include <ct/iiwa/iiwaInverseKinematics.h>

#include <ct/kuka/FixBaseFDSystem.h>

//#include <kuka/plotJointTrajMatlab.h>

//#include <ct/ros/conversion/sensor_msgs/JointStateWrapper.h>

using namespace ct::core;
using namespace ct::rbd;
using namespace std;

int main(int argc, char* argv[])
{

	// Define robot model types
	typedef iiwa::tpl::Dynamics<double> iiwaDynamics;
	typedef ct::kuka::FixBaseFDSystem<iiwaDynamics> iiwaSystem;

    typedef typename iiwaSystem::FixBaseRobotState_t RobotState;

    const size_t state_dim = iiwaSystem::STATE_DIM;
    const size_t control_dim = iiwaSystem::CONTROL_DIM;
    const size_t n_joints = iiwaSystem::NJOINTS;

//    // Set base pose
//	ct::rbd::tpl::RigidBodyPose<double> basePose = ct::rbd::tpl::RigidBodyPose<double>();
//	basePose.position() = ct::rbd::tpl::RigidBodyPose<double>::Position3Tpl(0.0, 0.0, 0.0);
//	basePose.setFromEulerAnglesXyz(ct::rbd::tpl::RigidBodyPose<double>::Vector3Tpl(M_PI, 0.0, 0.0));

    iiwaDynamics::Kinematics_t iiwaKin = iiwaDynamics::Kinematics_t();

    Eigen::Vector3d r_des = Eigen::Vector3d(0.6, 0.0, 0.2);
    Eigen::Vector3d rpy_des = Eigen::Vector3d(0.0, 1.8, 0.0);

    Eigen::Quaternion<double> quat_des(Eigen::AngleAxisd(rpy_des(0), Eigen::Vector3d::UnitX()) *
                                       Eigen::AngleAxisd(rpy_des(1), Eigen::Vector3d::UnitY()) *
                                       Eigen::AngleAxisd(rpy_des(2), Eigen::Vector3d::UnitZ()));

    ct::rbd::RigidBodyPose eePoseDes = ct::rbd::RigidBodyPose(quat_des, r_des,
    		ct::rbd::RigidBodyPose::QUAT);

    ct::rbd::IiwaInverseKinematics<double> ikSolver;

    using JointPosition_t = typename RobotState::JointState_t::Position;
	using JointPositionsVector_t = std::vector<JointPosition_t, Eigen::aligned_allocator<JointPosition_t>>;

    JointPositionsVector_t ikSolutions;
    JointPosition_t ikSolution;
    JointPosition_t jointPosRef = JointPosition_t::Zero();

    std::vector<double> freeJoints{ 0.0 };

	if (!ikSolver.computeInverseKinematicsFree(ikSolutions, eePoseDes, freeJoints))
    {
    	throw std::runtime_error("Could not find IK solution.");
    }

	std::cout << "IK solution for desired pose: \n" << ikSolutions.at(0).transpose() << std::endl;

	std::cout << "EE position from IK: " << iiwaKin.getEEPositionInBase(0, ikSolutions.at(0)) << std::endl;
}




