/*
 * testTrackingTerm.cpp
 *
 *  Created on: May 31, 2019
 *      Author: johannes
 */

#include <ct/core/core.h>
#include <ct/optcon/optcon.h>

#include <ct/lwr4/lwr4.h>

using namespace ct::optcon;
using namespace ct::core;
using namespace std;

int main(int argc, char** argv)
{

	typedef ct::rbd::lwr4::tpl::Dynamics<ct::core::ADCGScalar> Lwr4Dynamics;

    const size_t state_dim = 14;
    const size_t control_dim = 7;

    // analytical costfunction
    std::shared_ptr<CostFunctionAnalytical<state_dim, control_dim>> cost(
    		new CostFunctionAnalytical<state_dim, control_dim>());

    shared_ptr<CostFunctionAD<state_dim, control_dim>> costFunctionAD(
    		new CostFunctionAD<state_dim, control_dim>());

    Eigen::Matrix<double, state_dim, state_dim> Q;
    Eigen::Matrix<double, control_dim, control_dim> R;
    Q.setIdentity();
    R.setIdentity();

    using TermTaskspacePose_t = ct::rbd::TermTaskspacePoseCG<typename Lwr4Dynamics::Kinematics_t, false, state_dim, control_dim>;

    shared_ptr<TermTaskspacePose_t> termTaskspace(new TermTaskspacePose_t());

    string filename = "/home/johannes/repos/control/src/ct_kuka_user/config/lwr4/nloc/scrape/cost.info";

    termTaskspace->loadConfigFile(filename, "term_task_space", true);

//    ct::core::tpl::PeriodicActivation activation(100.0, 2.0, 0.0, 0.0);
//    activation.printInfo();


    cost->addIntermediateTerm(termTaskspace);

    StateVector<state_dim> x;
    ControlVector<control_dim> u;
    x.setRandom();
    u.setRandom();
    double t = 0.0;

    cost->setCurrentStateAndControl(x, u, t);
	cost->initialize();

	cout << "Cost evaluation " << cost->evaluateIntermediate() << endl;
	cout << "State derivative cost \n" << cost->stateDerivativeIntermediate() << endl;
	cout << "State second derivative cost \n" << cost->stateSecondDerivativeIntermediate() << endl;
	cout << "Control derivative cost \n" << cost->controlDerivativeIntermediate() << endl;
	cout << "Control second derivative cost \n" << cost->controlSecondDerivativeIntermediate() << endl;
	cout << "State control derivative cost \n" << cost->stateControlDerivativeIntermediate() << endl;

}
