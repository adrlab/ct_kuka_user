/*
 * testTrackingTerm.cpp
 *
 *  Created on: May 31, 2019
 *      Author: johannes
 */

#include <ct/core/core.h>
#include <ct/optcon/optcon.h>
//#include <ct/kuka/TermNormalForceAnalytical.h>

using namespace ct::optcon;
using namespace ct::core;
using namespace std;

int main(int argc, char** argv)
{

	typedef CppAD::AD< CppAD::cg::CG<double>> ScalarCG;

    const size_t state_dim = 14;
    const size_t control_dim = 7;

    using TermQuadraticAD_t = TermQuadratic<state_dim, control_dim, double, ScalarCG>;
//    using TermNormalForce = ct::kuka::TermNormalForceAnalytical;

    // analytical costfunction
    std::shared_ptr<CostFunctionAnalytical<state_dim, control_dim>> costFunction(
    		new CostFunctionAnalytical<state_dim, control_dim>());

    shared_ptr<CostFunctionAD<state_dim, control_dim>> costFunctionAD(
    		new CostFunctionAD<state_dim, control_dim>());

    shared_ptr<TermQuadraticAD_t> termQ(new TermQuadraticAD_t);
//    shared_ptr<TermNormalForce> termNF(new TermNormalForce());

    Eigen::Matrix<double, state_dim, state_dim> Q;
    Eigen::Matrix<double, control_dim, control_dim> R;
    Q.setIdentity();
    R.setIdentity();

    StateVector<state_dim, double> x;
    ControlVector<control_dim, double> u;
    x.setRandom();
    u.setRandom();

    termQ->setWeights(Q, R);
    termQ->setStateAndControlReference(x, u);

    costFunctionAD->addIntermediateADTerm(termQ);
//    costFunctionAD->addIntermediateTerm(termNF);

    costFunctionAD->initialize();

    cout << "Cost is: " << costFunctionAD->evaluateIntermediate() << endl;

    termQ->setWeights(Q, R.setZero());

//    termNF->setLinearWeight(double(3));
//    termNF->setup();

    costFunctionAD->initialize();

    cout << "Cost is: " << costFunctionAD->evaluateIntermediate() << endl;

    termQ->setWeights(Q.setZero(), R);

    costFunctionAD->initialize();

    cout << "Cost is: " << costFunctionAD->evaluateIntermediate() << endl;

    const string costFile =
    		"/home/johannes/repos/control/src/ct_kuka_user/task/iiwa/nloc/scrape/cost.info";

    termQ->loadConfigFile(costFile, "term_const", true);
    termQ->setStateAndControlReference(x, u);

    costFunctionAD->initialize();

    cout << "Cost is: " << costFunctionAD->evaluateIntermediate() << endl;

}
