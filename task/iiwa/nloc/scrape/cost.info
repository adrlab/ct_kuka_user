term_const
{
	name "constant cost"
	kind "quadratic"   
	type 0              ; = intermediate

	weights
	{
		; state weighting
		Q
		{

			scaling 0

            ; joint position              
            (0,0)   0
            (1,1)   0
            (2,2)   0
            (3,3)   0
            (4,4)   0
            (5,5)   0
            (6,6)   0

            ; joint velocity
            (7,7)   1
            (8,8)   1
            (9,9)   1
            (10,10) 1
            (11,11) 1
            (12,12) 1
            (13,13) 1
		}
		R
		{
			scaling 0.0
			
			(0,0) 1   
			(1,1) 1   
			(2,2) 1   
			(3,3) 1   
			(4,4) 1   
			(5,5) 1  
            (6,6) 1 
		}
	}
}

term_tracking
{
    name "tracking cost"
    kind "quadratic"   
    type 0

    weights
    {
        ; state weighting
        Q
        {
            scaling 1000.0

            ; joint position
            (0,0)   1
            (1,1)   10
            (2,2)   1
            (3,3)   10
            (4,4)   1
            (5,5)   1
            (6,6)   1

            ; joint velocity
            (7,7)   0.1
            (8,8)   1
            (9,9)   0.1
            (10,10) 1
            (11,11) 0.1
            (12,12) 0.1
            (13,13) 0.1
        }
        R
        {
            scaling 0.001
            
            (0,0) 1   
            (1,1) 1   
            (2,2) 1   
            (3,3) 1   
            (4,4) 1   
            (5,5) 1  
            (6,6) 1 
        }
    }
}

term_via_point_1
{
    name "via point 1 cost"
    kind "quadratic"
    type 0
    
    time_activation
    {
        kind    "single"    ; one-time activation
        t_on    0
        t_off   1.0
    }
    
    weights
    {
        Q
        {
            scaling 0.0

            ; joint position              
            (0,0)   1 
            (1,1)   1
            (2,2)   1 
            (3,3)   1 
            (4,4)   1 
            (5,5)   1
            (6,6)   1

            ; joint velocity
            (7,7)   0
            (8,8)   0
            (9,9)   0
            (10,10) 0 
            (11,11) 0
            (12,12) 0
            (13,13) 0
        }
    }
}

term_via_point_2
{
    name "via point 2 cost"
    kind "quadratic"
    type 0
    
    time_activation
    {
        kind    "single"    ; one-time activation
        t_on    5.0
        t_off   6.0
    }
    
    weights
    {
        Q
        {
            scaling 0.0

            ; joint position              
            (0,0)   1.0 
            (1,1)   1.0
            (2,2)   1.0 
            (3,3)   1.0 
            (4,4)   1.0 
            (5,5)   1.0
            (6,6)   1.0

            ; joint velocity
            (7,7)   0
            (8,8)   0
            (9,9)   0
            (10,10) 0 
            (11,11) 0
            (12,12) 0
            (13,13) 0
        }
        x_des
        {
            ; joint position              
            (0,0)   0.0 
            (1,0)   0.0
            (2,0)   0.0 
            (3,0)   0.0
            (4,0)   0.0 
            (5,0)   0.0 
            (6,0)   0.0
            
            ; joint velocity
            (7,0)   0.0
            (8,0)   0.0
            (9,0)   0.0
            (10,0)  0.0 
            (11,0)  0.0
            (12,0)  0.0
            (13,0)  0.0             
        }
    }
}

term_final
{
	name "final cost"
	kind "quadratic"   
	type 1              ; = final

	weights
	{
        ; state weighting
        Q
        {
            scaling 100
            
            ; joint position              
            (0,0)   1 
            (1,1)   1 
            (2,2)   1 
            (3,3)   1
            (4,4)   1 
            (5,5)   1
            (6,6)   1 

            ; joint velocity
            (7,7)   1.0
            (8,8)   1.0
            (9,9)   1.0
            (10,10) 1.0
            (11,11) 1.0
            (12,12) 1.0
            (13,13) 1.0   
        }
	}
}

term_normal_force
{  
    time_activation
    {
        kind    "single"    ; one-time activation
        t_on    1.0
        t_off   6.0
    }
    
    q_lin   -100.0
    q_quad  0.0
    
    force_reference 0
}

term_friction_work
{
    time_activation
    {
        kind    "single"    ; one-time activation
        t_on    0.0
        t_off   6.0
    }
    
    q_lin   -0.5
}

; taskspace start pose
pose_start
{

    ; desired position,
    ; xyz of ee frame in base frame
    r_des
    {
        (0,0)   0.5
        (1,0)   0.4
        (2,0)   0.0 
    }
    
    ; desired orientation
    ; rpy (xyz) local euler angles of ee frame in base frame
    rpy_des
    {
        (0,0) 0.0
        (1,0) 2.8
        (2,0) 0.0
    }
}

; taskspace end pose
pose_end
{

    ; desired position,
    ; xyz of ee frame in base frame
    r_des
    {
        (0,0)   0.5
        (1,0)   -0.4
        (2,0)   0.0
    }
    
    ; desired orientation
    ; rpy (xyz) local euler angles of ee frame in base frame
    rpy_des
    {
        (0,0) 0.0
        (1,0) 2.7
        (2,0) 0.0
    }
}

; initial state
x_start
{
	; joint position              
    (0,0)   0.0 
    (1,0)   1.4
    (2,0)   0.0 
    (3,0)   -0.5
    (4,0)   0.0 
    (5,0)   0.2 
    (6,0)   0.0
	
	; joint velocity
	(7,0)   0.1
	(8,0)   0.0
	(9,0)   0.0
	(10,0)  0.0 
	(11,0)  0.0
    (12,0)  0.0
    (13,0)  0.0     
}

; via state A
x_A
{
    ; joint position              
    (0,0)   0.0
    (1,0)   1.0
    (2,0)   0.0
    (3,0)   -2.2
    (4,0)   0.0
    (5,0)   0.2
    (6,0)   0.0
    
    ; joint velocity
    (7,0)   0.0
    (8,0)   0.0
    (9,0)   0.0
    (10,0)  0.0 
    (11,0)  0.0
    (12,0)  0.0 
    (13,0)  0.0    
}

; initial feedback matrix
K_init
{
	scaling 0.0

	; joint position              
	(0,0)   0.0 
	(1,1)   0.0 
	(2,2)   0.0 
	(3,3)   0.0 
	(4,4)   0.0 
	(5,5)   0.0
    (6,6)   0.0

	; joint velocity
	(7,7)   0.0
	(8,8)   0.0
	(9,9)   0.0
	(10,10) 0.0 
	(11,11) 0.0
    (12,12) 0.0
    (13,13) 0.0     
}

