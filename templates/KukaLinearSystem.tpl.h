/**********************************************************************************************************************
This file is part of the Control Toolbox (https://adrlab.bitbucket.io/ct), copyright by ETH Zurich, Google Inc.
Licensed under Apache2 license (see LICENSE file in main directory)
**********************************************************************************************************************/

#pragma once

#include <ct/iiwa/iiwa.h>

#include <ct/kuka/FixBaseFDSystem.h>
#include <ct/kuka/EEContactModel.h>

namespace ct {
namespace NS1 {
namespace NS2 {

class KUKA_LINEAR_SYSTEM : public ct::core::LinearSystem<STATE_DIM, CONTROL_DIM, SCALAR>
{
public:

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    typedef ct::core::LinearSystem<STATE_DIM, CONTROL_DIM, SCALAR> Base;
    typedef ct::core::LinearSystem<STATE_EXT_DIM, CONTROL_DIM, SCALAR> BaseExt;

    typedef typename Base::state_vector_t state_vector_t;
    typedef typename Base::control_vector_t control_vector_t;
    typedef typename Base::state_matrix_t state_matrix_t;
    typedef typename Base::state_control_matrix_t state_control_matrix_t;

    typedef typename BaseExt::state_vector_t state_vector_ext;
    typedef typename BaseExt::state_matrix_t state_matrix_ext;
    typedef typename BaseExt::state_control_matrix_t state_control_matrix_ext;

    typedef ct::core::StateVector<NUM_PARA, double> para_vector;

    typedef ct::kuka::EEContactModel<ct::rbd::iiwa::Dynamics::Kinematics_t> ContactModel;

    KUKA_LINEAR_SYSTEM(
    		const std::shared_ptr<ContactModel> contactModel =
				std::shared_ptr<ContactModel>(new ContactModel()),
    		const ct::core::SYSTEM_TYPE& type = ct::core::SYSTEM_TYPE::GENERAL) :
        ct::core::LinearSystem<STATE_DIM, CONTROL_DIM>(type), contactModel_(contactModel)
    {
        initialize();
    }

    KUKA_LINEAR_SYSTEM(const KUKA_LINEAR_SYSTEM& other) :
    	ct::core::LinearSystem<STATE_DIM, CONTROL_DIM>(other), contactModel_(other.contactModel_)
    {
    	initialize();
    }

    virtual ~KUKA_LINEAR_SYSTEM(){};

    virtual KUKA_LINEAR_SYSTEM* clone() const override { return new KUKA_LINEAR_SYSTEM; }

    virtual const state_matrix_t& getDerivativeState(const state_vector_t& x,
        const control_vector_t& u,
        const SCALAR t = SCALAR(0.0)) override
    {
    	setStateVectorExt(x);

    	getDerivativeStateExt(x_ext_, u, t);

    	A_ = dFdx_.template block<STATE_DIM, STATE_DIM>(0,0);

    	return A_;
    }

    virtual const state_control_matrix_t& getDerivativeControl(const state_vector_t& x,
        const control_vector_t& u,
        const SCALAR t = SCALAR(0.0)) override
    {
    	setStateVectorExt(x);

    	getDerivativeControlExt(x_ext_, u, t);

    	B_ = dFdu_.template block<STATE_DIM, CONTROL_DIM>(0,0);

    	return B_;
    }

private:
    void getDerivativeStateExt(const state_vector_ext& x,
        const control_vector_t& u,
        const SCALAR t = SCALAR(0.0));

    void getDerivativeControlExt(const state_vector_ext& x,
        const control_vector_t& u,
        const SCALAR t = SCALAR(0.0));

    void setStateVectorExt(const state_vector_t& trueState)
    {
    	cmParameters_[0] = contactModel_->k();
    	cmParameters_[1] = contactModel_->d();
    	cmParameters_[2] = contactModel_->alpha_k();
    	cmParameters_[3] = contactModel_->alpha_s();
    	cmParameters_[4] = contactModel_->mu();
    	cmParameters_[5] = contactModel_->eps();
    	cmParameters_[6] = contactModel_->zOffset();

    	x_ext_ << trueState, cmParameters_;
    }

    void initialize()
    {
        dFdx_.setZero();
        dFdu_.setZero();
        vX_.fill(0.0);
        vU_.fill(0.0);

        A_.setZero();
        B_.setZero();

        x_ext_.setZero();
        cmParameters_.setZero();
    }

    state_matrix_ext dFdx_;
    state_control_matrix_ext dFdu_;
    std::array<SCALAR, MAX_COUNT_STATE> vX_;
    std::array<SCALAR, MAX_COUNT_CONTROL> vU_;

	para_vector cmParameters_;
	state_vector_ext x_ext_;

	state_matrix_t A_;
	state_control_matrix_t B_;

	std::shared_ptr<ContactModel> contactModel_;
};

} // namespace NS2
} // namespace NS1
} // namespace ct
