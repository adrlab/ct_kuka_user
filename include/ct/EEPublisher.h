
#pragma once
#include <tf/transform_broadcaster.h>

template<typename SCALAR>
class EEPublisher {
public:
	void EEPublisher(ros::NodeHandle& nh)
	{
		nh_ = nh;
	}

	/*
	void advertise(::ros::NodeHandle& nh, const char* eeStateTopicName, const int& bufferLength)
	{
		nh.advertise<JointStateWrapper::Base>(eeStateTopicName, bufferLength);
	}
	*/

	void publishEE(const ct::rbd::RBDState_t& state)
	{
		kindr::Position<SCALAR,3> pos = state.basePose().posiiton();
		kindr::Quaternion< SCALAR > rot = state.basePose().getRotationQuaternion();

		tf::Transform transform;
		transform.setOrigin(tf::Vector3f(pos.x, pos.y, pos.z));
		transform.setRotation(tf::Quaternion(rot.x(), rot.y(), rot.z(), rot.w()));

		if(nh_.ok())
		{
			broadcaster.sendTransform(transform, ros::Time::now(), "/iiwabase", "/fr_ee");
		}


		//eePublisher_.publish(jointStateMsg.toROSMsg());
	}
private:
	ros::publisher eePublisher_;

	tf::TransformBroadcaster broadcaster_;
	ros::nodeHandle nh_;

};
