#pragma once

#include <ct/optcon/optcon.h>

#include <ct/kuka/TermNormalForceJitCm.h>
#include <ct/kuka/TermFrictionWorkJitCm.h>

#include <ct/kuka/EEContactModel.h>
#include <ct/kuka/FixBaseFDSystem.h>

namespace ct{
namespace kuka{

template<typename DYNAMICS>
class KukaNloc
{
public:

    // Scalar types to use throughout this class
	typedef double Scalar;
	typedef CppAD::AD< CppAD::cg::CG<Scalar>> ScalarCG;

    // Standard types
    typedef FixBaseFDSystem<DYNAMICS> RobotSystem;
    typedef EEContactModel<typename DYNAMICS::Kinematics_t> ContactModel;

    static const size_t n_joints = RobotSystem::NJOINTS;
    static const size_t state_dim = RobotSystem::STATE_DIM;
    static const size_t control_dim = RobotSystem::CONTROL_DIM;

    using LinearSystem_t = ct::core::LinearSystem<state_dim, control_dim, Scalar>;
    using ADCodegenLinearizer_t = ct::core::ADCodegenLinearizer<state_dim, control_dim, Scalar>;

    using InverseKinematicsBase_t = ct::rbd::InverseKinematicsBase<n_joints, Scalar>;

    using CostFunctionAD_t = ct::optcon::CostFunctionAD<state_dim, control_dim, Scalar>;
    using TermQuadratic_t = ct::optcon::TermQuadratic<state_dim, control_dim, Scalar, Scalar>;
    using TermQuadraticAD_t = ct::optcon::TermQuadratic<state_dim, control_dim, Scalar, ScalarCG>;

    // TODO debug ADCG for the tracking term
//    using TermQuadTrackingAD = ct::optcon::TermQuadTracking<state_dim, control_dim, Scalar, ScalarCG>;
    using TermQuadTracking_t = ct::optcon::TermQuadTracking<state_dim, control_dim, Scalar, Scalar>;

    using TermFrictionWork = ct::iiwa::TermFrictionWorkJitCm;
    using TermNormalForce = ct::iiwa::TermNormalForceJitCm;

    using FixBaseNLOC_t = ct::rbd::FixBaseNLOC<RobotSystem>;
    using RobotState_t = typename FixBaseNLOC_t::RobotState_t;

    using StateVector_t = typename FixBaseNLOC_t::StateVector;
    using ControlVector_t = typename FixBaseNLOC_t::ControlVector;
    using StateVectorArray_t = typename FixBaseNLOC_t::StateVectorArray;
    using ControlVectorArray_t = typename FixBaseNLOC_t::ControlVectorArray;

    using StateTrajectory_t = ct::core::StateTrajectory<state_dim, Scalar>;
    using ControlTrajectory_t = ct::core::ControlTrajectory<control_dim, Scalar>;

    using JointPosition_t = typename RobotState_t::JointState_t::Position;
	using JointPositionsVector_t = std::vector<JointPosition_t, Eigen::aligned_allocator<JointPosition_t>>;

    KukaNloc(const std::string& contactFilename,
    		const std::string& configFilename,
			const std::string& costFunctionFilename,
			std::shared_ptr<ContactModel> contactModel,
			std::shared_ptr<RobotSystem> robotSystem,
			std::shared_ptr<LinearSystem_t> robotSystemLinearized,
			std::shared_ptr<InverseKinematicsBase_t> robotInverseKinematics,
			const bool loadVerbose = false) :
				contactFile_(contactFilename),
				configFile_(configFilename),
				costFunctionFile_(costFunctionFilename),
				contactModel_(contactModel),
				robot_(robotSystem),
				robotLin_(robotSystemLinearized),
				robotIK_(robotInverseKinematics),
				costFunctionAD_(new CostFunctionAD_t)
	{

        // Constant Term
		// This term is constant over the whole duration of the trajectory. It's mainly used
		// to regulate control effort.
		termQuadConst_ = std::shared_ptr<TermQuadraticAD_t>(new TermQuadraticAD_t);
        costFunctionAD_->addIntermediateADTerm(termQuadConst_, loadVerbose);

        // Tracking Term
        // This term tracks linear state and control references generated between via points.
        termQuadTracking_ = std::shared_ptr<TermQuadTracking_t>(new TermQuadTracking_t);
        termQuadTracking_->setName("tracking_cost"); // TODO implement function for name loading
        costFunctionAD_->addIntermediateTerm(termQuadTracking_, loadVerbose);

        // Via Point 1
        // This term is used to ensure a smooth start. It is active only for short duration after start.
        // Without it velocity tends to spike off at the beginning. No control penalty.
        termQuadViaPoint1_ = std::shared_ptr<TermQuadratic_t>(new TermQuadratic_t);
        costFunctionAD_->addIntermediateTerm(termQuadViaPoint1_, loadVerbose);

        // Via Point 2
//	      termQuadViaPoint2 = std::shared_ptr<TermQuadratic_t>(new TermQuadratic_t);
//        termQuadViaPoint2_->loadConfigFile(costFunctionFile_, "term_via_point_2", loadVerbose);
//        termQuadViaPoint2_->loadTimeActivation(costFunctionFile_, "term_via_point_2", loadVerbose);
//        costFunctionAD_->addIntermediateADTerm(termQuadViaPoint2_);

        // Normal Force Term
        // This term is used to regulate amount of normal force at EE.
//        termNormalForce_ = std::shared_ptr<TermNormalForce>(
//        		new TermNormalForce(contactModel_, "normal_force_cost"));
//        costFunctionAD_->addIntermediateTerm(termNormalForce_, loadVerbose);

        // Work Term
        termFrictionWork_ = std::shared_ptr<TermFrictionWork>(
        		new TermFrictionWork(contactModel_, "friction_work_cost"));
		costFunctionAD_->addIntermediateTerm(termFrictionWork_);

        // Final Term
        // Standard final term requiring a state reference only.
		termQuadFinal_ = std::shared_ptr<TermQuadraticAD_t>(new TermQuadraticAD_t);
        costFunctionAD_->addFinalADTerm(termQuadFinal_, loadVerbose);


        // Set-up NLOC settings
        settings_ = ct::optcon::NLOptConSettings();
    	settings_.load(configFile_, loadVerbose, "solver");

        ct::core::loadScalar(configFile_, "time_horizon", timeHorizon_);
        N_ = settings_.computeK(timeHorizon_); // compute number of time steps

        // Set-up solver
        solver_ = FixBaseNLOC_t(costFunctionAD_, settings_, robot_, loadVerbose, robotLin_);

        recomputeIkPoses(loadVerbose);
        reloadCostTerms(loadVerbose);

        initialize();

        reloadSettings(loadVerbose);

    }


    void reloadSettings(const bool verbose)
    {
        ct::core::loadScalar(configFile_, "time_horizon", timeHorizon_);
        N_ = settings_.computeK(timeHorizon_); // compute number of time steps

    	settings_.load(configFile_, verbose, "solver");

    	solver_.getSolver()->configure(settings_);
    }

    void reloadCostTerms(const bool verbose)
    {

    	ct::core::loadScalar(configFile_, "num_cycles", numCycles_);

        generateCycles<StateVector_t>(numCycles_,
        		refStateStart_.toStateVector(),
				refStateEnd_.toStateVector(),
				stateRefTracking_, stateTrajInitial_);

        generateCycles<ControlVector_t>(numCycles_,
        		robot_->computeIDTorques(refStateStart_.toStateVector()),
				robot_->computeIDTorques(refStateEnd_.toStateVector()),
				controlRefTracking_, controlTrajInitial_);

        ct::core::loadScalar(costFunctionFile_, "term_friction_work.q_lin", qWorkLin_); // work term weighting

        ct::core::loadScalar(costFunctionFile_, "term_normal_force.q_lin", qNormalForceLin_); // normal force weighting
        ct::core::loadScalar(costFunctionFile_, "term_normal_force.q_quad", qNormalForceQuad_); // normal force weighting
        ct::core::loadScalar(costFunctionFile_, "term_normal_force.force_reference", normalForceRef_);
        // TODO implement loadConfigFile

        termQuadConst_->loadConfigFile(costFunctionFile_, "term_const", verbose);
        termQuadConst_->updateReferenceState(refStateEnd_.toStateVector());
        termQuadConst_->updateReferenceControl(robot_->computeIDTorques(refStateEnd_.toStateVector()));

        termQuadTracking_->loadConfigFile(costFunctionFile_, "term_tracking", verbose);
        termQuadTracking_->setStateAndControlReference(stateRefTracking_, controlRefTracking_);

        termQuadViaPoint1_->loadConfigFile(costFunctionFile_, "term_via_point_1", verbose);
        termQuadViaPoint1_->loadTimeActivation(costFunctionFile_, "term_via_point_1", verbose);
        termQuadViaPoint1_->updateReferenceState(refStateStart_.toStateVector());

//		termQuadViaPoint2_->loadConfigFile(costFunctionFile_, "term_via_point_2", true);
//		termQuadViaPoint2_->loadTimeActivation(costFunctionFile_, "term_via_point_2", true);

//        termNormalForce_->loadConfigFile(costFunctionFile_, "term_normal_force", verbose);
//        termNormalForce_->loadTimeActivation(costFunctionFile_, "term_normal_force", verbose);
//        termNormalForce_->setup();

        termFrictionWork_->loadConfigFile(costFunctionFile_, "term_friction_work", verbose);
        termFrictionWork_->loadTimeActivation(costFunctionFile_, "term_friction_work", verbose);
        termFrictionWork_->setup();

        termQuadFinal_->loadConfigFile(costFunctionFile_, "term_final", verbose);
        termQuadFinal_->updateReferenceState(refStateEnd_.toStateVector());

        costFunctionAD_->initialize();

        solver_.changeCostFunction(costFunctionAD_);

    }

    void recomputeIkPoses(bool loadVerbose)
    {
        // Load initial, final and intermediate states
        StateVector_t x;

        ct::core::loadMatrix(costFunctionFile_, "x_start", x);
        refStateStart_.fromStateVector(x);

        ct::core::loadMatrix(costFunctionFile_, "x_A", x);
        refStateViaA_.fromStateVector(x);

        ct::core::loadMatrix(costFunctionFile_, "x_start", x);
        refStateEnd_.fromStateVector(x);

        JointPositionsVector_t ikSolutions;
        JointPosition_t ikSolution;
        JointPosition_t jointPosZero = JointPosition_t::Zero();
        std::vector<size_t> freeJoints{ 0 }; // Set free joint to zero

        Eigen::Vector3d r_end, rpy_end, r_start, rpy_start;

       	ROS_INFO("Solving Inverse Kinematics for start pose");

		ct::core::loadMatrix(costFunctionFile_, "pose_start.r_des", r_start);
		ct::core::loadMatrix(costFunctionFile_, "pose_start.rpy_des", rpy_start);

		Eigen::Quaternion<double> quat_start(Eigen::AngleAxisd(rpy_start(0), Eigen::Vector3d::UnitX()) *
										Eigen::AngleAxisd(rpy_start(1), Eigen::Vector3d::UnitY()) *
										Eigen::AngleAxisd(rpy_start(2), Eigen::Vector3d::UnitZ()));

		ct::rbd::RigidBodyPose eePoseStart = ct::rbd::RigidBodyPose(quat_start, r_start, ct::rbd::RigidBodyPose::QUAT);

		if (!robotIK_->computeInverseKinematicsCloseTo(ikSolution, eePoseStart, jointPosZero, freeJoints))
        {
        	throw std::runtime_error("Could not find IK for start pose.");
        }

        if (loadVerbose)
        	std::cout << "IK solution for start pose: \n" << ikSolution.transpose() << std::endl;

        refStateStart_.joints().getPositions() = ikSolution;

       	ROS_INFO("Solving Inverse Kinematics for end pose");

        ct::core::loadMatrix(costFunctionFile_, "pose_end.r_des", r_end);
        ct::core::loadMatrix(costFunctionFile_, "pose_end.rpy_des", rpy_end);

        Eigen::Quaternion<double> quat_end(Eigen::AngleAxisd(rpy_end(0), Eigen::Vector3d::UnitX()) *
                                           Eigen::AngleAxisd(rpy_end(1), Eigen::Vector3d::UnitY()) *
                                           Eigen::AngleAxisd(rpy_end(2), Eigen::Vector3d::UnitZ()));

        ct::rbd::RigidBodyPose eePoseEnd = ct::rbd::RigidBodyPose(quat_end, r_end, ct::rbd::RigidBodyPose::QUAT);

		if (!robotIK_->computeInverseKinematicsCloseTo(ikSolution, eePoseEnd, jointPosZero, freeJoints))
        {
        	throw std::runtime_error("Could not find IK for end pose.");
        }

        if (loadVerbose)
        	std::cout << "IK solution for end pose: \n" << ikSolution.transpose() << std::endl;

        refStateEnd_.joints().getPositions() = ikSolution;
    }

    void initialize()
    {
        ct::core::loadScalar(configFile_, "init_type", initType_);
        ct::core::loadMatrix(costFunctionFile_, "K_init", K_); 				// initial feedback matrix

    	// Set initial trajectory for solver and control references for certain cost terms.
		switch (initType_)
		{
			case 0:  // steady state
			{
				ROS_INFO("Initialize solver with constant initial state");

				ct::core::ControlVector<control_dim, Scalar> uff_0;
				solver_.initializeSteadyPose(refStateStart_, timeHorizon_, N_, uff_0, K_);

				termQuadConst_->updateReferenceControl(uff_0);

				break;
			}
			case 1:  // linear interpolation
			{
				ROS_INFO("Initialize solver with linear interpolation between start and end state");

				solver_.initializeDirectInterpolation(refStateStart_, refStateEnd_, timeHorizon_, N_, K_);

				break;
			}
			case 2: // linear interpolation with via points
			{
				ROS_INFO("Initialize solver with linear interpolation between start, via points and end");

				StateVectorArray_t x_init(N_ + 1);
				ControlVectorArray_t uff_init(N_);

				// Sample initial state trajectory at points of the time array and compute controls
				ct::core::TimeArray time = ct::core::TimeArray(settings_.dt, N_+1, 0.0);
				ct::core::TimeArray::iterator timeIter;

				for (timeIter = time.begin(); timeIter != time.end()-1; timeIter++)
				{
					size_t index = std::distance(time.begin(), timeIter);

					x_init[index] = stateTrajInitial_.eval(*timeIter);
					uff_init[index] = controlTrajInitial_.eval(*timeIter);
				}

				x_init[N_] = stateTrajInitial_.eval(*(time.end())); // final state

				typename FixBaseNLOC_t::FeedbackArray ufb_init(N_, K_);

				typename FixBaseNLOC_t::NLOptConSolver::Policy_t initialPolicy(
						x_init, uff_init, ufb_init , settings_.dt);

				solver_.getSolver()->changeInitialState(refStateStart_.toStateVector());
				solver_.getSolver()->changeTimeHorizon(timeHorizon_);
				solver_.getSolver()->setInitialGuess(initialPolicy);

				break;
			}
			default:
			{
				throw std::runtime_error("Illegal init type");
				break;
			}
		}
    }

    //! some getter methods
    const double& 						getTimeHorizon() 	{ return timeHorizon_; }
    const int& 							getNumTimeSteps() 	{ return N_; }
    const RobotState_t& 				getInitialState() 	{ return refStateStart_; }
    const RobotState_t& 				getFinalState() 	{ return refStateEnd_; }
    ct::optcon::NLOptConSettings& 		getNlocSettings() 	{ return settings_; }
    FixBaseNLOC_t& 						getSolver() 		{ return solver_; }
    StateTrajectory_t&					getStateRefTraj()	{ return stateRefTracking_; }
    ControlTrajectory_t&				getControlRefTraj() { return controlRefTracking_; }
    std::shared_ptr<RobotSystem> 		getSystem() 		{ return robot_; }
    std::shared_ptr<LinearSystem_t> 	getLinearSystem() 	{ return robotLin_; }
    std::shared_ptr<CostFunctionAD_t> 	getCostFunction() 	{ return costFunctionAD_; }
    std::shared_ptr<ContactModel>		getContactModel() 	{ return contactModel_; }

    ControlVectorArray_t 				getControlRef()
    {
    	ControlVectorArray_t controlRef = ControlVectorArray_t(N_-1);

    	ct::core::TimeArray time = ct::core::TimeArray(settings_.dt, N_-1, 0.0);

    	for (size_t i=0; i<N_-1; i++)
    	{
    		controlRef.at(i) = controlRefTracking_.eval(time.at(i));
    	}

    	return controlRef;
    }

    std::vector<std::shared_ptr<ct::optcon::TermBase<state_dim, control_dim, Scalar>>>
										getIntermediateTerms()
	{
    	std::vector<std::shared_ptr<ct::optcon::TermBase<state_dim, control_dim, Scalar>>> terms;

//    	terms.push_back(termQuadConst_); // cannot add; has an AD type
    	terms.push_back(termQuadTracking_);
    	terms.push_back(termQuadViaPoint1_);
//    	terms.push_back(termNormalForce_);
    	terms.push_back(termFrictionWork_);

    	return terms;
	}

private:

    typename FixBaseNLOC_t::FeedbackArray::value_type K_;

    ct::rbd::tpl::RigidBodyPose<double> basePose_;

    std::shared_ptr<ContactModel> contactModel_;

    std::shared_ptr<RobotSystem> robot_;
    std::shared_ptr<LinearSystem_t> robotLin_;
    std::shared_ptr<InverseKinematicsBase_t> robotIK_;

    std::shared_ptr<TermQuadratic_t> termQuadViaPoint1_;
    std::shared_ptr<TermQuadraticAD_t> termQuadConst_;
    std::shared_ptr<TermQuadraticAD_t> termQuadViaPoint2_;
    std::shared_ptr<TermQuadraticAD_t> termQuadFinal_;
    std::shared_ptr<TermQuadTracking_t> termQuadTracking_;
    std::shared_ptr<TermFrictionWork> termFrictionWork_;
//    std::shared_ptr<TermNormalForce> termNormalForce_;

    std::shared_ptr<CostFunctionAD_t> costFunctionAD_;

    ct::core::StateTrajectory<state_dim, Scalar> stateTrajInit_;

    size_t intTermID;
    size_t finalTermID;
    size_t mixedTermID;
    size_t intTermWorkID;

    ct::optcon::NLOptConSettings settings_;
    FixBaseNLOC_t solver_;

    double qWorkLin_, qNormalForceLin_, qNormalForceQuad_, normalForceRef_;
    double numCycles_;

    RobotState_t refStateStart_, refStateEnd_, refStateViaA_;

    StateTrajectory_t stateRefTracking_, stateTrajInitial_;
    ControlTrajectory_t controlRefTracking_, controlTrajInitial_;

    std::string contactFile_, configFile_, costFunctionFile_;

    ct::core::Time timeHorizon_;
//    const int numViaPoints_ = 0;
    int initType_ = 0, N_ = 0;


    template <class VEC>
    void generateCycles(const int numCycles,
    		const VEC& pointA, const VEC& pointB,
    		ct::core::DiscreteTrajectoryBase<VEC, Eigen::aligned_allocator<VEC>>& trajZOH,
			ct::core::DiscreteTrajectoryBase<VEC, Eigen::aligned_allocator<VEC>>& trajLin)
    {
    	// Generate linear and zero-order hold trajectories between vector points A and B.

    	int numPoints = numCycles * 2 + 1;

    	ct::core::tpl::TimeArray<Scalar> time(timeHorizon_/(numPoints-1), numPoints);

    	trajZOH.clear();
    	trajLin.clear();

    	trajZOH.setInterpolationType(ct::core::InterpolationType::ZOH);
    	trajLin.setInterpolationType(ct::core::InterpolationType::LIN);

    	for (int i=0; i<numCycles*2; i+=2)
    	{
    		trajZOH.push_back(pointA, time.at(i), true);
    		trajZOH.push_back(pointB, time.at(i+1), true);

    		trajLin.push_back(pointA, time.at(i), true);
			trajLin.push_back(pointB, time.at(i+1), true);
    	}

    	trajZOH.push_back(pointB, time.at(numPoints-1), true);
    	trajLin.push_back(pointA, time.at(numPoints-1), true);
    }
};

} // kuka
} // ct
