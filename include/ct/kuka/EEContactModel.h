
#pragma once

#include <kindr/Core>

#include <ct/rbd/rbd.h>

//#pragma GCC diagnostic push  // include IIT headers and disable warnings
//#pragma GCC diagnostic ignored "-Wunused-parameter"
//#pragma GCC diagnostic ignored "-Wunused-value"
//#pragma GCC diagnostic pop


namespace ct {
namespace kuka {

/*!
 * \brief EEContactMoel Contact model for the kuka robots.
 *
 * Implements smoothed versions of normal ground contact forces and tangential dry friction forces.
 *
 */
template <class Kinematics>
class EEContactModel
{
public:
    static const size_t NUM_EE = Kinematics::NUM_EE;
    static const size_t NJOINTS = Kinematics::NJOINTS;
    static const size_t NUM_PARA = 7;

    typedef typename Kinematics::SCALAR SCALAR;

    typedef typename ct::core::tpl::TraitSelector<SCALAR>::Trait TRAIT;

    typedef std::array<bool, NUM_EE> ActiveMap;
    typedef typename Kinematics::EEForceLinear EEForceLinear;
    typedef std::array<EEForceLinear, NUM_EE> EEForcesLinear;

    typedef Eigen::Matrix<SCALAR, 3, 1> Vector3s;
    typedef kindr::Position<SCALAR, 3> Position3S;
    typedef kindr::Velocity<SCALAR, 3> Velocity3S;


    /*!
	 * \brief the type of sigmoid
	 */
    enum SIGMOID_TYPE
    {
        EXP = 0,  	//!< sigmoid
        TANH = 1,   //!< tanh
        ABS = 2     //!< abs
    };

    /*!
	 * \brief Default constructor
	 *
	 * All parameters are optional.
	 *
	 * @param k stiffness of vertical spring
	 * @param d damper coefficient
	 * @param alpha_k normal force smoothing coefficient
	 * @param alpha_s velocity smoothing coefficient
	 * @param mu friction coefficient
	 * @param eps velocity smoothing
	 * @param zOffset z offset of the plane with respect to (0, 0, 0)
	 * @param kinematics instance of the kinematics (optionally). Should be provided for efficiency when using Auto-Diff.
	 */
    EEContactModel(
    	const std::shared_ptr<Kinematics> kinematics = std::shared_ptr<Kinematics>(new Kinematics()),
    	const SCALAR& k = SCALAR(50.0),
        const SCALAR& d = SCALAR(10.0),
        const SCALAR& alpha_k = SCALAR(10.0),
        const SCALAR& alpha_s = SCALAR(10.0),
		const SCALAR& mu = SCALAR(1.0),
		const SCALAR& eps = SCALAR(1e-3),
        const SCALAR& zOffset = SCALAR(0.0)) :
			kinematics_(kinematics),
			k_(k),
			d_(d),
			alpha_k_(alpha_k),
			alpha_s_(alpha_s),
			mu_(mu),
			eps_(eps),
			zOffset_(zOffset)
    {
        for (size_t i = 0; i < NUM_EE; i++)
            EEactive_[i] = true;
    }

    //! Copy constructor
    EEContactModel(const EEContactModel& other) :
			kinematics_(other.kinematics_->clone()),
			EEactive_(other.EEactive_),
			k_(other.k_),
			d_(other.d_),
			alpha_k_(other.alpha_k_),
			alpha_s_(other.alpha_s_),
			mu_(other.mu_),
			eps_(other.eps_),
			zOffset_(other.zOffset_) {}

    //! Deep cloning
    EEContactModel* clone() const { return new EEContactModel(*this); }

    /*!
	 * \brief Sets which end-effectors can have forces excerted on them
	 * @param activeMap flags of active end-effectors
	 */
    void setActiveEE(const ActiveMap& activeMap) { EEactive_ = activeMap; }

    void loadParameters(const std::string& filename)
    {
        core::loadScalar(filename, "spring_constant", k_);
        core::loadScalar(filename, "damping_constant", d_);
        core::loadScalar(filename, "alpha_k", alpha_k_);
        core::loadScalar(filename, "alpha_s", alpha_s_);
        core::loadScalar(filename, "friction_coefficient", mu_);
        core::loadScalar(filename, "velocity_normalization", eps_);
        core::loadScalar(filename, "ground_offset", zOffset_);
    }

    /*!
	 * \brief Computes the contact forces given a state of the robot. Returns forces expressed in the world frame
	 * @param state The state of the robot
	 * @return End-effector forces expressed in the world frame
	 */
    EEForcesLinear computeContactForces(const rbd::RBDState<NJOINTS, SCALAR>& state)
    {
        EEForcesLinear eeForces;

        for (size_t i = 0; i < NUM_EE; i++)
        {
            if (EEactive_[i])
            {
                Vector3s eePenetration = computePenetration(i, state.basePose(), state.jointPositions());

				Velocity3S eeVelocity = kinematics_->getEEVelocityInWorld(i, state);
				eeForces[i] = computeEEForce(eePenetration, eeVelocity);
            }
        }

        return eeForces;
    }

    SCALAR& k() { return k_; }
    SCALAR& d() { return d_; }
    SCALAR& alpha_k() { return alpha_k_; }
    SCALAR& alpha_s() { return alpha_s_; }
    SCALAR& mu() { return mu_; }
    SCALAR& eps() { return eps_; }
    SCALAR& zOffset() { return zOffset_; }

    const SIGMOID_TYPE& smoothing() { return type_; }

    std::shared_ptr<Kinematics> kinematics() { return kinematics_; }

    void setKinematics (const std::shared_ptr<Kinematics> kinematics)
    {
    	kinematics_ = kinematics;
    }

private:

    /*!
	 * \brief Computes the surface penetration. Currently assumes the surface is at height z = 0.
	 * @param eeId ID of the end-effector
	 * @param basePose Pose of the robot base
	 * @param jointPosition Joint position of the robot
	 * @return Penetration in world coordinates
	 */
    Vector3s computePenetration(const size_t& eeId,
        const rbd::tpl::RigidBodyPose<SCALAR>& basePose,
        const typename rbd::JointState<NJOINTS, SCALAR>::Position& jointPosition)
    {
        Position3S pos = kinematics_->getEEPositionInWorld(eeId, basePose, jointPosition);

        // we currently assume flat ground at height zero penetration is only z height
        Vector3s penetration = Vector3s(SCALAR(0.0), SCALAR(0.0), pos.z());

        return penetration;
    }

    /*!
	 * \brief Compute the endeffector force based on penetration and velocity
	 * @param eePenetration end-effector penetration
	 * @param eeVelocity end-effector velocity
	 * @return resulting force vecttor
	 */
    EEForceLinear computeEEForce(const Vector3s& eePenetration, const Velocity3S& eeVelocity)
    {
    	EEForceLinear forceNormalSpring, forceNormalDamper, forceTangential;

    	// TODO move offset to computePenetration()
        Vector3s eePenetrationOffset = Vector3s(eePenetration(0), eePenetration(1), eePenetration(2) - zOffset_);

        computeNormalSpringForce(forceNormalSpring, eePenetrationOffset);

        computeNormalDamperForce(forceNormalDamper, eePenetrationOffset, eeVelocity);

        computeDryFrictionForce(forceTangential, forceNormalSpring + forceNormalDamper, eeVelocity);

        return  forceNormalSpring + forceNormalDamper + forceTangential;
    }

    /*!
	 * \brief computes damping force in vertical direction\f$ \lambda = -d * \dot{x}z \f$
	 *
	 * @param force force to be computed
	 * @param eePenetration endeffector penetration of the surface
	 * @param velocity endeffector velocity
	 */
    void computeNormalDamperForce(EEForceLinear& force,
    		const Vector3s& penetration, const Velocity3S& velocity)
    {
    	force.setZero();

//    	if (d_ > SCALAR(0.0))
//    	{
    		force(2) = - d_ * velocity(2) * sigmoid(- penetration(2));
//    	}
//    	else
//    	{
//    		throw std::runtime_error("d_ must be strictly positive");
//    	}
    }

    void computeNormalSpringForce(EEForceLinear& force,
    		const Vector3s& penetration)
    {
    	force.setZero();

//        if (alpha_k_ > SCALAR(0.0) && k_ > SCALAR(0.0))
//        {
            force(2) = k_ * TRAIT::exp(- alpha_k_ * penetration(2));
//        }
//        else
//        {
//        	throw std::runtime_error("alpha_k_ and k_ must be strictly positive.");
//        }
    }


    void computeDryFrictionForce(EEForceLinear& forceTangential,
    		const EEForceLinear& forceNormal, const Velocity3S& velocity)
    {
    	Velocity3S velXYUnit, velXY;

		SCALAR eps2(eps_ * eps_);
//		SCALAR eps2(eps_);

		SCALAR velNormSquared = velocity(0)*velocity(0) + velocity(1)*velocity(1) + eps2;

		SCALAR velNorm = TRAIT::sqrt(velNormSquared);

		velXYUnit = velocity / velNorm; // Project velocity onto flat ground

		velXYUnit(2) = (SCALAR)0.0;

		forceTangential = - velXYUnit.toImplementation() * mu_ * forceNormal(2);
    }

	/*!
	 * \brief Compute a sigmoidal function of predefined type.
	 *
	 * Its domain is [0, 1]: 0 for large negative values, 1 for large positive values,
	 * It corresponds to a smoothed step function. The type of sigmoid is specified by the
	 * smoothing property, type_, the amount of smoothing by alpha_s_.
	 *
	 * @param input sigmoid input value
	 * @return sigmoid output value
	 */
	SCALAR sigmoid(const SCALAR input)
    {
		switch (type_)
		{
			case EXP:
				return 1. / (1. + TRAIT::exp(- alpha_s_ * input));
			case TANH:
				return 0.5 * TRAIT::tanh(-0.5 * alpha_s_ * input) + 0.5;
			case ABS:
				return 0.5 * - input * alpha_s_ / (1. + TRAIT::fabs(- input * alpha_s_)) + 0.5;
			default:
				throw std::runtime_error("undefined sigmoid type");
		}
    }

	/*!
	 * \brief Compute an approximated sign function.
	 *
	 * Its domain is [-1, 1]: -1 for large negative values, +1 for large positive values.
	 * See the sigmoid function for smoothing properties.
	 *
	 * @param input function input value
	 * @return function output value
	 */
	SCALAR sigsign(const SCALAR input)
	{
		return 2. * sigmoid(input) - 1;
	}

    //! Instance of the kinematics
    std::shared_ptr<Kinematics> kinematics_;

    //! Type of velocity smoothing
    const SIGMOID_TYPE type_ = EXP; // TODO remove. Keep fixed for now.

    SCALAR k_;        //!< spring constant
    SCALAR d_;        //!< damper constant
    SCALAR alpha_k_;  //!< normal force smoothing coefficient
    SCALAR alpha_s_;  //!< velocity smoothing coefficient
    SCALAR mu_;		  //!< friction coefficient
    SCALAR eps_;	  //!< velocity normalization constant
    SCALAR zOffset_;  //!< vertical offset of the contact pane

    ActiveMap EEactive_;  //!< stores which endeffectors are active, i.e. can make contact

};
} // kuka
} // ct
