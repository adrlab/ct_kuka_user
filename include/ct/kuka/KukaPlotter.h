
#pragma once

#include <ct/core/core-prespec.h>
#include <ct/kuka/pathNames.h>

#include <stdio.h>

namespace ct {
namespace kuka {

class KukaPlotter
{

public:

	static const size_t state_dim = 14;
	static const size_t num_joints = 7;

	//! Default Constructor
	KukaPlotter()
	{

#ifndef PLOTTING_ENABLED
		throw std::runtime_error("Plotting is disabled. Check CMake flag PLOTTING_ENABLED.");
#endif

	}

	template<typename SCALAR>
	void plotJointPositions(const ct::core::TimeArray& time,
			const ct::core::StateVectorArray<state_dim, SCALAR>& states)
	{

		ct::core::StateVectorArray<num_joints, SCALAR> positions;

		for (size_t i=0; i<states.size(); i++)
		{
			positions.push_back(states.at(i).head(num_joints));
		}

		ct::core::plot::figure("Joint Positions");

		plotTimeSeriesVector(time, positions);

	}

	template<typename SCALAR>
	void plotJointVelocities(const ct::core::TimeArray& time,
			const ct::core::StateVectorArray<state_dim, SCALAR>& states)
	{

		ct::core::StateVectorArray<num_joints, SCALAR> velocities;

		for (size_t i=0; i<states.size(); i++)
		{
			velocities.push_back(states.at(i).tail(num_joints));
		}

		ct::core::plot::figure("Joint Velocities");

		plotTimeSeriesVector(time, velocities);
	}

	template<typename SCALAR>
	void plotJointTorques(const ct::core::TimeArray& time,
			const ct::core::ControlVectorArray<num_joints, SCALAR>& controls)
	{

		ct::core::plot::figure("Joint Torques");

		plotTimeSeriesVector(time, controls);
	}

	template<typename SYSTEM>
	void plotEEStates(const ct::core::TimeArray& time,
			const ct::core::StateVectorArray<state_dim, typename SYSTEM::SCALAR>& states,
			const std::shared_ptr<SYSTEM> system,
			const typename SYSTEM::RigidBodyPose_t& basePose)
	{

		const size_t n = states.size();

		using Vec3d = ct::core::StateVector<3, typename SYSTEM::SCALAR>;

		ct::core::DiscreteArray<Vec3d> force(n);
		ct::core::DiscreteArray<Vec3d> position(n);
		ct::core::DiscreteArray<Vec3d> velocity(n);

	    computeEEStates<SYSTEM, Vec3d>(states, basePose, system, position, velocity, force);

	    ct::core::plot::figure("EE Position");
	    plotTimeSeriesVector(time, position);

	    ct::core::plot::figure("EE Velocity");
	    plotTimeSeriesVector(time, velocity);

	    ct::core::plot::figure("EE Force");
	    plotTimeSeriesVector(time, force);
	}

	// TODO parse terms using costfunction instead of vector container
	template<typename SCALAR, typename SCALAR_DIFF>
	void plotCostTerms(const ct::core::TimeArray& time,
			const ct::core::StateVectorArray<state_dim, SCALAR>& states,
			const ct::core::ControlVectorArray<num_joints, SCALAR>& controls,
			const std::vector<std::shared_ptr<ct::optcon::TermBase<
				state_dim, num_joints, SCALAR, SCALAR_DIFF>>>& terms)
	{

		const size_t nTimes = controls.size();
		const size_t nTerms = terms.size();

		using TermScalar = ct::core::StateVector<1, SCALAR>;

//		std::shared_ptr<ct::optcon::TermBase<state_dim, num_joints, SCLAR, SCALAR_DIFF>> = term;

		TermScalar costVec;
		ct::core::DiscreteArray<TermScalar> costs;

//		for (auto it : terms) // not working here
		for (size_t i=0; i<nTerms; i++)
		{
			costs.clear();
			ct::core::plot::figure("Cost Term " + terms.at(i)->getName());

			for (size_t j=0; j<nTimes; j++)
			{
				// Compute activated costs
				costVec[0] = terms.at(i)->eval(states.at(j), controls.at(j), time.at(j)) *
						(SCALAR)terms.at(i)->isActiveAtTime(time.at(j));

				costs.push_back(costVec);
			}

			plotTimeSeriesVector(time, costs);
		}
	}

private:

	template<typename SYSTEM, typename VEC>
	void computeEEStates(const ct::core::StateVectorArray<state_dim, typename SYSTEM::SCALAR>& states,
			const typename SYSTEM::RigidBodyPose_t& basePose,
			const std::shared_ptr<SYSTEM> system,
			ct::core::DiscreteArray<VEC>& position,
			ct::core::DiscreteArray<VEC>& velocity,
		    ct::core::DiscreteArray<VEC>& force)
	{
		const size_t n = states.size();

		force.clear();
		force.reserve(n);

		position.clear();
		position.reserve(n);

		velocity.clear();
		velocity.reserve(n);

		VEC forceVec;

		typedef typename SYSTEM::FixBaseRobotState_t RobotState;

		typename SYSTEM::Dynamics::Kinematics_t kin = typename SYSTEM::Dynamics::Kinematics_t();

		for (size_t i=0; i<n; i++)
		{
			position.push_back(kin.getEEPositionInWorld(
					0, basePose, RobotState::rbdStateFromVector(states.at(i)).jointPositions()).toImplementation());

			velocity.push_back(kin.getEEVelocityInWorld(
					0, RobotState::rbdStateFromVector(states.at(i))).toImplementation());

			system->computeContactForceInWorld(states.at(i), forceVec);

			force.push_back(forceVec);
		}
	}

	template<typename SERIES_VEC>
	void plotTimeSeriesVector(const ct::core::TimeArray& time,
			const SERIES_VEC& seriesVector)
	{
		// Convert series of vectors into multiple series of scalars and plot each.

		const size_t vecDim = SERIES_VEC::value_type::DIM;

		// Convert time to STL vector types
		ct::core::TimeArray::Base timeStl = time.toImplementation();

		// Crop time and data series to the same size
		const size_t seriesCropSize = std::min(timeStl.size(), seriesVector.size());

		timeStl.erase(timeStl.begin()+seriesCropSize, timeStl.end());

		std::vector<typename SERIES_VEC::value_type::value_type> seriesScalar;

		// Plot each dimension
		for (size_t i=0; i<vecDim; i++)
		{
			// Extract time series of this dimension
			seriesScalar.clear();
			seriesScalar.reserve(seriesCropSize);

			for (size_t j=0; j<seriesCropSize; j++)
			{
				seriesScalar.push_back(seriesVector.at(j)(i, 0));
			}

			ct::core::plot::subplot(vecDim, 1, i+1);
			ct::core::plot::plot(timeStl, seriesScalar);
			ct::core::plot::grid(true);
			ct::core::plot::ion();
		}
	}
};


} // kuka
} // ct
