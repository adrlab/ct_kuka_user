#pragma once

#include <ct/core/core.h>
#include <ct/iiwa/iiwa.h>

#include <ct/kuka/EEContactModel.h>
#include <ct/kuka/FixBaseFDSystem.h>

//#include <cppad/cppad.hpp>


namespace ct {
namespace kuka {

template<typename SCALAR_EVAL = double, typename SCALAR = SCALAR_EVAL>
class TermFrictionWork : public optcon::TermBase<14, 7, SCALAR_EVAL, SCALAR>
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	using Scalar = SCALAR_EVAL;
    using ADScalar_t = core::ADScalar;
    using ADCGScalar_t = core::ADCGScalar;

    typedef EEContactModel<typename rbd::iiwa::tpl::Dynamics<Scalar>::Kinematics_t> ContactModel;
    typedef EEContactModel<rbd::iiwa::tpl::Dynamics<ADScalar_t>::Kinematics_t> ContactModelAD;
    typedef EEContactModel<rbd::iiwa::tpl::Dynamics<ADCGScalar_t>::Kinematics_t> ContactModelCG;

//    typedef FixBaseFDSystem<Dynamics, 0, false> IiwaSystem;
//
//    typedef typename IiwaSystem::FixBaseRobotState_t RobotState;
//    typedef typename Kinematics::EEForceLinear Force;
//    typedef typename Kinematics::Velocity3Tpl Velocity;

    static const size_t state_dim = 14;
    static const size_t control_dim = 7;
    static const size_t act_state_dim = 0;

    using BASE = optcon::TermBase<state_dim, control_dim, Scalar, SCALAR>;

    //! default constructor
    TermFrictionWork() :
    			contactFile_(""), name_("TermFrictionWork"), Q_(9999)
    {
    	contactModel_ = std::shared_ptr<ContactModel> (new ContactModel);
    	contactModelAD_ = std::shared_ptr<ContactModelAD> (new ContactModelAD);
    	contactModelCG_ = std::shared_ptr<ContactModelCG> (new ContactModelCG);
    }

    // Constructor taking contact model
    TermFrictionWork(const std::string& contactFile) :
				contactFile_(contactFile), name_("TermFrictionWork"), Q_(9999)
    {
    	contactModel_ = std::shared_ptr<ContactModel> (new ContactModel);
    	contactModelAD_ = std::shared_ptr<ContactModelAD> (new ContactModelAD);
    	contactModelCG_ = std::shared_ptr<ContactModelCG> (new ContactModelCG);

    	contactModel_->loadParameters(contactFile);
    	contactModelAD_->loadParameters(contactFile);
    	contactModelCG_->loadParameters(contactFile);
    }
    //! Constructor taking contact model, name, and work-term weighting
    TermFrictionWork(const std::string& contactFile, const std::string& name, double Q) :
				contactFile_(contactFile), name_(name), Q_(Q)
    {
    	contactModel_ = std::shared_ptr<ContactModel> (new ContactModel);
    	contactModelAD_ = std::shared_ptr<ContactModelAD> (new ContactModelAD);
    	contactModelCG_ = std::shared_ptr<ContactModelCG> (new ContactModelCG);

    	contactModel_->loadParameters(contactFile);
    	contactModelAD_->loadParameters(contactFile);
    	contactModelCG_->loadParameters(contactFile);
    }

    //! deep cloning
    virtual TermFrictionWork* clone() const override { return new TermFrictionWork(*this); }

    //! destructor
    virtual ~TermFrictionWork() = default;

    //! evaluate
    virtual SCALAR evaluate(const Eigen::Matrix<SCALAR, state_dim, 1>& x,
        const Eigen::Matrix<SCALAR, control_dim, 1>& u,
        const SCALAR& t) override
    {
        return evalLocal<ContactModelCG>(contactModelCG_, x, u, t)[0];
    }

    //! eval
    virtual Scalar eval(const Eigen::Matrix<Scalar, state_dim, 1>& x,
        const Eigen::Matrix<Scalar, control_dim, 1>& u,
        const Scalar& t) // TODO cannot override
    {
        return evalLocal<ContactModel>(contactModel_, x, u, t)[0];
    }

    virtual ADScalar_t evaluateCppad(const ct::core::StateVector<state_dim, ADScalar_t>& x,
        const ct::core::ControlVector<control_dim, ADScalar_t>& u,
		ADScalar_t t)
    {
        return evalLocal<ContactModelAD>(contactModelAD_, x, u, t)[0];
    }

    virtual ADCGScalar_t evaluateCppadCg(const ::ct::core::StateVector<state_dim, ADCGScalar_t>& x,
        const ::ct::core::ControlVector<control_dim, ADCGScalar_t>& u,
		ADCGScalar_t t) override
    {
        return evalLocal<ContactModelCG>(contactModelCG_, x, u, t)[0];
    }

    void setWeighting(Scalar weighting)
    {
    	Q_ = weighting;
    }

protected:

    //! evaluate() method templated on the scalar (SC) type
    template <typename CM>
    Eigen::Matrix<typename CM::SCALAR, 1, 1> evalLocal(
    		const std::shared_ptr<CM> contactModel,
    		const Eigen::Matrix<typename CM::SCALAR, state_dim, 1>& x,
			const Eigen::Matrix<typename CM::SCALAR, control_dim, 1>& u,
			const typename CM::SCALAR& t)
    {

    	using SC = typename CM::SCALAR;
        using TRAIT = typename iit::rbd::tpl::TraitSelector<SC>::Trait;

        using RobotState = rbd::FixBaseRobotState<7, 0, SC>;

        using Force = typename CM::EEForceLinear;
        using Velocity = typename CM::Velocity3S;


        RobotState robotState = RobotState(core::StateVector<state_dim, SC>(x));

        Force contactForce = contactModel->computeContactForces(robotState.toRBDState())[0];
        Velocity velocity = contactModel->kinematics()->getEEVelocityInWorld(0, robotState.toRBDState());

        // Inner product of velocity with force in horizontal direction only
        SC power = contactForce(0) * velocity(0) + contactForce(1) * velocity(1);
//        SC penalty = (SC)-Q_ * power * power;
        SC penalty = (SC)Q_ * power;

        return Eigen::Matrix<SC, 1, 1>(penalty);
    }

private:
    Scalar Q_;

    std::string name_ , contactFile_;

    std::shared_ptr<ContactModel> contactModel_;
    std::shared_ptr<ContactModelAD> contactModelAD_;
    std::shared_ptr<ContactModelCG> contactModelCG_;
};


}  // namespace ct
}  // namespace iiwa
