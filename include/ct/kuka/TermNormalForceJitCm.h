#pragma once

#include <ct/iiwa/iiwa.h>

#include <ct/kuka/EEContactModel.h>
#include <ct/kuka/FixBaseFDSystem.h>


namespace ct {
namespace iiwa {

/*!
 * \brief TermNormalForceJIT Cost term penalizing the z component of the contact force.
 *
 */
class TermNormalForceJitCm : public optcon::TermBase<14, 7, double>
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    using ADCGScalar = ct::core::ADCGScalar;

    typedef typename ct::core::tpl::TraitSelector<ADCGScalar>::Trait AdcgTrait;

    typedef ct::kuka::EEContactModel<rbd::iiwa::tpl::Dynamics<ADCGScalar>::Kinematics_t> ContactModelCG;
    typedef ct::kuka::EEContactModel<rbd::iiwa::tpl::Dynamics<double>::Kinematics_t> ContactModel;

    static const size_t N_JOINTS = 7;
    static const size_t STATE_DIM = 14;
    static const size_t CONTROL_DIM = 7;
    static const size_t NUM_CONTACT_PARA = ContactModel::NUM_PARA;
    static const size_t NUM_WEIGHTS = 1;

    static const size_t AD_PARAMETER_DIM = STATE_DIM + CONTROL_DIM + NUM_CONTACT_PARA + NUM_WEIGHTS + 1;

    using BASE = ct::optcon::TermBase<STATE_DIM, CONTROL_DIM, double>;

    using state_matrix_t = Eigen::Matrix<double, STATE_DIM, STATE_DIM>;
    using control_matrix_t = Eigen::Matrix<double, CONTROL_DIM, CONTROL_DIM>;
    using control_state_matrix_t = Eigen::Matrix<double, CONTROL_DIM, STATE_DIM>;

    using DerivativesCppadJIT = ct::core::DerivativesCppadJIT<AD_PARAMETER_DIM, 1>;

    using RobotState = rbd::FixBaseRobotState<N_JOINTS, 0, ADCGScalar>;
    using Force = ContactModelCG::EEForceLinear;

    //! default constructor
    TermNormalForceJitCm() :
				q_lin_(9999),
				q_quad_(9999),
				FN_ref_(9999),
				contactModel_(std::shared_ptr<ContactModel>(new ContactModel())),
				contactModelCG_(ContactModelCG()) {}

    //! Constructor taking contact model parameters from file
    TermNormalForceJitCm(const std::shared_ptr<ContactModel> contactModel,
    		const std::string& name = "TermNormalForceJIT",
			double q_lin = 9999, double q_quad = 9999, double forceReference = 9999) :
				BASE(name),
				q_lin_(q_lin),
				q_quad_(q_quad),
				FN_ref_(forceReference),
				contactModel_(contactModel),
				contactModelCG_(ContactModelCG()) {}


    //! deep cloning
    virtual TermNormalForceJitCm* clone() const override { return new TermNormalForceJitCm(*this); }

    //! destructor
    virtual ~TermNormalForceJitCm() = default;

    void loadConfigFile(const std::string& filename, const std::string& termName, bool verbose)
    {
    	ct::core::loadScalar(filename, ".q_lin", q_lin_, termName);
    	ct::core::loadScalar(filename, ".q_quad", q_quad_, termName);
    	ct::core::loadScalar(filename, ".force_reference", FN_ref_, termName);

        if (verbose)
        {
            std::cout << "Read q_lin = " << q_lin_ << std::endl;
            std::cout << "Read q_quad = " << q_quad_ << std::endl;
            std::cout << "Read force_reference = " << FN_ref_ << std::endl;
        }
    }

    void setLinearWeight(double q_lin)
    {
    	q_lin_ = q_lin;
    }

    //! setup the AD Derivatives
    void setup(bool verbose = false)
    {
    	// map term evaluation function to AD function
        costFun_ = [&](const Eigen::Matrix<ADCGScalar, AD_PARAMETER_DIM, 1>& input) { return evalLocal(input); };

        // set up derivatives
        derivativesCppadJIT_ =
        		std::shared_ptr<DerivativesCppadJIT>(new DerivativesCppadJIT(costFun_, AD_PARAMETER_DIM, 1));

        ct::core::DerivativesCppadSettings settings;
        settings.createForwardZero_ = true;
    	settings.createReverseOne_ = true;
        settings.createJacobian_ = true;
        settings.createHessian_ = true;

        settings.compiler_ = ct::core::DerivativesCppadSettings::CompilerType::CLANG;
        settings.useDynamicLibrary_ = false;

        std::cout << "TermNormalForceJIT: compiling JIT for " << name_ << std::endl;
        derivativesCppadJIT_->compileJIT(settings, name_, verbose);
    }

//    virtual double eval(const Eigen::Matrix<double, STATE_DIM, 1>& x,
//        const Eigen::Matrix<double, CONTROL_DIM, 1>& u,
//        const double& t) override
//    {
//        return this->computeActivation(t) * (double)this->isActiveAtTime(t) * evaluate(x, u, t);
//    }

    //! evaluate
    virtual double evaluate(const Eigen::Matrix<double, STATE_DIM, 1>& x,
        const Eigen::Matrix<double, CONTROL_DIM, 1>& u,
        const double& t) override
    {
        setCurrentStateAndControl(x, u, t);
        return derivativesCppadJIT_->forwardZero(adParameterVector_)(0);
    }

    //! compute derivative of this cost term w.r.t. the state
    virtual core::StateVector<STATE_DIM> stateDerivative(const core::StateVector<STATE_DIM>& x,
        const core::ControlVector<CONTROL_DIM>& u,
        const double& t) override
    {
        setCurrentStateAndControl(x, u, t);
        Eigen::Matrix<double, 1, AD_PARAMETER_DIM> jacTot = derivativesCppadJIT_->jacobian(adParameterVector_);
        return jacTot.template leftCols<STATE_DIM>().transpose();
    }

    //! compute derivative of this cost term w.r.t. the control input
    virtual core::ControlVector<CONTROL_DIM> controlDerivative(const core::StateVector<STATE_DIM>& x,
        const core::ControlVector<CONTROL_DIM>& u,
        const double& t) override
    {
        setCurrentStateAndControl(x, u, t);
        Eigen::Matrix<double, 1, AD_PARAMETER_DIM> jacTot = derivativesCppadJIT_->jacobian(adParameterVector_);
        return jacTot.template block<1, CONTROL_DIM>(0, STATE_DIM).transpose();
    }

    //! compute second order derivative of this cost term w.r.t. the state
    virtual state_matrix_t stateSecondDerivative(const core::StateVector<STATE_DIM>& x,
        const core::ControlVector<CONTROL_DIM>& u,
        const double& t) override
    {
        setCurrentStateAndControl(x, u, t);
        Eigen::Matrix<double, 1, 1> w(1.0);
        Eigen::MatrixXd hesTot = derivativesCppadJIT_->hessian(adParameterVector_, w);
        return hesTot.template block<STATE_DIM, STATE_DIM>(0, 0);
    }

    //! compute second order derivative of this cost term w.r.t. the control input
    virtual control_matrix_t controlSecondDerivative(const core::StateVector<STATE_DIM>& x,
        const core::ControlVector<CONTROL_DIM>& u,
        const double& t) override
    {
        setCurrentStateAndControl(x, u, t);
        Eigen::Matrix<double, 1, 1> w(1.0);
        Eigen::MatrixXd hesTot = derivativesCppadJIT_->hessian(adParameterVector_, w);
        return hesTot.template block<CONTROL_DIM, CONTROL_DIM>(STATE_DIM, STATE_DIM);
    }

    //! compute the cross-term derivative (state-control) of this cost function term
    virtual control_state_matrix_t stateControlDerivative(const core::StateVector<STATE_DIM>& x,
        const core::ControlVector<CONTROL_DIM>& u,
        const double& t) override
    {
        setCurrentStateAndControl(x, u, t);
        Eigen::Matrix<double, 1, 1> w(1.0);
        Eigen::MatrixXd hesTot = derivativesCppadJIT_->hessian(adParameterVector_, w);
        return hesTot.template block<CONTROL_DIM, STATE_DIM>(STATE_DIM, 0);
    }



protected:

    //! evalLocal() method runnng on the ADCGScalar type
    Eigen::Matrix<ADCGScalar, 1, 1> evalLocal(const Eigen::Matrix<ADCGScalar, AD_PARAMETER_DIM, 1>& adParams)
    {
        // friction force times velocity
        Eigen::Matrix<ADCGScalar, STATE_DIM, 1> x = adParams.template segment<STATE_DIM>(0);
        Eigen::Matrix<ADCGScalar, CONTROL_DIM, 1> u = adParams.template segment<CONTROL_DIM>(STATE_DIM);
        Eigen::Matrix<ADCGScalar, NUM_CONTACT_PARA, 1> cp =
        		adParams.template segment<NUM_CONTACT_PARA>(STATE_DIM + CONTROL_DIM);

        ADCGScalar weight = adParams(STATE_DIM + CONTROL_DIM + NUM_CONTACT_PARA);

    	contactModelCG_.k() 		= cp(0);
    	contactModelCG_.d() 		= cp(1);
    	contactModelCG_.alpha_k() 	= cp(2);
    	contactModelCG_.alpha_s() 	= cp(3);
    	contactModelCG_.mu() 		= cp(4);
    	contactModelCG_.eps() 		= cp(5);
    	contactModelCG_.zOffset() 	= cp(6);

        RobotState robotState = RobotState(core::StateVector<STATE_DIM, ADCGScalar>(x));

        Force contactForce = contactModelCG_.computeContactForces(robotState.toRBDState())[0];

        ADCGScalar penalty = (ADCGScalar)weight * AdcgTrait::log(contactForce(2));

        return Eigen::Matrix<ADCGScalar, 1, 1>(penalty);
    }

private:

    void setCurrentStateAndControl(const Eigen::Matrix<double, STATE_DIM, 1>& x,
        const Eigen::Matrix<double, CONTROL_DIM, 1>& u,
        const double& t)
    {
        adParameterVector_.template segment<STATE_DIM>(0) = x;
        adParameterVector_.template segment<CONTROL_DIM>(STATE_DIM) = u;
        adParameterVector_(STATE_DIM + CONTROL_DIM + NUM_CONTACT_PARA) = q_lin_;
        adParameterVector_(AD_PARAMETER_DIM - 1) = t;

        adParameterVector_(STATE_DIM + CONTROL_DIM + 0) = contactModel_->k();
        adParameterVector_(STATE_DIM + CONTROL_DIM + 1) = contactModel_->d();
        adParameterVector_(STATE_DIM + CONTROL_DIM + 2) = contactModel_->alpha_k();
        adParameterVector_(STATE_DIM + CONTROL_DIM + 3) = contactModel_->alpha_s();
        adParameterVector_(STATE_DIM + CONTROL_DIM + 4) = contactModel_->mu();
        adParameterVector_(STATE_DIM + CONTROL_DIM + 5) = contactModel_->eps();
        adParameterVector_(STATE_DIM + CONTROL_DIM + 6) = contactModel_->zOffset();
    }

    //! the cppad JIT derivatives
    std::shared_ptr<DerivativesCppadJIT> derivativesCppadJIT_;

    //! cppad functions
    typename DerivativesCppadJIT::FUN_TYPE_CG costFun_;

    //! AD Parameter Vector
    Eigen::Matrix<double, AD_PARAMETER_DIM, 1> adParameterVector_;

    ContactModelCG contactModelCG_; //! Contact model required for AD tape recording
    std::shared_ptr<ContactModel> contactModel_; //! Contact model for parameter retrieval after compilation

    double q_lin_; //! Linear cost weight
    double q_quad_; //! Quadratic cost weight
    double FN_ref_; //! Normal force reference

};

}  // namespace kuka
}  // namespace ct
