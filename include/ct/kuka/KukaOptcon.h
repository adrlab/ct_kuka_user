/*
 * KukaOptcon.h
 *
 *  Created on: Jul 24, 2020
 *      Author: johannes
 */

#pragma once

#include <ct/optcon/optcon-prespec.h>


namespace ct {
namespace kuka {

static const size_t n_joints = 7;
static const size_t state_dim = 14;
static const size_t control_dim = 7;

using ScalarCG = CppAD::AD< CppAD::cg::CG<double>>;

using LinearSystem_t 	= ct::core::LinearSystem<state_dim, control_dim, double>;

using CostFunctionAD_t 	= ct::optcon::CostFunctionAD<state_dim, control_dim, double>;
using TermQuadratic_t 	= ct::optcon::TermQuadratic<state_dim, control_dim, double, double>;
using TermQuadraticAD_t = ct::optcon::TermQuadratic<state_dim, control_dim, double, ScalarCG>;

}
}

extern template class ct::core::LinearSystem<ct::kuka::state_dim, ct::kuka::control_dim, double>;

extern template class ct::optcon::CostFunctionAD<ct::kuka::state_dim, ct::kuka::control_dim, double>;
extern template class ct::optcon::TermQuadratic<ct::kuka::state_dim, ct::kuka::control_dim, double, double>;
extern template class ct::optcon::TermQuadratic<ct::kuka::state_dim, ct::kuka::control_dim, double, ct::kuka::ScalarCG>;

