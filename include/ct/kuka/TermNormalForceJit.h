#pragma once

#include <ct/iiwa/iiwa.h>

//#include <ct/kuka/EEContactModel.h>
#include <ct/kuka/FixBaseFDSystem.h>


namespace ct {
namespace kuka {

class TermNormalForceJit : public optcon::TermBase<14, 7, double>
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    using SCALAR = ct::core::ADCGScalar;

    static const size_t STATE_DIM = 14;
    static const size_t CONTROL_DIM = 7;
    static const size_t AD_PARAMETER_DIM = STATE_DIM + CONTROL_DIM + 1;

    using BASE = ct::optcon::TermBase<STATE_DIM, CONTROL_DIM, double>;

    using state_matrix_t = Eigen::Matrix<double, STATE_DIM, STATE_DIM>;
    using control_matrix_t = Eigen::Matrix<double, CONTROL_DIM, CONTROL_DIM>;
    using control_state_matrix_t = Eigen::Matrix<double, CONTROL_DIM, STATE_DIM>;

    using DerivativesCppadJIT = ct::core::DerivativesCppadJIT<AD_PARAMETER_DIM, 1>;

    typedef EEContactModel<rbd::iiwa::tpl::Dynamics<SCALAR>::Kinematics_t> ContactModelCG;


    //! default constructor
    TermNormalForceJit() :
    			contactFile_(""),
				q_lin_(9999),
				q_quad_(9999),
				contactForceNormalReference_(9999)
    {
    	name_ = "TermNormalForceAnalytical";

    	contactModelCG_ = std::shared_ptr<ContactModelCG> (new ContactModelCG);

    	setup();
    }

    //! Constructor taking contact model
    TermNormalForceJit(const std::string& contactFile) :
				contactFile_(contactFile),
				q_lin_(9999),
				q_quad_(9999),
				contactForceNormalReference_(9999)
    {
    	name_ = "TermNormalForceAnalytical";

    	contactModelCG_ = std::shared_ptr<ContactModelCG> (new ContactModelCG);
    	contactModelCG_->loadParameters(contactFile);

    	setup();
    }

    //! Constructor taking contact model, name, and work-term weighting
    TermNormalForceJit(const std::string& contactFile,
    		const std::string& name,
			const double q_lin,
			const double q_quad,
			const double forceReference) :
				contactFile_(contactFile),
				q_lin_(q_lin),
				q_quad_(q_quad),
				contactForceNormalReference_(forceReference)
    {
    	name_ = name;

    	contactModelCG_ = std::shared_ptr<ContactModelCG> (new ContactModelCG);
    	contactModelCG_->loadParameters(contactFile);

    	setup();
    }


    //! deep cloning
    virtual TermNormalForceJit* clone() const override { return new TermNormalForceJit(*this); }

    //! destructor
    virtual ~TermNormalForceJit() = default;

    void setLinearWeight(double q_lin)
    {
    	q_lin_ = q_lin;
    }

    //! setup the AD Derivatives
    void setup()
    {
    	// map term evaluation function to AD function
        costFun_ = [&](
            const Eigen::Matrix<SCALAR, AD_PARAMETER_DIM, 1>& inputParams) { return evalLocal(inputParams); };

        // set up derivatives
        derivativesCppadJIT_ =
            std::shared_ptr<DerivativesCppadJIT>(new DerivativesCppadJIT(costFun_, AD_PARAMETER_DIM, 1));

        ct::core::DerivativesCppadSettings settings;
        settings.createForwardZero_ = true;
        settings.createJacobian_ = true;
        settings.createHessian_ = true;

        std::cout << "compiling JIT for " << this->getName() << std::endl;
        derivativesCppadJIT_->compileJIT(settings, "TermNormalForceAnalytical");
    }

    void setCurrentStateAndControl(const Eigen::Matrix<double, STATE_DIM, 1>& x,
        const Eigen::Matrix<double, CONTROL_DIM, 1>& u,
        const double& t)
    {
        adParameterVector_.template segment<STATE_DIM>(0) = x;
        adParameterVector_.template segment<CONTROL_DIM>(STATE_DIM) = u;
        adParameterVector_(AD_PARAMETER_DIM - 1) = t;
    }

    virtual double scalarEval(const Eigen::Matrix<double, STATE_DIM, 1>& x,
        const Eigen::Matrix<double, CONTROL_DIM, 1>& u,
        const double& t) //override
    {
        return this->computeActivation(t) * (double)this->isActiveAtTime(t) * evaluate(x, u, t);
    }

    //! evaluate
    virtual double evaluate(const Eigen::Matrix<double, STATE_DIM, 1>& x,
        const Eigen::Matrix<double, CONTROL_DIM, 1>& u,
        const double& t) override
    {
        setCurrentStateAndControl(x, u, t);
        return derivativesCppadJIT_->forwardZero(adParameterVector_)(0);
    }

    //! compute derivative of this cost term w.r.t. the state
    virtual core::StateVector<STATE_DIM> stateDerivative(const core::StateVector<STATE_DIM>& x,
        const core::ControlVector<CONTROL_DIM>& u,
        const double& t) override
    {
        setCurrentStateAndControl(x, u, t);
        Eigen::Matrix<double, 1, AD_PARAMETER_DIM> jacTot = derivativesCppadJIT_->jacobian(adParameterVector_);
        return jacTot.template leftCols<STATE_DIM>().transpose();
    }

    //! compute derivative of this cost term w.r.t. the control input
    virtual core::ControlVector<CONTROL_DIM> controlDerivative(const core::StateVector<STATE_DIM>& x,
        const core::ControlVector<CONTROL_DIM>& u,
        const double& t) override
    {
        setCurrentStateAndControl(x, u, t);
        Eigen::Matrix<double, 1, AD_PARAMETER_DIM> jacTot = derivativesCppadJIT_->jacobian(adParameterVector_);
        return jacTot.template block<1, CONTROL_DIM>(0, STATE_DIM).transpose();
    }

    //! compute second order derivative of this cost term w.r.t. the state
    virtual state_matrix_t stateSecondDerivative(const core::StateVector<STATE_DIM>& x,
        const core::ControlVector<CONTROL_DIM>& u,
        const double& t) override
    {
        setCurrentStateAndControl(x, u, t);
        Eigen::Matrix<double, 1, 1> w(1.0);
        Eigen::MatrixXd hesTot = derivativesCppadJIT_->hessian(adParameterVector_, w);
        return hesTot.template block<STATE_DIM, STATE_DIM>(0, 0);
    }

    //! compute second order derivative of this cost term w.r.t. the control input
    virtual control_matrix_t controlSecondDerivative(const core::StateVector<STATE_DIM>& x,
        const core::ControlVector<CONTROL_DIM>& u,
        const double& t) override
    {
        setCurrentStateAndControl(x, u, t);
        Eigen::Matrix<double, 1, 1> w(1.0);
        Eigen::MatrixXd hesTot = derivativesCppadJIT_->hessian(adParameterVector_, w);
        return hesTot.template block<CONTROL_DIM, CONTROL_DIM>(STATE_DIM, STATE_DIM);
    }

    //! compute the cross-term derivative (state-control) of this cost function term
    virtual control_state_matrix_t stateControlDerivative(const core::StateVector<STATE_DIM>& x,
        const core::ControlVector<CONTROL_DIM>& u,
        const double& t) override
    {
        setCurrentStateAndControl(x, u, t);
        Eigen::Matrix<double, 1, 1> w(1.0);
        Eigen::MatrixXd hesTot = derivativesCppadJIT_->hessian(adParameterVector_, w);
        return hesTot.template block<CONTROL_DIM, STATE_DIM>(STATE_DIM, 0);
    }



protected:

    //! evalLocal() method runnng on the ADCGScalar type
    Eigen::Matrix<SCALAR, 1, 1> evalLocal(const Eigen::Matrix<SCALAR, AD_PARAMETER_DIM, 1>& adParams)
    {
        // friction force times velocity
        Eigen::Matrix<SCALAR, STATE_DIM, 1> x = adParams.template segment<STATE_DIM>(0);
        Eigen::Matrix<SCALAR, CONTROL_DIM, 1> u = adParams.template segment<CONTROL_DIM>(STATE_DIM);

        using TRAIT = typename iit::rbd::tpl::TraitSelector<SCALAR>::Trait;

        using RobotState = rbd::FixBaseRobotState<CONTROL_DIM, 0, SCALAR>;

        using Force = ContactModelCG::EEForceLinear;

        RobotState robotState = RobotState(core::StateVector<STATE_DIM, SCALAR>(x));

        Force contactForce = contactModelCG_->computeContactForces(robotState.toRBDState())[0];

        SCALAR penalty = (SCALAR)q_lin_ * log(contactForce(2));

        return Eigen::Matrix<SCALAR, 1, 1>(penalty);
    }

    //! the cppad JIT derivatives
    std::shared_ptr<DerivativesCppadJIT> derivativesCppadJIT_;

    //! cppad functions
    typename DerivativesCppadJIT::FUN_TYPE_CG costFun_;

    //! AD Parameter Vector
    Eigen::Matrix<double, AD_PARAMETER_DIM, 1> adParameterVector_;

    std::shared_ptr<ContactModelCG> contactModelCG_;

    double q_lin_, q_quad_;
    double contactForceNormalReference_;

    std::string contactFile_;

};

}  // namespace kuka
}  // namespace ct
