
#pragma once

#include <ct/rbd/rbd-prespec.h>

#include <ct/kuka/EEContactModel.h>

namespace ct {
namespace kuka {

/*!
 * \brief Fix base rigid body system for the kuka robots with contact model.
 *
 * A fix base rigid body system that uses forward dynamics. The control input vector is assumed to consist of
 * - joint torques and
 * - end effector forces expressed in the world frame
 *
 * The overall state vector is arranged in the order
 * - joint positions
 * - joint velocities
 *
 * \warning when modelled with RobCoGen, the base pose must be rotated "against gravity" (RobCoGen modelling assumption)
 */
template <class RBD>
class FixBaseFDSystem :
     	public rbd::RBDSystem<RBD, false>,
    	public core::ControlledSystem<RBD::NSTATE, RBD::NJOINTS, typename RBD::SCALAR>
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	using Dynamics = RBD;
    using SCALAR = typename Dynamics::SCALAR;
    using ContactModel = ct::kuka::EEContactModel<typename Dynamics::Kinematics_t>;

    //! ID of the end effector in contact
    static const size_t EE_ID = 0;
    //! number of end-effectors of the kuka robot
    static const size_t N_EE = Dynamics::N_EE;
    //! joint torques plus end-effector forces
    static const size_t NJOINTS = Dynamics::NJOINTS;
    //! rigid body system state-dim plus actuator state-dim
    static const size_t STATE_DIM = Dynamics::NSTATE;
    //! we assume one control input per joint
    static const size_t CONTROL_DIM = NJOINTS;
    //! Extended state comprising system state + contact model parameters
    static const size_t STATE_DIM_EXT = STATE_DIM + (size_t) ContactModel::NUM_PARA;
    //! No actuator modelling
    static const size_t ACTUATOR_STATE_DIM = 0;

    using Base = core::ControlledSystem<STATE_DIM, CONTROL_DIM, SCALAR>;

    using ActuatorDynamics_t = rbd::ActuatorDynamics<ACTUATOR_STATE_DIM, NJOINTS, SCALAR>;

    using RigidBodyPose_t = rbd::tpl::RigidBodyPose<SCALAR>;
    using FixBaseRobotState_t = rbd::FixBaseRobotState<NJOINTS, 0, SCALAR>;

    // typedefs state and controls
    using state_vector_t = typename FixBaseRobotState_t::state_vector_t;
    using state_vector_ext = typename ct::core::StateVector<STATE_DIM_EXT, SCALAR>;
    using control_vector_t = core::ControlVector<CONTROL_DIM, SCALAR>;

    using JointState_t = rbd::JointState<NJOINTS, SCALAR>;
    using JointAcceleration_t = rbd::JointAcceleration<NJOINTS, SCALAR>;

    using Force = typename Dynamics::Kinematics_t::EEForceLinear;
    using Wrench = typename Dynamics::Kinematics_t::EEForce;


    //! constructor
    FixBaseFDSystem(const std::shared_ptr<ContactModel>& contactModel,
    	const RigidBodyPose_t& basePose = RigidBodyPose_t()) :
    		Base(), actuatorDynamics_(), basePose_(basePose),
			dynamics_(Dynamics()), contactModel_(contactModel)
    {
    	// Share kinematics instance with contact model
    	contactModel_->setKinematics(dynamics_.kinematicsPtr());
    }

    //! copy constructor
    FixBaseFDSystem(const FixBaseFDSystem& arg) :
    	Base(arg), actuatorDynamics_(), basePose_(arg.basePose_),
		dynamics_(Dynamics()), contactModel_(arg.contactModel_)
    {
    	// Share kinematics instance with contact model
    	contactModel_->setKinematics(dynamics_.kinematicsPtr());
    }

    //! destructor
    virtual ~FixBaseFDSystem() {}

    //! deep cloning
    virtual FixBaseFDSystem<Dynamics>* clone() const override { return new FixBaseFDSystem<Dynamics>(*this); }

    //! get dynamics
    virtual Dynamics& dynamics() override { return dynamics_; }
    //! get dynamics (const)
    virtual const Dynamics& dynamics() const override { return dynamics_; }

    /*!
	 * \brief Compute the controlled dynamics of the fixed base robotic system
	 * @param state State vector (position and velocity)
	 * @param t Time
	 * @param control Control torques
	 * @return derivative State derivative vector
	 */
    void computeControlledDynamics(const state_vector_t& state,
        const SCALAR& t,
        const control_vector_t& control,
		state_vector_t& derivative)
    {
        FixBaseRobotState_t robotState(state);

        derivative.template topRows<NJOINTS>() = robotState.joints().getVelocities();

        // Cache updated rbd state
        typename Dynamics::ExtLinkForces_t linkForces(Eigen::Matrix<SCALAR, 6, 1>::Zero());

        // Compute rotation matrix from ee to base frame
//        typename Dynamics::Kinematics_t::Matrix3Tpl R_base_ee;
//        Force w_Fc;

        // Compute contact forces
        Force w_Fc = contactModel_->computeContactForces(robotState.toRBDState(basePose_))[0];

        // Map from world to ee frame
		// turn linear froce into wrench (spatial force)
        Wrench w_Wc, ee_Wc;
        w_Wc.setZero();
        w_Wc.tail(3) = w_Fc;

        computeEEForceInLink(state, w_Wc, ee_Wc);

        auto endEffector = dynamics_.kinematics().getEndEffector(EE_ID);
        size_t linkId = endEffector.getLinkId();

        linkForces[static_cast<typename Dynamics::ROBCOGEN::LinkIdentifiers>(linkId)] = ee_Wc;

        typename Dynamics::JointAcceleration_t jAcc;

        dynamics_.FixBaseForwardDynamics(
            robotState.joints(), control.template head<Dynamics::NJOINTS>(), linkForces, jAcc);

        derivative.template segment<Dynamics::NJOINTS>(NJOINTS) = jAcc.getAcceleration();
    }

    /*!
     * \brief Compute controlled robot dynamics with contact model parameters. Required for ADCG linearization.
     *
     * Compute controlled dynamics of the robotic system with contact model parameters stacked in the state.
     */
    void computeControlledDynamicsWithCMPara(const state_vector_ext& state,
        const SCALAR& t,
        const control_vector_t& control,
		state_vector_ext& derivative)
    {

    	state_vector_t trueState = state.template topRows<STATE_DIM>();

    	// Set CM parameters from extended state input
    	ct::core::StateVector<ContactModel::NUM_PARA, SCALAR> cmParameters =
    			state.template bottomRows<ContactModel::NUM_PARA>();

    	contactModel_->k() 			= cmParameters[0];
    	contactModel_->d() 			= cmParameters[1];
    	contactModel_->alpha_k() 	= cmParameters[2];
    	contactModel_->alpha_s() 	= cmParameters[3];
    	contactModel_->mu() 		= cmParameters[4];
    	contactModel_->eps() 		= cmParameters[5];
    	contactModel_->zOffset() 	= cmParameters[6];

    	state_vector_t trueDerivative = state_vector_t::Zero();

    	computeControlledDynamics(trueState, t, control, trueDerivative);

    	// Keep CM parameters as dummy values in output
    	derivative << trueDerivative, cmParameters;
    }


    void computeContactForceInWorld(const state_vector_t& state, Force& w_force)
    {
        FixBaseRobotState_t robotState(state);

        w_force = contactModel_->computeContactForces(robotState.toRBDState(basePose_))[0];
    }

    void computeEEForceInLink(const state_vector_t& state, const Wrench& w_force, Wrench& ee_force)
    {
    	FixBaseRobotState_t robotState(state);

        ee_force = dynamics_.kinematics().mapForceFromWorldToLink(
        		w_force, basePose_, robotState.joints().getPositions(), EE_ID);
    }

    //! compute inverse dynamics torques
    control_vector_t computeIDTorques(const JointState_t& jState,
        const JointAcceleration_t& jAcc = JointAcceleration_t(Eigen::Matrix<SCALAR, NJOINTS, 1>::Zero()))
    {
        control_vector_t u;
        dynamics_.FixBaseID(jState, jAcc, u);
        return u;
    }

    //! get pointer to actuator dynamics
    std::shared_ptr<ActuatorDynamics_t> getActuatorDynamics() { return actuatorDynamics_; }

private:
    //! a "dummy" base pose which sets the robot's "fixed" position in the world
    rbd::tpl::RigidBodyPose<SCALAR> basePose_;

    //! rigid body dynamics container
    Dynamics dynamics_;

    //! Contact model influencing robot dynamics through external forces
    std::shared_ptr<ContactModel> contactModel_;

    //! pointer to the dummy actuator dynamics
    std::shared_ptr<ActuatorDynamics_t> actuatorDynamics_;
};

}  // namespace kuka
}  // namespace ct
