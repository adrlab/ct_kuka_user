#pragma once

#include <ros/ros.h>

template <class Kinematics>
static void visualizeTrajectory(ct::ros::RBDStatePublisher& publisher,
    const ct::core::StateVectorArray<ct::rbd::FixBaseRobotState<Kinematics::NJOINTS>::NSTATE>& x,
	const ct::core::DiscreteArray<ct::rbd::SpatialForceVector<double>>& eeforce,
	const ct::rbd::tpl::RigidBodyPose<double>& basePose,
    double dt)
{
    using RobotState_t = ct::rbd::FixBaseRobotState<Kinematics::NJOINTS>;

    std::shared_ptr<Kinematics> kukaKin(new Kinematics);

    ros::Rate publishRate(1. / dt);

    for (size_t i = 0; i < x.size(); i++)
    {
        ct::rbd::RBDState<Kinematics::NJOINTS> state;

        for (int j = 0; j < x[j].size(); j++)
        {
            if (!std::isfinite(x[i](j)))
                throw std::runtime_error("Not finite");
        }

        state = RobotState_t::rbdStateFromVector(x[i]);
        state.basePose() = basePose;

        ::ros::Time publishtime = ::ros::Time::now();

//        publisher.publishEEPoseTF(kukaKin->getEEPoseInWorld(0, basePose, state.jointPositions()), publishtime);

        // test ee force
//        publisher.publishEEForce(eeforce[i], publishtime);
        publisher.publishState(state, publishtime);

        publishRate.sleep();
    }

    ros::Duration(1.0).sleep();
}
