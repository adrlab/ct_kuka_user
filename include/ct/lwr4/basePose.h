
#include <Eigen/Core>

namespace ct {
namespace lwr4 {

// Pose of the right KUKA arm base of AMD's Apollo robot, relative to Apollo's (global) base frame.
const Eigen::Vector3d APOLLO_BASE_POSITION_XYZ = Eigen::Vector3d(0.4, 0.2, 0.0);
const Eigen::Vector3d APOLLO_BASE_ORIENTATION_XYZ = Eigen::Vector3d(0.0, 0.0, 0.54); // xyz local Euler Angles

}
}
