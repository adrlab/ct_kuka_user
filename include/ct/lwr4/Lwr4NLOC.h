
#pragma once

//#include <ct/rbd/rbd-prespec.h>

#include <ct/lwr4/lwr4.h>

#include <ct/lwr4/TermFrictionWork.h>

#include <ct/kuka/EEContactModel.h>

class Lwr4NLOC
{
public:
	// Typedef robot and environment for autodiff linearization
	typedef double Scalar;
	typedef CppAD::AD< CppAD::cg::CG<Scalar>> ScalarCG;

    // Standard types
	typedef ct::rbd::lwr4::tpl::Dynamics<Scalar> Lwr4Dynamics;
    typedef ct::kuka::FixBaseFDSystem<Lwr4Dynamics> Lwr4System;
    typedef ct::kuka::EEContactModel<typename Lwr4Dynamics::Kinematics_t> ContactModel;

	// AD types:
	typedef ct::rbd::lwr4::tpl::Dynamics<ScalarCG> Lwr4DynamicsAD;
    typedef ct::kuka::FixBaseFDSystem<Lwr4DynamicsAD> Lwr4SystemAD;
    typedef ct::kuka::EEContactModel<typename Lwr4DynamicsAD::Kinematics_t> ContactModelAD;

    static const size_t n_joints = Lwr4System::NJOINTS;
    static const size_t state_dim = Lwr4System::STATE_DIM;
    static const size_t control_dim = Lwr4System::CONTROL_DIM;

    using LinearSystem_t = ct::core::LinearSystem<state_dim, control_dim, Scalar>;
    using ADCodegenLinearizer_t = ct::core::ADCodegenLinearizer<state_dim, control_dim, Scalar>;

    using CostFunctionAD_t = ct::optcon::CostFunctionAD<state_dim, control_dim, Scalar>;
    using TermQuadratic_t = ct::optcon::TermQuadratic<state_dim, control_dim, Scalar, Scalar>;

    // TODO debug ADCG for the tracking term. Right now only analytical derivatives are functional.
//    using TermQuadTrackingAD = ct::optcon::TermQuadTracking<state_dim, control_dim, Scalar, ScalarCG>;
    using TermQuadTracking_t = ct::optcon::TermQuadTracking<state_dim, control_dim, Scalar, Scalar>;

    using TermTaskspacePose_t = ct::rbd::TermTaskspacePoseCG<typename Lwr4DynamicsAD::Kinematics_t, false, state_dim, control_dim>;

    using TermFrictionWorkAD = ct::lwr4::TermFrictionWork<double, ScalarCG>;

    using FixBaseNLOC_t = ct::rbd::FixBaseNLOC<Lwr4System>;
//    using RobotState_t = ct::rbd::FixBaseRobotState<n_joints, 0, Scalar>;
    using RobotState_t = typename FixBaseNLOC_t::RobotState_t;

    using StateVector_t = FixBaseNLOC_t::StateVector;
    using ControlVector_t = FixBaseNLOC_t::ControlVector;
//    using ControlVector_t = ct::core::ControlVector<control_dim, Scalar>;
    using StateVectorArray_t = typename FixBaseNLOC_t::StateVectorArray;
    using ControlVectorArray_t = typename FixBaseNLOC_t::ControlVectorArray;

    using StateTrajectory_t = ct::core::StateTrajectory<state_dim, Scalar>;
    using ControlTrajectory_t = ct::core::ControlTrajectory<control_dim, Scalar>;


    Lwr4NLOC(const std::string& contactFile,
    		const std::string& configFile,
			const std::string& costFunctionFile,
			const ct::rbd::tpl::RigidBodyPose<Scalar> basePose) :
				termQuadInter_(new TermQuadratic_t),
				termQuadFinal_(new TermQuadratic_t),
				termTaskspacePose_(new TermTaskspacePose_t),
				costFunctionAD_(new CostFunctionAD_t)
    {
        // load initial, final and intermediate states
        StateVector_t x0, xf, xi;

        ct::core::loadMatrix(costFunctionFile, "x_init", x0);
        refStateInitial_.fromStateVector(x0);

        ct::core::loadMatrix(costFunctionFile, "x_final", xf);
        refStateFinal_.fromStateVector(xf);

        // Load some settings
        ct::core::loadScalar(configFile, "init_type", initType_); 			// steady state or interpolated
        ct::core::loadScalar(configFile, "time_horizon", timeHorizon_);
        ct::core::loadScalar(configFile, "num_cycles", numCycles_);
        ct::core::loadScalar(costFunctionFile, "Q_friction_work", Q_work_); // work term weighting
        ct::core::loadMatrix(costFunctionFile, "K_init", K_); 				// initial feedback

        // Set-up iiwa with contacts
        contactModel_ = std::shared_ptr<ContactModel>(new ContactModel);
        contactModelAD_ = std::shared_ptr<ContactModelAD>(new ContactModelAD);

        contactModel_->loadParameters(contactFile);
        contactModelAD_->loadParameters(contactFile);

        lwr4_ = std::shared_ptr<Lwr4System>(new Lwr4System(contactModel_, basePose));
//        lwr4AD_ = std::shared_ptr<Lwr4SystemAD>(new Lwr4SystemAD(contactModelAD_, basePose));


        // Linearize system using numdiff
		lwr4Lin_ = std::shared_ptr<ct::rbd::RbdLinearizer<Lwr4System>>(
				new ct::rbd::RbdLinearizer<Lwr4System>(lwr4_));

        // Linearize the system using adcg. compile on the fly
//        lwr4Lin_ = std::shared_ptr<LinearSystem_t>(new ADCodegenLinearizer_t(lwr4AD_));
//        std::shared_ptr<ADCodegenLinearizer_t> iiwaLinearizer(new ADCodegenLinearizer_t(lwr4AD_));
//        iiwaLinearizer->compileJIT();
//        lwr4Lin_ = iiwaLinearizer;

        // load cost function terms from config file
        termQuadInter_->loadConfigFile(costFunctionFile, "term_inter");
        termQuadFinal_->loadConfigFile(costFunctionFile, "term_final");

        termTaskspacePose_->loadConfigFile(costFunctionFile, "term_task_space", true);

        // update reference states
        termQuadInter_->updateReferenceState(refStateFinal_.toStateVector());
        termQuadFinal_->updateReferenceState(refStateFinal_.toStateVector());

        // autodiff cost fun
        costFunctionAD_->addIntermediateTerm(termQuadInter_);
        costFunctionAD_->addIntermediateTerm(termTaskspacePose_);

        costFunctionAD_->addFinalTerm(termQuadFinal_);

        // load NLOC settings
        nlocSettings_.load(configFile, true, "ilqr");

        // compute number of time steps
        N_ = nlocSettings_.computeK(timeHorizon_);

        ct::core::StateVector<state_dim, Scalar> x_ref = xi;
		ct::core::ControlVector<control_dim, Scalar> u_ref =
				ct::core::ControlVector<control_dim, Scalar>::Zero(); // minimize control effort

        StateTrajectory_t stateRef, refState_0;
        ControlTrajectory_t controlRef, refControlFF_0;

        stateRefTraj_ = StateTrajectory_t(stateRef);
        controlRefTraj_ = ControlTrajectory_t(controlRef);

        costFunctionAD_->initialize();

        // Set-up solver
        nlocSolver_ = FixBaseNLOC_t(costFunctionAD_, nlocSettings_, lwr4_, true, lwr4Lin_);

        switch (initType_)
		{
			case 0:  // steady state
			{
				ROS_INFO("Initialize with constant initial state");

				ct::core::ControlVector<control_dim, Scalar> uff_ref;
				nlocSolver_.initializeSteadyPose(refStateInitial_, timeHorizon_, N_, uff_ref);

				// Also update cost function terms
				termQuadInter_->updateReferenceControl(uff_ref);

/*                std::vector<std::shared_ptr<ct::optcon::CostFunctionQuadratic<state_dim, control_dim>>>& inst1 =
					nlocSolver_.getSolver()->getCostFunctionInstances();

				for (size_t i = 0; i < inst1.size(); i++)
				{
					inst1[i]->getIntermediateTermById(intTermID)->updateReferenceControl(uff_ref);
				}*/

				break;
			}
			case 1:  // linear interpolation
			{
				ROS_INFO("Initialize with linear interpolation between initial and final state");

				nlocSolver_.initializeDirectInterpolation(refStateInitial_, refStateInter_, timeHorizon_, N_, K_);

				break;
			}
			default:
			{
				throw std::runtime_error("illegal init type");
				break;
			}
		}
    }




    //! some accessor methods
    const double& 						getTimeHorizon() 	{ return timeHorizon_; }
    const int& 							getNumTimeSteps() 	{ return N_; }
    const RobotState_t& 				getFinalState() 	{ return refStateFinal_; }
    FixBaseNLOC_t& 						getSolver() 		{ return nlocSolver_; }
    ct::optcon::NLOptConSettings& 		getNLOCSettings() 	{ return nlocSettings_; }
    std::shared_ptr<Lwr4System> 		getSystem() 		{ return lwr4_; }
    std::shared_ptr<LinearSystem_t> 	getLinearSystem() 	{ return lwr4Lin_; }
    std::shared_ptr<CostFunctionAD_t> 	getCostFunction() 	{ return costFunctionAD_; }

    StateTrajectory_t&					getStateRef()		{ return stateRefTraj_; }
    ControlTrajectory_t&				getControlRef() 	{ return controlRefTraj_; }

    std::shared_ptr<ContactModel> contactModel_;

    double Q_work_;

private:
    typename FixBaseNLOC_t::FeedbackArray::value_type K_;


    std::shared_ptr<ContactModelAD> contactModelAD_;

    std::shared_ptr<Lwr4System> lwr4_;
    std::shared_ptr<Lwr4SystemAD> lwr4AD_;
    std::shared_ptr<LinearSystem_t> lwr4Lin_;

    std::shared_ptr<TermQuadratic_t> termQuadInter_;
    std::shared_ptr<TermQuadratic_t> termQuadFinal_;
    std::shared_ptr<TermTaskspacePose_t> termTaskspacePose_;

    std::shared_ptr<CostFunctionAD_t> costFunctionAD_;

    ct::core::StateTrajectory<state_dim, Scalar> stateTrajInit_;

    size_t intTermID;
    size_t finalTermID;
    size_t mixedTermID;
    size_t intTermWorkID;

    ct::optcon::NLOptConSettings nlocSettings_;
    FixBaseNLOC_t nlocSolver_;

    RobotState_t refStateInitial_, refStateFinal_, refStateInter_;

    StateTrajectory_t stateRefTraj_;
    ControlTrajectory_t controlRefTraj_;

    ct::core::Time timeHorizon_;
    int initType_ = 0, N_ = 0;
    double numCycles_;

    const int numViaPoints_ = 4;
};
