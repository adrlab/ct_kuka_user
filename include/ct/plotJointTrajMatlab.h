
#pragma once

#include <kindr/Core>

//#include <matlabCppInterface/Engine.hpp>
#include <matlabCppInterface/MatFile.hpp>

#include <ct/kuka/pathNames.h>
#include <ct/kuka/TermFrictionWork.h>

template <size_t STATE_DIM, size_t CONTROL_DIM>
int plotJointTrajMatlab(
	const ct::core::TimeArray& timeArray,
	const ct::core::StateVectorArray<STATE_DIM, double>& stateArray,
	const ct::core::ControlVectorArray<CONTROL_DIM, double>& controlArray,
	const ct::core::DiscreteArray<Eigen::Matrix<double, 3, 1>>& eeForceArray,
	const ct::core::DiscreteArray<Eigen::Matrix<double, 3, 1>>& eePosArray,
	const ct::core::DiscreteArray<Eigen::Matrix<double, 3, 1>>& eeVelArray,
	//const std::shared_ptr<ct::rbd::iiwa::EEContactModel<typename RBDynamics::Kinematics_t>> contactModel,
	//const ct::rbd::tpl::RigidBodyPose<double> basePose,
	const char* filename)
{

    using namespace ct::core;

//    typedef ct::rbd::kuka::FixBaseFDSystem<RBDynamics, 0, false> RobotSystem;
//    typedef typename RobotSystem::FixBaseRobotState_t RobotState;
//    typedef ct::rbd::iiwa::EEContactModel<typename RBDynamics::Kinematics_t> ContactModel;

//    typedef kindr::Velocity<double, 3> Velocity3S;

//    std::shared_ptr<typename RBDynamics::Kinematics_t> iiwakin(new typename RBDynamics::Kinematics_t);

    /*
	// Plot directly with MATLAB engine
	matlab::Engine engine;

	if (!engine.initialize())
	{
		std::cout<<"Could not start Matlab"<<std::endl;
		return -1;
	}
	if (!engine.good())
	{
		std::cout<<"Matlab interface not working properly"<<std::endl;
		return -1;
	}
	*/

	if (timeArray.size() != stateArray.size())
	{
		std::cout << timeArray.size() << std::endl;
		std::cout << stateArray.size() << std::endl;
		throw std::runtime_error("Cannot plot data, x and t not equal length");
	}

	std::vector<double> joint1;
	std::vector<double> joint2;
	std::vector<double> joint3;
	std::vector<double> joint4;
	std::vector<double> joint5;
	std::vector<double> joint6;
	std::vector<double> joint7;

	std::vector<double> jointVel1;
	std::vector<double> jointVel2;
	std::vector<double> jointVel3;
	std::vector<double> jointVel4;
	std::vector<double> jointVel5;
	std::vector<double> jointVel6;
	std::vector<double> jointVel7;

	std::vector<double> control1;
	std::vector<double> control2;
	std::vector<double> control3;
	std::vector<double> control4;
	std::vector<double> control5;
	std::vector<double> control6;
	std::vector<double> control7;

	std::vector<double> eeforceX;
	std::vector<double> eeforceY;
	std::vector<double> eeforceZ;

	std::vector<double> eeposX;
	std::vector<double> eeposY;
	std::vector<double> eeposZ;

	std::vector<double> eeVelX;
	std::vector<double> eeVelY;
	std::vector<double> eeVelZ;

	std::vector<double> time_state;

	std::vector<double> workCostIntegral;

	for (size_t j = 0; j < stateArray.size(); j++)
	{
		joint1.push_back(stateArray[j](0));
		joint2.push_back(stateArray[j](1));
		joint3.push_back(stateArray[j](2));
		joint4.push_back(stateArray[j](3));
		joint5.push_back(stateArray[j](4));
		joint6.push_back(stateArray[j](5));
		joint7.push_back(stateArray[j](6));

		jointVel1.push_back(stateArray[j](7));
		jointVel2.push_back(stateArray[j](8));
		jointVel3.push_back(stateArray[j](9));
		jointVel4.push_back(stateArray[j](10));
		jointVel5.push_back(stateArray[j](11));
		jointVel6.push_back(stateArray[j](12));
		jointVel7.push_back(stateArray[j](13));

		control1.push_back(controlArray[j](0));
		control2.push_back(controlArray[j](1));
		control3.push_back(controlArray[j](2));
		control4.push_back(controlArray[j](3));
		control5.push_back(controlArray[j](4));
		control6.push_back(controlArray[j](5));
		control7.push_back(controlArray[j](6));

		time_state.push_back(timeArray[j]);

//		// Compute EE position
//		typename RobotState::RBDState_t rbdstate = RobotState::rbdStateFromVector(stateArray[j]);
//		rbdstate.basePose() = basePose;
//
//		typename RBDynamics::Kinematics_t::Position3Tpl eepos =
//				iiwakin->getEEPositionInWorld(0, rbdstate.basePose(), rbdstate.jointPositions());

		eeposX.push_back(eePosArray[j](0));
		eeposY.push_back(eePosArray[j](1));
		eeposZ.push_back(eePosArray[j](2));

		// Compute EE velocity
//		Velocity3S eeVelocity = iiwakin->getEEVelocityInWorld(0, rbdstate);

		eeVelX.push_back(eeVelArray[j](0));
		eeVelY.push_back(eeVelArray[j](1));
		eeVelZ.push_back(eeVelArray[j](2));

//		// Compute EE forces
//		typename ContactModel::EEForcesLinear Fcontact = contactModel->computeContactForces(rbdstate);

		eeforceX.push_back(eeForceArray[j](0));
		eeforceY.push_back(eeForceArray[j](1));
		eeforceZ.push_back(eeForceArray[j](2));

//		// Backward integrate the friction work term
//		double dt;
//
//		workCostIntegral.push_back(termFrictionWork.evaluate(
//				stateArray[j].toImplementation(),
//				controlArray[j].toImplementation(),
//				timeArray[j]));
//
//		if ( j > 0 )
//		{
//			dt = timeArray[j] - timeArray[j-1];
//			workCostIntegral[j] = dt * workCostIntegral[j] + workCostIntegral[j-1];
//		}
//		else
//		{
//			workCostIntegral[j] = 0.0;
//		}

	}

	// logging to matlab
	matlab::MatFile matFile;
	matFile.open(ct::kuka::loggingDir + filename);
	matFile.put("joint1", joint1);
	matFile.put("joint2", joint2);
	matFile.put("joint3", joint3);
	matFile.put("joint4", joint4);
	matFile.put("joint5", joint5);
	matFile.put("joint6", joint6);
	matFile.put("joint7", joint7);

	matFile.put("jointVel1", jointVel1);
	matFile.put("jointVel2", jointVel2);
	matFile.put("jointVel3", jointVel3);
	matFile.put("jointVel4", jointVel4);
	matFile.put("jointVel5", jointVel5);
	matFile.put("jointVel6", jointVel6);
	matFile.put("jointVel7", jointVel7);

	matFile.put("control1", control1);
	matFile.put("control2", control2);
	matFile.put("control3", control3);
	matFile.put("control4", control4);
	matFile.put("control5", control5);
	matFile.put("control6", control6);
	matFile.put("control7", control7);

	matFile.put("eeforceX", eeforceX);
	matFile.put("eeforceY", eeforceY);
	matFile.put("eeforceZ", eeforceZ);

	matFile.put("eeposx", eeposX);
	matFile.put("eeposy", eeposY);
	matFile.put("eeposz", eeposZ);

	matFile.put("eevelx", eeVelX);
	matFile.put("eevely", eeVelY);
	matFile.put("eevelz", eeVelZ);

//	matFile.put("workCostIntegral", workCostIntegral);

	matFile.put("t", time_state);
	matFile.close();


/*
	char* plotcommand = "figure(); "
			"subplot(3,1,1);"
			"plot(joint1);"
			"ylabel('joint 1');"
			"grid on;";

	/*
			"subplot(3,1,2);"
			"plot(squeeze(t),x(2,:));"
			"ylabel('x velocity');"
			"grid on;"
			"subplot(3,1,3);"
			"plot(squeeze(t),Ff);"
			"ylabel('friction force');"
			"xlabel('time');"
			"grid on";


	engine.executeCommand(plotcommand);

	engine.openCommandLine();
*/

	return 0;
}
