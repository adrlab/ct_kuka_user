
//#include <kuka/TermFrictionWork.h>
//#include <kuka/TermNormalForce.h>
//#include <kuka/TermNormalForceAnalytical.h>
//#include <kuka/EEContactModel.h>
//#include <kuka/FixBaseFDSystem.h>

//#include <ct/iiwa/iiwa.h>
#include <ct/iiwa/IiwaSystem.h>
#include <ct/iiwa/codegen/IiwaLinearizedForward.h>
#include <ct/iiwa/codegen/IiwaCMLinearizedForwardExt.h>

#include <ct/kuka/KukaOptcon.h>

//#include <kuka/CostFunctionAD.hpp>
//#include <kuka/CostFunctionAD-impl.hpp>

class IiwaNLOC
{
public:
    // Typedef robot and environment for autodiff linearization
	typedef double Scalar;
	typedef CppAD::AD< CppAD::cg::CG<Scalar>> ScalarCG;

    // Standard types
//	typedef ct::rbd::iiwa::Dynamics IiwaDynamics;
//    typedef ct::kuka::FixBaseFDSystem<IiwaDynamics, 0, false> IiwaSystem;
//    typedef ct::kuka::EEContactModel<typename IiwaDynamics::Kinematics_t> ContactModel;

	// AD types:
//	typedef ct::rbd::iiwa::tpl::Dynamics<ScalarCG> IiwaDynamicsAD;
//    typedef ct::kuka::FixBaseFDSystem<IiwaDynamicsAD, 0, false> IiwaSystemAD;
//    typedef ct::kuka::EEContactModel<typename IiwaDynamicsAD::Kinematics_t> ContactModelAD;

    static const size_t n_joints = ct::iiwa::System::NJOINTS;
    static const size_t state_dim = ct::iiwa::System::STATE_DIM;
    static const size_t control_dim = ct::iiwa::System::CONTROL_DIM;

//    using LinearSystem_t = ct::core::LinearSystem<state_dim, control_dim, Scalar>;
//    using ADCodegenLinearizer_t = ct::core::ADCodegenLinearizer<state_dim, control_dim, Scalar>;

//    using IiwaLinGenerated = ct::iiwa::codegen::IiwaLinearizedForward;
    using IiwaLinGenerated = ct::iiwa::codegen::IiwaCMLinearizedForward;

//    using CostFunctionAD_t = ct::optcon::CostFunctionAD<state_dim, control_dim, Scalar>;
//    using TermQuadratic_t = ct::optcon::TermQuadratic<state_dim, control_dim, Scalar, Scalar>;
//    using TermQuadraticAD_t = ct::optcon::TermQuadratic<state_dim, control_dim, Scalar, ScalarCG>;

    // TODO debug ADCG for the tracking term
//    using TermQuadTrackingAD = ct::optcon::TermQuadTracking<state_dim, control_dim, Scalar, ScalarCG>;
//    using TermQuadTracking_t = ct::optcon::TermQuadTracking<state_dim, control_dim, Scalar, Scalar>;

//    using TermFrictionWorkAD = ct::kuka::TermFrictionWork<double, ScalarCG>;

//    using TermNormalForceAD = ct::kuka::TermNormalForce<double, ScalarCG>;
//    using TermNormalForce = ct::kuka::TermNormalForceAnalytical;


//    using RobotState_t = ct::rbd::FixBaseRobotState<n_joints, 0, Scalar>;
    using RobotState_t = typename ct::iiwa::FixBaseNLOC_t::RobotState_t;

    using StateVector_t = ct::iiwa::FixBaseNLOC_t::StateVector;
    using ControlVector_t = ct::iiwa::FixBaseNLOC_t::ControlVector;
//    using ControlVector_t = ct::core::ControlVector<control_dim, Scalar>;
    using StateVectorArray_t = typename ct::iiwa::FixBaseNLOC_t::StateVectorArray;
    using ControlVectorArray_t = typename ct::iiwa::FixBaseNLOC_t::ControlVectorArray;

    using StateTrajectory_t = ct::core::StateTrajectory<state_dim, Scalar>;
    using ControlTrajectory_t = ct::core::ControlTrajectory<control_dim, Scalar>;


    IiwaNLOC(const std::string& contactFile,
    		const std::string& configFile,
			const std::string& costFunctionFile,
			const ::ct::rbd::tpl::RigidBodyPose<double> basePose) :
				termQuadInter_(new ct::kuka::TermQuadraticAD_t),
//				termQuadViaPoint1_(new ct::kuka::TermQuadratic_t),
//				termQuadViaPoint2_(new ct::kuka::TermQuadraticAD_t),
				termQuadFinal_(new ct::kuka::TermQuadratic_t),
//				termQuadTracking_(new TermQuadTracking_t),
				costFunctionAD_(new ct::kuka::CostFunctionAD_t),
				basePose_(basePose)
    {
        // load initial, final and intermediate states
        StateVector_t x0, xf, xi;

        ct::core::loadMatrix(costFunctionFile, "x_start", x0);
        refStateInitial_.fromStateVector(x0);

        ct::core::loadMatrix(costFunctionFile, "x_A", xi);
        refStateInter_.fromStateVector(xi);

        ct::core::loadMatrix(costFunctionFile, "x_A", xf);
        refStateFinal_.fromStateVector(xf);

        // Load some settings
        ct::core::loadScalar(configFile, "init_type", initType_); 			// steady state or interpolated
        ct::core::loadScalar(configFile, "time_horizon", timeHorizon_);
        ct::core::loadScalar(configFile, "num_cycles", numCycles_);
//        ct::core::loadScalar(costFunctionFile, "Q_friction_work", Q_work_); // work term weighting
        ct::core::loadMatrix(costFunctionFile, "K_init", K_); 				// initial feedback

        ct::core::loadScalar(costFunctionFile, "term_normal_force.q_lin", Q_force_lin_); // normal force weighting
        ct::core::loadScalar(costFunctionFile, "term_normal_force.q_quad", Q_force_quad_); // normal force weighting
        ct::core::loadScalar(costFunctionFile, "term_normal_force.force_reference", normalForceReference_);

        // Set-up iiwa with contacts
        contactModel_ = std::shared_ptr<ct::iiwa::ContactModel>(new ct::iiwa::ContactModel);
//        contactModelAD_ = std::shared_ptr<ContactModelAD>(new ContactModelAD);

        contactModel_->loadParameters(contactFile);
//        contactModelAD_->loadParameters(contactFile);

        iiwa_ = std::shared_ptr<ct::iiwa::System>(new ct::iiwa::System(contactModel_));
//        iiwaAD_ = std::shared_ptr<IiwaSystemAD>(new IiwaSystemAD(contactModelAD_));


        // Linearize system using numdiff
//		iiwaLin_ = std::shared_ptr<ct::rbd::RbdLinearizer<IiwaSystem>>(
//				new ct::rbd::RbdLinearizer<IiwaSystem>(iiwa_));

        // Linearize the system using adcg. compile on the fly
//        iiwaLin_ = std::shared_ptr<ct::kuka::LinearSystem_t>(new ADCodegenLinearizer_t(iiwaAD_));
//        std::shared_ptr<ADCodegenLinearizer_t> iiwaLinearizer(new ADCodegenLinearizer_t(iiwaAD_));
//        iiwaLinearizer->compileJIT();
//        iiwaLin_ = iiwaLinearizer;

        // Use pre-generated linear model
        std::shared_ptr<IiwaLinGenerated> iiwaLinearized(new IiwaLinGenerated(contactModel_));
        iiwaLin_ = iiwaLinearized;

        // load cost function terms from config file
        termQuadInter_->loadConfigFile(costFunctionFile, "term_const");
        termQuadFinal_->loadConfigFile(costFunctionFile, "term_final");

//        termFrictionWork_ = std::shared_ptr<TermFrictionWorkAD>(
//        		new TermFrictionWorkAD(contactFile, "TermFrictionWork", Q_work_));

//        termNormalForce_ = std::shared_ptr<TermNormalForce>(
//        		new TermNormalForce(contactFile, "normal force cost",
//        				Q_force_lin_, Q_force_quad_, normalForceReference_));
//        termNormalForce_->loadTimeActivation(costFunctionFile, "term_normal_force", true);
//
//        termQuadViaPoint1_->loadConfigFile(costFunctionFile, "term_via_point_1", true);
//        termQuadViaPoint1_->loadTimeActivation(costFunctionFile, "term_via_point_1", true);
//
//        termQuadViaPoint2_->loadConfigFile(costFunctionFile, "term_via_point_2", true);
//        termQuadViaPoint2_->loadTimeActivation(costFunctionFile, "term_via_point_2", true);

        // update reference states
        termQuadInter_->updateReferenceState(refStateInter_.toStateVector());
        termQuadFinal_->updateReferenceState(refStateFinal_.toStateVector());

        // autodiff cost fun
        costFunctionAD_->addIntermediateADTerm(termQuadInter_);
//        costFunctionAD_->addIntermediateTerm(termQuadViaPoint1_);
//        costFunctionAD_->addIntermediateADTerm(termQuadViaPoint2_);
//        costFunctionAD_->addIntermediateADTerm(termFrictionWork_);
//        costFunctionAD_->addIntermediateTerm(termNormalForce_);
        costFunctionAD_->addFinalTerm(termQuadFinal_);

        // load NLOC settings
        nlocSettings_.load(configFile, true, "solver");

        // compute number of time steps
        N_ = nlocSettings_.computeK(timeHorizon_);

//        ct::core::StateVector<state_dim, Scalar> x_ref = xi;
//		ct::core::ControlVector<control_dim, Scalar> u_ref =
//		ct::core::ControlVector<control_dim, Scalar>::Zero(); // minimize control effort

        StateTrajectory_t stateRef, refState_0;
        ControlTrajectory_t controlRef, refControlFF_0;

//        generateCycles<StateVector_t>(numCycles_,
//        		refStateFinal_.toStateVector(),
//				refStateInter_.toStateVector(),
//				stateRef, refState_0);
//
//        generateCycles<ControlVector_t>(numCycles_,
//        		iiwa_->computeIDTorques(refStateFinal_.toStateVector()),
//				iiwa_->computeIDTorques(refStateInter_.toStateVector()),
//        		controlRef, refControlFF_0);

//        // Set back to zero for testing
//        for (int i=0; i<numCycles_*2+1; i++)
//        {
//        	controlRef[i] = ControlVector_t::Zero();
//        }

//        std::cout << "Initial control:\n" << controlRef[0] << std::endl;
//
//        stateRefTraj_ = StateTrajectory_t(stateRef);
//        controlRefTraj_ = ControlTrajectory_t(controlRef);
//
//        termQuadTracking_->loadConfigFile(costFunctionFile, "term_tracking", true);
//        termQuadTracking_->setStateAndControlReference(stateRef, controlRef);
//        termQuadTracking_->setName("tracking cost"); // TODO implement function for name loading

//        costFunctionAD_->addIntermediateTerm(termQuadTracking_);

        costFunctionAD_->initialize();

        // Set-up solver
        nlocSolver_ = ct::iiwa::FixBaseNLOC_t(costFunctionAD_, nlocSettings_, iiwa_, true, iiwaLin_);

        switch (initType_)
		{
			case 0:  // steady state
			{
				ROS_INFO("Initialize with constant initial state");

				ct::core::ControlVector<control_dim, Scalar> uff_ref;
				nlocSolver_.initializeSteadyPose(refStateInitial_, timeHorizon_, N_, uff_ref);

				// Also update cost function terms
//				termQuadInter_->updateReferenceControl(uff_ref);

/*                std::vector<std::shared_ptr<ct::optcon::CostFunctionQuadratic<state_dim, control_dim>>>& inst1 =
					nlocSolver_.getSolver()->getCostFunctionInstances();

				for (size_t i = 0; i < inst1.size(); i++)
				{
					inst1[i]->getIntermediateTermById(intTermID)->updateReferenceControl(uff_ref);
				}*/

				break;
			}
			case 1:  // linear interpolation
			{
				ROS_INFO("Initialize with linear interpolation between initial and final state");

				nlocSolver_.initializeDirectInterpolation(refStateInitial_, refStateInter_, timeHorizon_, N_, K_);

				break;
			}
			case 2: // linear interpolation with via points
			{
				ROS_INFO("Initialize with linear interpolation between via points");

                StateVectorArray_t x_0(N_ + 1);
        		ControlVectorArray_t uff_0(N_);

        		// Sample initial state trajectory at points of the time array and compute controls
        		ct::core::TimeArray time = ct::core::TimeArray(nlocSettings_.dt, N_+1, 0.0);
        		ct::core::TimeArray::iterator timeIter;

        		for (timeIter = time.begin(); timeIter != time.end()-1; timeIter++)
        		{
        			size_t index = std::distance(time.begin(), timeIter);

        			x_0[index] = refState_0.eval(*timeIter);
        			uff_0[index] = refControlFF_0.eval(*timeIter);
        		}

        		x_0[N_] = refState_0.eval(*(time.end())); // final state

				ct::iiwa::FixBaseNLOC_t::FeedbackArray ufb_0(N_, K_);

        		typename ct::iiwa::FixBaseNLOC_t::NLOptConSolver::Policy_t policy(x_0, uff_0, ufb_0 , nlocSettings_.dt);

				nlocSolver_.getSolver()->changeInitialState(refStateInitial_.toStateVector());
				nlocSolver_.getSolver()->changeTimeHorizon(timeHorizon_);
				nlocSolver_.getSolver()->setInitialGuess(policy);

				break;
			}
			default:
			{
				throw std::runtime_error("illegal init type");
				break;
			}
		}
    }

    template <class Vector>
    void generateCycles(const int numCycles,
    		const Vector& pointA, const Vector& pointB,
    		ct::core::DiscreteTrajectoryBase<Vector, Eigen::aligned_allocator<Vector>>& trajZOH,
			ct::core::DiscreteTrajectoryBase<Vector, Eigen::aligned_allocator<Vector>>& trajLin)
    {
    	int numPoints = numCycles * 2 + 1;

    	ct::core::tpl::TimeArray<Scalar> time(timeHorizon_/(numPoints-1), numPoints);

    	trajZOH.clear();
    	trajLin.clear();

    	trajZOH.setInterpolationType(ct::core::InterpolationType::ZOH);
    	trajLin.setInterpolationType(ct::core::InterpolationType::LIN);

    	for (int i=0; i<numCycles*2; i+=2)
    	{
    		trajZOH.push_back(pointA, time.at(i), true);
    		trajZOH.push_back(pointB, time.at(i+1), true);

    		trajLin.push_back(pointA, time.at(i), true);
			trajLin.push_back(pointB, time.at(i+1), true);
    	}

    	trajZOH.push_back(pointB, time.at(numPoints-1), true);
    	trajLin.push_back(pointA, time.at(numPoints-1), true);

    }

    void loadCostTerms()
    {
        // load cost function terms from config file
        termQuadInter_->loadConfigFile(costFunctionFile_, "term_inter");
        termQuadFinal_->loadConfigFile(costFunctionFile_, "term_final");

//        termQuadViaPoint1_->loadConfigFile(costFunctionFile_, "term_via_point_1", true);
//        termQuadViaPoint1_->loadTimeActivation(costFunctionFile_, "term_via_point_1", true);
//
//        termQuadViaPoint2_->loadConfigFile(costFunctionFile_, "term_via_point_2", true);
//        termQuadViaPoint2_->loadTimeActivation(costFunctionFile_, "term_via_point_2", true);

    }


    //! some accessor methods
    const double& 						getTimeHorizon() 	{ return timeHorizon_; }
    const int& 							getNumTimeSteps() 	{ return N_; }
    const RobotState_t& 				getFinalState() 	{ return refStateFinal_; }
    ct::iiwa::FixBaseNLOC_t& 						getSolver() 		{ return nlocSolver_; }
    ct::optcon::NLOptConSettings& 		getNLOCSettings() 	{ return nlocSettings_; }
    std::shared_ptr<ct::iiwa::System> 		getSystem() 		{ return iiwa_; }
    std::shared_ptr<ct::kuka::LinearSystem_t> 	getLinearSystem() 	{ return iiwaLin_; }
    std::shared_ptr<ct::kuka::CostFunctionAD_t> 	getCostFunction() 	{ return costFunctionAD_; }
//    std::shared_ptr<TermFrictionWorkAD>	getWorkTerm() 		{ return termFrictionWork_; }

    StateTrajectory_t&					getStateRefTraj()		{ return stateRefTraj_; }
    ControlTrajectory_t&				getControlRefTraj() 	{ return controlRefTraj_; }

    ControlVectorArray_t 				getControlRef()
    {
    	ControlVectorArray_t controlRef = ControlVectorArray_t(N_-1);

    	ct::core::TimeArray time = ct::core::TimeArray(nlocSettings_.dt, N_-1, 0.0);

    	for (size_t i=0; i<N_-1; i++)
    	{
    		controlRef.at(i) = controlRefTraj_.eval(time.at(i));
    	}

    	return controlRef;
    }

    std::vector<std::shared_ptr<ct::optcon::TermBase<state_dim, control_dim, Scalar>>> getIntermediateTerms()
	{
    	std::vector<std::shared_ptr<ct::optcon::TermBase<state_dim, control_dim, Scalar>>> terms;

//    	terms.push_back(termQuadViaPoint1_);
//    	terms.push_back(termQuadTracking_);
//    	terms.push_back(termNormalForce_);

    	return terms;
	}

private:
    typename ct::iiwa::FixBaseNLOC_t::FeedbackArray::value_type K_;

    ct::rbd::tpl::RigidBodyPose<double> basePose_;

//    std::shared_ptr<ContactModelAD> contactModelAD_;

    std::shared_ptr<ct::iiwa::System> iiwa_;
//    std::shared_ptr<ct::iiwa::SystemAD> iiwaAD_;
    std::shared_ptr<ct::kuka::LinearSystem_t> iiwaLin_;

    std::shared_ptr<ct::kuka::TermQuadraticAD_t> termQuadInter_;
//    std::shared_ptr<TermQuadratic_t> termQuadViaPoint1_;
//    std::shared_ptr<ct::kuka::TermQuadraticAD_t> termQuadViaPoint2_;
    std::shared_ptr<ct::kuka::TermQuadratic_t> termQuadFinal_;
//    std::shared_ptr<TermQuadTracking_t> termQuadTracking_;
//    std::shared_ptr<TermFrictionWorkAD> termFrictionWork_;
//    std::shared_ptr<TermNormalForce> termNormalForce_;

    std::shared_ptr<ct::kuka::CostFunctionAD_t> costFunctionAD_;

    ct::core::StateTrajectory<state_dim, Scalar> stateTrajInit_;

    size_t intTermID;
    size_t finalTermID;
    size_t mixedTermID;
    size_t intTermWorkID;

    ct::optcon::NLOptConSettings nlocSettings_;
    ct::iiwa::FixBaseNLOC_t nlocSolver_;

    std::shared_ptr<ct::iiwa::ContactModel> contactModel_;

//    double Q_work_;
	double Q_force_lin_, Q_force_quad_, normalForceReference_;

    RobotState_t refStateInitial_, refStateFinal_, refStateInter_;
    StateTrajectory_t stateRefTraj_;
    ControlTrajectory_t controlRefTraj_;

    const std::string costFunctionFile_, configFile_, contactFile_;

    ct::core::Time timeHorizon_;
    int initType_ = 0, N_ = 0;
    double numCycles_;

    const int numViaPoints_ = 4;

    std::string termTrackingName_;
};
