/*
 * IiwaSystem.h
 *
 *  Created on: Jul 23, 2020
 *      Author: johannes
 *
 * Define frequently used types of the iiwa robot using the scalar type double.
 *
 */

#pragma once

#include <ct/iiwa/iiwa.h>

#include <ct/kuka/FixBaseFDSystem.h>
#include <ct/kuka/EEContactModel.h>

namespace ct{
namespace iiwa{

using Dynamics = ct::rbd::iiwa::Dynamics; // scalar type is double

using System = ct::kuka::FixBaseFDSystem<Dynamics>;
using ContactModel = ct::kuka::EEContactModel<typename Dynamics::Kinematics_t>;

using FixBaseNLOC_t = ct::rbd::FixBaseNLOC<System>;

}
}

//extern template class ct::rbd::EndEffector<ct::iiwa::Dynamics::NJOINTS, double>;
//extern template class ct::rbd::SelectionMatrix<ct::iiwa::System::STATE_DIM, ct::iiwa::System::CONTROL_DIM, double>;

extern template class ct::kuka::FixBaseFDSystem<ct::iiwa::Dynamics>;
extern template class ct::kuka::EEContactModel<typename ct::iiwa::Dynamics::Kinematics_t>;

//extern template class ct::rbd::FixBaseNLOC<ct::iiwa::System>;

